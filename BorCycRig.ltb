\documentclass{amsbook}
\usepackage{etex}
\usepackage[ngerman,british]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{microtype}

% Get all MR-entries for articles in the Courant Kolloquium and in
% Abbaspour Marcolli 2010 Deformation spaces
% Bakke et al Abel Symposium 2011
% Baum Cortiñas Meyer et al Topics in algebraic and topological

% \setlength{\textwidth}{199cm}
% \setlength{\textheight}{399cm}
\usepackage[lite]{amsrefs}
\newcommand*{\MRref}[2]{ \href{http://www.ams.org/mathscinet-getitem?mr=#1}{MR \textbf{#1}}}

\newcommand*{\arxiv}[1]{\href{http://www.arxiv.org/abs/#1}{arXiv: #1}}

\renewcommand*{\PrintDOI}[1]{\href{http://dx.doi.org/\detokenize{#1}}{doi: \detokenize{#1}}}

% doi and eprint added to standard definition from amsrefs.sty
\BibSpec{book}{%
  +{}  {\PrintPrimary}                {transition}
  +{,} { \textit}                     {title}
  +{.} { }                            {part}
  +{:} { \textit}                     {subtitle}
  +{,} { \PrintEdition}               {edition}
  +{}  { \PrintEditorsB}              {editor}
  +{,} { \PrintTranslatorsC}          {translator}
  +{,} { \PrintContributions}         {contribution}
  +{,} { }                            {series}
  +{,} { \voltext}                    {volume}
  +{,} { }                            {publisher}
  +{,} { }                            {organization}
  +{,} { }                            {address}
  +{,} { \PrintDateB}                 {date}
  +{,} { }                            {status}
  +{}  { \parenthesize}               {language}
  +{}  { \PrintTranslation}           {translation}
  +{;} { \PrintReprint}               {reprint}
  +{.} { }                            {note}
  +{.} {}                             {transition}
  +{} { \PrintDOI}                   {doi}
  +{} { available at \url}            {eprint}
  +{}  {\SentenceSpace \PrintReviews} {review}
}

\setlength{\emergencystretch}{3em}
\renewcommand*{\MRref}[2]{ \href{http://ams.math.uni-bielefeld.de/mathscinet-getitem?mr=#1}{MR \textbf{#1}}}

\usepackage[utf8]{inputenc},
\usepackage[pdftitle={Bibliography},
pdfauthor={Ralf Meyer},
pdfsubject={Mathematics},
]{hyperref}
\begin{document}
\begin{bibdiv}
\begin{biblist}

\bib{Atiyah-Macdonald:Commutative}{book}{
  author={Atiyah, Michael F.},
  author={Macdonald, Ian G.},
  title={Introduction to commutative algebra},
  publisher={Addison-Wesley Publishing Co., Reading, Mass.-London-Don Mills, Ont.},
  date={1969},
  pages={ix+128},
  % review={\MR{0242802}},
}%g

\bib{Bambozzi:Affinoid}{thesis}{
  author={Bambozzi, Federico},
  title={On a generalization of affinoid varieties},
  institution={Universit\`a degli Studi di Padova},
  type={phdthesis},
  date={2014},
  eprint={arXiv:1401.5702},
}

\bib{berth}{article}{
author={Berthelot, Pierre},
title={Cohomologie rigide et cohomologie rigide \`a supports propres.  Premi\`ere partie},
status={preprint},
}

\bib{Berthelot-finitude}{article}{
   author={Berthelot, Pierre},
   title={Finitude et puret\'e cohomologique en cohomologie rigide},
   language={French},
   note={With an appendix in English by Aise Johan de Jong},
   journal={Invent. Math.},
   volume={128},
   date={1997},
   number={2},
   pages={329--377},
   issn={0020-9910},
   %doi={10.1007/s002220050143},
}

\bib{Besser}{article}{
   author={Besser, Amnon},
   title={Syntomic regulators and $p$-adic integration. I. Rigid syntomic
   regulators},
   booktitle={Proceedings of the Conference on $p$-adic Aspects of the
   Theory of Automorphic Representations (Jerusalem, 1998)},
   journal={Israel J. Math.},
   volume={120},
   date={2000},
   number={part B},
   part={part B},
   pages={291--334},
   issn={0021-2172},
   %doi={10.1007/BF02834843},
}


\bib{BourbakiAC}{book}{
   author={Bourbaki, Nicolas},
   title={\'El\'ements de math\'ematique. Fascicule XXVIII. Alg\`ebre commutative.
   Chapitre 3: Graduations, filtrations et topologies. Chapitre 4: Id\'eaux
   premiers associ\'es et d\'ecomposition primaire},
   language={French},
   series={Actualit\'es Scientifiques et Industrielles, No. 1293},
   publisher={Hermann, Paris},
   date={1961},
   pages={183},
   %review={\MR{0171800}},
}

\bib{chwinf}{article}{
   author={Corti\~nas, G.},
   author={Haesemeyer, C.},
   author={Weibel, C.  A.},
   title={Infinitesimal cohomology and the Chern character to negative
   cyclic homology},
   journal={Math.  Ann.},
   volume={344},
   date={2009},
   number={4},
   pages={891--922},
   issn={0025-5831},
   % review={\MR{2507630 (2010i:19006)}},
   doi={10.1007/s00208-009-0333-9},
}

\bib{corval}{article}{
   author={Corti\~nas, Guillermo},
   author={Valqui, Christian},
   title={Excision in bivariant periodic cyclic cohomology: a categorical approach},
   note={Special issue in honor of Hyman Bass on his seventieth birthday.
   Part II},
   journal={\(K\)-Theory},
   volume={30},
   date={2003},
   number={2},
   pages={167--201},
   issn={0920-3036},
   % review={\MR{2064238 (2005c:19005)}},
   %doi={10.1023/B:KTHE.0000018383.93721.dd},
}

\bib{coco}{article} {
    AUTHOR = {Corti\~nas, Guillermo},
     TITLE = {De {R}ham and infinitesimal cohomology in {K}apranov's model
              for noncommutative algebraic geometry},
   JOURNAL = {Compositio Math.},
    VOLUME = {136},
      YEAR = {2003},
    NUMBER = {2},
     PAGES = {171--208},
      ISSN = {0010-437X},
       %DOI = {10.1023/A:1022732008165},
       %URL = {http://dx.doi.org/10.1023/A:1022732008165},
}

\bib{connes}{article}{
  author={Connes, Alain},
  title={Noncommutative differential geometry},
  journal={Inst. Hautes \'Etudes Sci. Publ. Math.},
  number={62},
  date={1985},
  pages={257--360},
  issn={0073-8301},
  % review={\MR{823176}},
  %eprint={http://www.numdam.org/item?id=PMIHES_1985__62__257_0},
}

\bib{ConEntire}{article}{
    AUTHOR = {Connes, Alain},
     TITLE = {Entire cyclic cohomology of {B}anach algebras and characters
              of {$\theta$}-summable {F}redholm modules},
   JOURNAL = {$K$-Theory},
    VOLUME = {1},
      YEAR = {1988},
    NUMBER = {6},
     PAGES = {519--548},
      ISSN = {0920-3036},
%DOI = {10.1007/BF00533785},
}


\bib{CD}{article} {
    AUTHOR = {Cuntz, Joachim},
    author = {Deninger, Christopher}
     TITLE = {An alternative to {W}itt vectors},
   JOURNAL = {M\"unster J. Math.},
  %FJOURNAL = {M\"unster Journal of Mathematics},
    VOLUME = {7},
      YEAR = {2014},
    NUMBER = {1},
     PAGES = {105--114},
      ISSN = {1867-5778},
}

\bib{cmr}{book}{
   author={Cuntz, Joachim},
   author={Meyer, Ralf},
   author={Rosenberg, Jonathan M.},
   title={Topological and bivariant \(K\)-theory},
   series={Oberwolfach Seminars},
   volume={36},
   publisher={Birkh\"auser Verlag, Basel},
   date={2007},
   pages={xii+262},
   isbn={978-3-7643-8398-5},
   %review={\MR{2340673}},
}

\bib{cqhc}{article}{
   author={Cuntz, Joachim},
   author={Quillen, Daniel},
   title={Cyclic homology and nonsingularity},
   journal={J.  Amer.  Math.  Soc.},
   volume={8},
   date={1995},
   number={2},
   pages={373--442},
   issn={0894-0347},
   %review={\MR{1303030}},
   %doi={10.2307/2152822},
}


\bib{CQEx}{article}{
    AUTHOR = {Cuntz, Joachim},
		 author= {Quillen, Daniel},
     TITLE = {Excision in bivariant periodic cyclic cohomology},
   JOURNAL = {Invent.  Math.},
    VOLUME = {127},
      YEAR = {1997},
    NUMBER = {1},
     PAGES = {67--98},
      ISSN = {0020-9910},
       DOI = {10.1007/s002220050115},
       URL = {http://dx.doi.org/10.1007/s002220050115},
}

\bib{mme}{article}{
   author={Elkik, Ren{\'e}e},
   title={Solutions d'\'equations \`a coefficients dans un anneau
   hens\'elien},
   language={French},
   journal={Ann.  Sci.  \'Ecole Norm.  Sup.  (4)},
   volume={6},
   date={1973},
   pages={553--603 (1974)},
   issn={0012-9593},
   %review={\MR{0345966 (49 \#10692)}},
}

\bib{ft}{article}{
   author={Fe{\u\i}gin, Boris},
   author={Tsygan, Boris},
   title={Additive \(K\)-theory and crystalline cohomology},
   language={Russian},
   journal={Funktsional.  Anal.  i Prilozhen.},
   volume={19},
   date={1985},
   number={2},
   pages={52--62, 96},
   issn={0374-1990},
   %review={\MR{800920 (88e:18008)}},
}

\bib{ft2}{incollection}  {
    author={Fe{\u\i}gin, Boris},
   author={Tsygan, Boris},
     TITLE = {Additive {$K$}-theory},
 BOOKTITLE = {{$K$}-theory, arithmetic and geometry ({M}oscow, 1984--1986)},
    SERIES = {Lecture Notes in Math.},
    VOLUME = {1289},
     PAGES = {67--209},
 PUBLISHER = {Springer, Berlin},
      YEAR = {1987},
       %DOI = {10.1007/BFb0078368},
       %URL = {http://dx.doi.org/10.1007/BFb0078368},
}


\bib{freput}{book}{
   author={Fresnel, Jean},
   author={van der Put, Marius},
   title={Rigid analytic geometry and its applications},
   series={Progress in Mathematics},
   volume={218},
   publisher={Birkh\"auser Boston, Inc., Boston, MA},
   date={2004},
   pages={xii+296},
   isbn={0-8176-4206-4},
   %review={\MR{2014891 (2004i:14023)}},
   %doi={10.1007/978-1-4612-0041-3},
}

\bib{ful}{article}{
   author={Fulton, William},
   title={A note on weakly complete algebras},
   journal={Bull.  Amer.  Math.  Soc.},
   volume={75},
   date={1969},
   pages={591--593},
   issn={0002-9904},
   %review={\MR{0238829 (39 \#193)}},
}

\bib{gkdagger}{article}{
   author={Gro\ss e-Kl\"onne, Elmar},
   title={Rigid analytic spaces with overconvergent structure sheaf},
   journal={J.  Reine Angew.  Math.},
   volume={519},
   date={2000},
   pages={73--95},
   issn={0075-4102},
   %review={\MR{1739729 (2001b:14033)}},
   %doi={10.1515/crll.2000.018},
}

\bib{gkdr}{article}{
   author={Gro\ss e-Kl\"onne, Elmar},
   title={De Rham cohomology of rigid spaces},
   journal={Math.  Z.},
   volume={247},
   date={2004},
   number={2},
   pages={223--240},
   issn={0025-5874},
   %review={\MR{2064051 (2005c:14024)}},
   %doi={10.1007/s00209-003-0544-9},
}

\bib{Hogbe-Nlend:Completions}{article}{
  author={Hogbe-Nlend, Henri},
  title={Compl\'etion, tenseurs et nucl\'earit\'e en bornologie},
  journal={J.  Math.  Pures Appl.  (9)},
  volume={49},
  date={1970},
  pages={193--288},
  %review={\MR{0279557 {43\,\#5279}},
}

\bib{Hogbe-Nlend:Theorie}{book} {
    AUTHOR = {Hogbe-Nlend, Henri},
     TITLE = {Th\'eorie des bornologies et applications},
    SERIES = {Lecture Notes in Mathematics, Vol. 213},
 PUBLISHER = {Springer-Verlag, Berlin-New York},
      YEAR = {1971},
     PAGES = {v+168},
}

\bib{Hogbe-Nlend:Bornologies}{book}{
  author={Hogbe-Nlend, Henri},
  title={Bornologies and functional analysis},
  publisher={North-Holland Publishing Co.},
  place={Amsterdam},
  date={1977},
  pages={xii+144},
  isbn={0-7204-0712-5},
  %review={\MRref {0500064}{58\,\#17774}},
}

\bib{Hubl}{article}{
    AUTHOR = {H{\"u}bl, Reinhold},
     TITLE = {A note on the {H}ochschild homology and cyclic homology of a
              topological algebra},
   JOURNAL = {Manuscripta Math.},
    VOLUME = {77},
      YEAR = {1992},
    NUMBER = {1},
     PAGES = {63--70},
      ISSN = {0025-2611},
       DOI = {10.1007/BF02567044},
       URL = {http://dx.doi.org/10.1007/BF02567044},
}

\bib{mix}{article}{
   author={Kassel, Christian},
   title={Cyclic homology, comodules, and mixed complexes},
   journal={J.  Algebra},
   volume={107},
   date={1987},
   number={1},
   pages={195--216},
   issn={0021-8693},
   % review={\MR{883882}},
   %doi={10.1016/0021-8693(87)90086-X},
}

\bib{lestum}{book}{
author={Le Stum, Bernard},
title={Rigid cohomology},
series={Cambridge Tracts in Mathematics},
volume={172},
publisher={Cambridge University Press},
isbn={978--0--521--87524--0}
}

\bib{lod}{book}{
author={Loday, Jean-Louis},
title={Cyclic homology},
date={1992},
series={Grundlehren der Mathematischen Wissenschaften},
volume={301},
publisher={Springer},
address={Berlin},
}

\bib{lq}{article}{
  author={Loday, Jean-Louis},
  author={Quillen, Daniel},
  title={Cyclic homology and the Lie algebra homology of matrices},
  journal={Comment. Math. Helv.},
  volume={59},
  date={1984},
  number={4},
  pages={569--591},
  issn={0010-2571},
  % review={\MR{780077}},
  %doi={10.1007/BF02566367},
}

\bib{mat}{book}{
author={Matsumura, Hideyuki},
title={Commutative algebra},
publisher={Benjamin/Cummings Publishing Company},
address={Reading},
date={1980},
}

\bib{Meyer:Embed_derived}{article}{
  author={Meyer, Ralf},
  title={Embeddings of derived categories of bornological modules},
  date={2004},
  status={eprint},
  note={arXiv: math.FA/0410596},
}%g

\bib{Meyer:HLHA}{book}{
  author={Meyer, Ralf},
  title={Local and analytic cyclic homology},
  series={EMS Tracts in Mathematics},
  volume={3},
  publisher={European Mathematical Society (EMS), Z\"urich},
  date={2007},
  pages={viii+360},
  isbn={978-3-03719-039-5},
  %review={\MRref {2337277}{2009g:46138}},
  %doi={10.4171/039},
}

\bib{Meyer:Homological_algebra_Schwartz}{article}{
  author={Meyer, Ralf},
  title={Homological algebra for Schwartz algebras},
  conference={
    title={Symmetries in Algebra and Number Theory},
    date={2008},
    place={G\"ottingen},
  },
  book={
    publisher={Universit\"atsverlag G\"ottingen},
  },
}%g

\bib{mw}{article}{
  author={Monsky, Paul},
  author={Washnitzer, Gerard},
  title={Formal cohomology.  I},
  journal={Ann. of Math.  (2)},
  volume={88},
  date={1968},
  pages={181--217},
  issn={0003-486X},
  % review={\MR{0248141 (40 \#1395)}},
}

\bib{pus}{article}{
  author={Puschnigg, Michael},
  title={Explicit product structures in cyclic homology theories},
  journal={$K$-Theory},
  volume={15},
  date={1998},
  number={4},
  pages={323--345},
  issn={0920-3036},
  %review={\MRref{1656222}{99j:16003}},
  %doi={10.1023/A:1007748727044},
} 

\bib{qhcr}{misc}{
author={Quillen, Daniel},
title={Homology of commutative rings},
status={Unpublished MIT notes (1968).}
}
\bib{qhct}{article}{
   author={Quillen, Daniel},
   title={Bivariant cyclic cohomology and models for cyclic homology types},
   journal={J.  Pure Appl.  Algebra},
   volume={101},
   date={1995},
   number={1},
   pages={1--33},
   issn={0022-4049},
   % review={\MR{1346426}},
   %doi={10.1016/0022-4049(95)00002-E},
}

\bib{schneider}{book}{
   author={Schneider, Peter},
   title={Nonarchimedean functional analysis},
   series={Springer Monographs in Mathematics},
   publisher={Springer-Verlag, Berlin},
   date={2002},
   pages={vi+156},
   isbn={3-540-42533-0},
   review={\MR{1869547}},
   %doi={10.1007/978-3-662-04728-6},
}

\bib{swan}{article}{
   author={Swan, Richard G.},
   title={N\'eron-Popescu desingularization},
   conference={
      title={Algebra and geometry},
      address={Taipei},
      date={1995},
   },
   book={
      series={Lect.  Algebra Geom.},
      volume={2},
      publisher={Int.  Press, Cambridge, MA},
   },
   date={1998},
   pages={135--192},
   % review={\MR{1697953 (2000h:13006)}},
}

\bib{Taylor:General_fun}{article}{
  author={Taylor, Joseph L.},
  title={A general framework for a multi-operator functional calculus},
  journal={Adv. Math.},
  volume={9},
  date={1972},
  pages={183--252},
  issn={0001-8708},
  % review={\MRref{0328625}{48\,\#6967}},
  %doi={10.1016/0001-8708(72)90017-5},
}%g

\bib{vdp}{article}{
   author={van der Put, Marius},
   title={The cohomology of Monsky and Washnitzer},
   language={English, with French summary},
   note={Introductions aux cohomologies \(p\)\nb-adiques (Luminy, 1984)},
   journal={M\'em.  Soc.  Math.  France (N.S.)},
   number={23},
   date={1986},
   pages={4, 33--59},
   issn={0037-9484},
   % review={\MR{865811 (88a:14022)}},
}

\bib{vdp2}{article}{
   author={van der Put, Marius},
   title={de Rham cohomology of affinoid spaces},
   journal={Compositio Math.},
   volume={73},
   date={1990},
   number={2},
   pages={223--239},
   issn={0010-437X},
   % review={\MR{1046739 (91c:14027)}},
}


\bib{chubu}{book}{
   author={Weibel, Charles A.},
   title={An introduction to homological algebra},
   series={Cambridge Studies in Advanced Mathematics},
   volume={38},
   publisher={Cambridge University Press, Cambridge},
   date={1994},
   pages={xiv+450},
   isbn={0-521-43500-5},
   isbn={0-521-55987-1},
   % review={\MR{1269324 (95f:18001)}},
   doi={10.1017/CBO9781139644136},
}


\end{biblist}
\end{bibdiv}
\end{document}
