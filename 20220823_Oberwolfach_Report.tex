\documentclass{owrart}
\usepackage{amssymb}
\begin{document}

%% --------------------------------------------------------------------------
%% Please use the environment "talk" for each abstract.
%% It has three obligatory and one optional argument. The syntax is:
%% -----------------------
%% \begin{talk}[coauthors]{Name of the speaker}{Title of the talk}{Author Sorting Index}
%%      .....
%% \end{talk}
%% -----------------------
%% The names of coauthors will appear in form of "(joint work with ...)"
%%
%% The Author Sorting Index should be given as the last and first name of the speaker,
%% separated by a comma. If for example the name of the speaker is "John Smith", then
%% the correct Author Sorting Index is "Smith, John".
%% Any special characters (like accents, German umlaute, etc.) should be replaced by
%% their "non-special" version, eg replace \"a by a, \'a by a, etc.
%%
%% Please use the standard thebibliography environment to include
%% your references, and try to use labels for the bibitems, which
%% are uniquely assigned to you in order to avoid conflicts with other authors.
%% You can achieve unique labels by using our on initials before every label.
%% -------------------------------------------------------------------------------

\begin{talk}[Suliman Albandik, Alcides Buss, Camila Fabre Sehnem,
  and Chenchang Zhu]{Ralf
    Meyer}{A bicategorical interpretation for crossed products and
    Cuntz-Pimsner algebras}{Meyer, Ralf}

\noindent
Many interesting C*-algebras are defined as a crossed product for a
group action on a C*-algebra.  Here an action of a group~\(G\) on a
C*-algebra~\(A\) is a homomorphism~\(\alpha\) from~\(G\) to the
automorphism group of~\(A\).  For various purposes, we would like to
consider more general group actions.  For instance, in the study of
the Farrell--Jones conjecture, a key tool are group actions up to
homotopy, where the homomorphism condition
\(\alpha_g\alpha_h = \alpha_{g h}\) is replaced by a homotopy, and
higher homotopies as well.  These higher homotopies may be encoded
succinctly by a principal bundle over the classifying space~\(B G\)
of~\(G\) with fibre~\(A\).  A drawback of these actions up to
homotopy is that there is no way to define a crossed product
C*-algebra for them.  How can we generalise group actions so that
the crossed product C*-algebra may still be defined?  I have studied
this question and its ramifications for several years, with various
coauthors.

It is useful to start from the crossed product \(B = A \rtimes G\)
and observe some extra structure on it.  Namely, it is graded by the
group~\(G\).  View \(A\rtimes G\) as a completion of the space of
finitely supported functions \(G\to A\) and let \(B_g \subseteq B\)
be the subspace of functions supported in the singleton~\(\{g\}\).
This is a closed subspace of~\(B\), and the *-algebra structure
on~\(B\) is defined so that \(B_g \cdot B_h \subseteq B_{g h}\) and
\(B_g^* = B_{g^{-1}}\).  When we remember only the Banach
spaces~\(B_g\) with the multiplication and involution maps that they
inherit from~\(B\), then we get a \emph{Fell bundle}.  Given a Fell
bundle \((B_g)_{g\in G}\), the direct sum \(\bigoplus B_g\) carries
a canonical *-algebra structure, and the C*-completion of
\(\bigoplus B_g\) is the analogue of the crossed product for a Fell
bundle.  Thus we may view Fell bundles as generalised group actions.
Here the C*-algebra on which the action takes place is the unit
fibre \(A\mathbin{:=} B_e\) for the unit element \(e\in G\).

Actually, it is better to view Fell bundles as generalised partial
actions, where a partial action of~\(G\) on~\(A\) only gives
isomorphisms between ideals in~\(A\), not on~\(A\) itself.  Each
fibre~\(B_g\) becomes a Hilbert \(A\)-bimodule using the
multiplication maps \(B_e \times B_g \to B_g\),
\(B_g \times B_e \to B_g\) and the inner products
\(x^* y, x y^*\in B_e\) for \(x,y\in B_g\).  Such a Hilbert bimodule
gives a Morita--Rieffel equivalence between the two-sided ideals
in~\(A\) that are spanned by the inner products \(x^* y\) and
\(x y^*\), respectively.  That is, it is a partial Morita--Rieffel
equivalence on~\(A\).  A Fell bundle is called \emph{saturated} if
these ideals are equal to~\(A\) for all \(g\in G\).  Then
each~\(B_g\) is a Morita--Rieffel self-equivalence of~\(A\).  The
multiplication maps \(B_g \times B_h \to B_{g h}\) in the Fell
bundles are a crucial ingredient, which we should not forget.  To
encode these as well, we consider a bicategory that has
C*-algebras as objects, Hilbert bimodules as arrows, and bimodule
maps that are isometric for both inner products as 2-arrows.  The
composition of arrows is given by the balanced tensor product of
Hilbert bimodules.  A bicategory homomorphism from~\(G\), viewed as
a bicategory to this bicategory of Hilbert bimodules is equivalent
to a saturated Fell bundle over~\(G\) (see~\cite{rm-BMZ2}).  (The
more general bicategory morphisms also allow partial actions, but we
must add some technical extra conditions to the standard definition
of a bicategory morphism to get exactly the right concept.  The
issue is to recover the involution of a Fell bundle from the inner
products \(x y^*\) and \(x^* y\) that it defines.  This only works
under extra conditions.)

The crossed product for a group action is defined by a universal
property, and an analogous universal property works for the
``crossed product'' C*-algebra of a Fell bundle.  This universal
property becomes that of a (bi)limit in a suitable bicategory.  To
understand such universal properties, we must enlarge our bicategory
because we need *-homomorphisms such as the inclusion
of \(A\) into its crossed products to be arrows.  When we combine
*-homomorphisms with Hilbert bimodules, we get C*-correspondences.
A \emph{C*-correspondence} \(A\leftarrow B\) is a Hilbert
\(B\)-module~\(E\) with a nondegenerate *-homomorphism from~\(A\)
to the C*-algebra of adjointable operators on~\(E\).  If we insist
on Hilbert bimodules that are full on the left, then the
*-homomorphism has values in the C*-algebra of compact operators
on~\(E\).  Such C*-correspondences are called \emph{proper}.

In my earlier papers, I viewed a C*-correspondence as an arrow
from~\(A\) to~\(B\), but I have found since then that it is better
to treat it as an arrow from~\(B\) to~\(A\).  A homomorphism
from~\(G\) to the C*-correspondence bicategory is still the same as
a saturated Fell bundle because the equivalences in the
C*-correspondence bicategory are exactly the Morita--Rieffel
equivalences and a homomorphism from a group to a bicategory maps
all arrows in~\(G\) to equivalences.

Now it turns out that the crossed product C*-algebra~\(B\) of a Fell
bundle or group action satisfies the universal property of a limit
in the C*-correspondence bicategory.  The limit is universal for
bicategorical cones.  Such a cone under the homomorphism \((B_g)\)
consists of a C*-correspondence \(F\colon B_e\leftarrow D\) and
isomorphisms of correspondences
\(u_g\colon B_g \otimes_{B_e} F \to F\) for all \(g\in G\),
satisfying some coherence conditions.  The isomorphism~\(u_g\) is
equivalent to a Toeplitz representation~\(T_g\) of~\(B_g\) by
adjointable operators on~\(F\) with the extra nondegeneracy property
that the linear span of \(T_g(b_g)\cdot x\) for \(b_g\in B_g\),
\(x\in F\) is dense in~\(F\); this translates to~\(u_g\) being
surjective.  It turns out that this is exactly the same as a
representation of the Fell bundle on~\(F\) by adjointable operators
or, in other words, a C*-correspondence \(B\leftarrow D\).  It is
remarkable that the limit in a bicategory becomes ``bigger'' than
the original diagram.  In contrast, if we take the limit for a group
action on a set, we get the subset of fixed points, while the
colimit is the set of orbits.  In a bicategory, however, the cone
comes with extra data --~the unitaries~\(u_g\) above~-- instead of
extra conditions.

Now let us go beyond the group case.  What is a homomorphism from,
say, a monoid~\(M\) to the C*-correspondence bicategory, and what
would its limit look like?  These questions are mostly answered
in~\cite{rm-AM1}.  A homomorphism from~\(M\) to the
C*-correspondence bicategory is the same as a product system
over~\(M\).  (With the conventions used in~\cite{rm-AM1}, it is a
product system over the opposite monoid instead, and the limit
becomes a colimit instead.)  Assume that the product system is
proper, meaning that the left actions in the C*-correspondences are
by compact operators.  Then a cone over the diagram with
summit~\(D\) turns out to be the same as a Cuntz--Pimsner covariant
representation of the product system by adjointable operators on a
Hilbert \(D\)-module~\(F\).  Thus the Cuntz--Pimsner algebra of the
product system is a bicategorical limit.  If the product system is
not proper, then it is unclear whether a limit exists.  Certainly,
the Cuntz--Pimsner algebra is not a limit any more.

Already the case of the monoid of natural numbers is interesting.
In this case, a homomorphism or product system is determined by a
single C*-correspondence \(E\colon A\leftarrow A\).  Many
interesting C*-algebras are defined as the Cuntz--Pimsner algebra of
such a single C*-correspondence.  This includes the C*-algebras of
(regular) graphs, (regular) topological graphs, self-similar groups,
or self-similar graphs.  In these cases, the C*-algebra~\(A\) is
much simpler than the final C*-algebra, and the Cuntz--Pimsner
algebra definition offers a lot of tools to study it.  The case when
the C*-correspondence is not proper is more complicated.  The most
interesting aspect in the construction of a Cuntz--Pimsner algebra
is the Cuntz--Pimsner covariance condition, which only makes sense
for an element of~\(A\) that acts on~\(E\) by a compact operator.
To get the most interesting Cuntz--Pimsner algebra, Katsura proposed
to ask the Cuntz--Pimsner covariance condition on the largest ideal
in~\(A\) that acts on~\(E\) faithfully and by compact operators.
This rather careful choice is not just coming from bicategory
theory.

What bicategory theory can explain is the \emph{relative
  Cuntz--Pimsner algebra} construction, where we specify the ideal
on which we ask the Cuntz--Pimsner covariance condition as part of
the data (see~\cite{rm-MS}).  The limit construction for diagrams is
right adjoint to the inclusion of constant diagrams.  To get a more
general construction, we may consider a larger subcategory of
``nice'' diagrams and consider a reflector to that subcategory.  The
Cuntz--Pimsner algebra carries a canonical gauge action by the
circle group, which is equivalent to a grading by the group of
integers.  This grading is always ``semi-saturated'', so that it is
determined by its subspaces of degree \(0\) and~\(1\).  These form
another C*-algebra~\(B\) and a Hilbert bimodule over~\(B\),
which usually is not saturated.  Thus the Cuntz--Pimsner algebra
construction, together with the gauge action, may be interpreted as
mapping a C*-correspondence \(A\leftarrow A\) to a Hilbert bimodule
\(B \leftarrow B\).  Hilbert bimodules form a subbicategory in
the C*-correspondence bicategory.  In~\cite{rm-MS}, we also add an
ideal in~\(A\) to the data to be able to choose the ideal on which
the Cuntz--Pimsner covariance condition is imposed.  Then we
carefully choose a bicategory of such decorated self-correspondences
and a subbicategory, so that the Cuntz--Pimsner algebra construction
is a reflector to that subbicategory.

\begin{thebibliography}{99}
\bibitem{rm-AM1}%
  S.~Albandik, R.~Meyer, \textit{Colimits in the correspondence
    bicategory}, M\"unster J. Math. \textbf{9} (2016), 51--76.
  
\bibitem{rm-BMZ2}
  A.~Buss, R.~Meyer, Ch.~Zhu,
  \textit{A higher category approach to twisted actions on C*-algebras},
  Proc. Edinb. Math. Soc. (2)~\textbf{56} (2013), 387--426.

\bibitem{rm-MS}
  R.~Meyer, C.~Sehnem,,
  \textit{A bicategorical interpretation for relative Cuntz--Pimsner algebras},
  Math. Scand. \textbf{125} (2019),
  84--112.
\end{thebibliography}
\end{talk}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
