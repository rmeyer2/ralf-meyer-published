size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("s1", hatch(1mm, NE));
add("s2", hatch(1mm, NW));
path c=circle((0,0), 2);
path c1=circle(polar(2,-pi/12), 0.8);
path c2=circle(polar(2,pi/8), 1);
//---------------------------------------------------------
filldraw(c1, pattern("s2"), invisible);
clip(c); clip(c2);
filldraw(c1, pattern("s1"), invisible);
clip(c2);

dot((0,0));
draw(c);

dot("\(1\)", polar(2,-pi/12), E);
draw(c1);

dot("\(2\)", polar(2,pi/8), NE);
draw(c2);
