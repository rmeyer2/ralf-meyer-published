size(10cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform t=shift((12,0));
real u=0, v=0;
real yu1(real x) {return sqrt(x^2-u);}
real yu2(real x) {return -sqrt(x^2-u);}
real yv(real x) {return v/(2*x);}
//---------------------------------------------------------
for(int i=-3; i<=3; ++i)
{
  draw(t*shift((i,0))*((0,-3.5)--(0,3.5)), dashed);
  draw(t*shift((0,i))*((-3.5,0)--(3.5,0)));
}

draw("\(w=z^2\)", (5,1)..(6,1.2)..(7.2,1), N, dotted, EndArrow);


draw((-4,0)--(4,0));
draw((0,-4)--(0,4));
for(u=0; u<=6; u+=2)
{
  draw(graph(yu1, sqrt(u)+0.00001, 4, operator ..), dashed);
  draw(graph(yu2, sqrt(u)+0.00001, 4, operator ..), dashed);
  draw(graph(yu1, -4, -sqrt(u)-0.00001, operator ..), dashed);
  draw(graph(yu2, -4, -sqrt(u)-0.00001, operator ..), dashed);
}
for(u=-6; u<=0; u+=2)
{
  draw(graph(yu1, -4, 4, operator ..), dashed);
  draw(graph(yu2, -4, 4, operator ..), dashed);
}
for(v=-6; v<=6; v+=2)
{
  draw(graph(yv, -4, -0.1, operator ..));
  draw(graph(yv, 0.1, 4, operator ..));
}
label("\(+\)", (3.5,-1.5));
label("\(+\)", (-3.5,-1.5));
label("\(-\)", (-1.5,3.5));
label("\(-\)", (-1.5,-3.5));
clip((-5,-5)--(-5,5)--t*(5,5)--t*(5,-5)--cycle);
