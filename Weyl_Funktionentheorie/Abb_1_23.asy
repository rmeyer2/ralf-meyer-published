size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
xaxis("$x$", black+0.3mm, EndArrow);
yaxis("$y$", black+0.3mm, EndArrow);
dot((0,0));

draw((-2,-2)--(2,2));
draw((-2,2)--(2,-2));

for(int i=0; i<5; ++i)
    draw(arc((0,0), 0.4*i, 22.5,382.5), EndArrow);

draw(arc((0,0), 0.4*2 + 0.2, 45,67.5), EndArrow);
