size(7cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
//---------------------------------------------------------
filldraw((-3,-0.5)--(-3,0.5)--(3,0.5)--(3,-0.5)--cycle,
      pattern("strich"), invisible);
filldraw((-3,-0.1)--(-3,0.1)--(-0.8,0.1)--(-0.8,-0.1)--cycle,
      white, invisible);
filldraw((3,-0.1)--(3,0.1)--(0.8,0.1)--(0.8,-0.1)--cycle,
      white, invisible);
draw((-1,0)--(-3,0));
draw((1,0)--(3,0));
filldraw(circle((-1,-0.3), 0.25), white, invisible);
filldraw(circle((1,-0.3), 0.25), white, invisible);
dot("\(-1\)", (-1,0), S);
dot("\(1\)", (1,0), S);
