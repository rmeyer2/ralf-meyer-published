size(7cm,0);
import solids;
defaultpen(font("OT1","cmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,0,0);
// O ist automatisch der Ursprung: O == (0,0,0) durch import three

triple M = (0,0,1), No = (0,0,2), P = (0,sqrt(8),0);
real t = -(2*P.z-4)/((P.x)^2+(P.y)^2+(P.z-2)^2);
triple P1 = (t*P.x,t*P.y,t*(P.z-2)+2), Q1 = (P1.x,P1.y,0), V1 = (0,0,P1.z);
revolution kugel = sphere(M,1);
//guide3 halbkreis = No..P1..O;
//---------------------------------------------------------
kugel.draw(2);
draw( O -- (0,3.5,0), EndArrow); //label("$y$",(0,3.5,0),S);
draw( O -- (0,0,2.5), EndArrow); label("$z$",(0,0,2.5),W);
dot("$0$", O, S);
dot("$(P)$", P, S);
dot("$N$", No, NW);
dot("$(P')$", P1, (-2.5,-1.5));
dot("$V'$", V1, W);
dot("$(Q')$", Q1, S);

//draw(halbkreis);
draw(No--P);
draw(O--P);
draw(P1--V1, dashed);
draw(P1--Q1, dashed);
draw(P1--O, dashed);
