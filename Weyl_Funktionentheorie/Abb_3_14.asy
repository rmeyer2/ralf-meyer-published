usepackage("amsfonts");
size(8cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich1", hatch(3mm, NE));
add("strich2", hatch(3mm, NW));

real r=0.3, rho=0.3;
path K=circle((-1,0), rho);
path k=circle((0,0), r);
path K1=circle((0,0), 2);
//---------------------------------------------------------
filldraw(arc((0,0), 2, 0, 180)--cycle, pattern("strich1"));
filldraw(arc((0,0), 2, 180, 360)--cycle, pattern("strich2"));

filldraw(K, white);
draw(arc((-1,0), rho, 0, 120), ArcArrow);
draw("{\(\rho\)}", (-1,0)--polar(rho, 5*pi/4)-(1,0), SE-(0.5,0));
filldraw(circle((-1,-rho-0.14), 0.12), white, invisible);
label("\(K\)", (-1,-rho), S);

filldraw(k, white);
draw(arc((0,0), r, 0, 120), ArcArrow);
draw("{\(r\)}", (0,0)--polar(r, 5*pi/4), SE-(0.5,0));
filldraw(circle((-0.1,-r-0.12), 0.1), white, invisible);
label("\(\mathfrak{k}\)", (0,-r), SW);

draw(arc((0,0), 2, 0, 45), Arrow);
label("\(\mathfrak{K}\)", (1.5,-1.5));

xaxis("\(x\)", xmax=2.3, mediumgrey, EndArrow);
yaxis("\(y\)", ymax=2.3, mediumgrey, EndArrow);
draw((-2,0)--(-1-rho,0));
draw((-1+rho,0)--(-r,0));
draw((r,0)--(2,0));
dot("\(-1\)", (-1,0), N+(0,0));
