size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair O=(0,0);
//---------------------------------------------------------
filldraw((-5,-1)--(-5,1)--(1,1)--(1,-1)--cycle,
          pattern("strich"), invisible);
filldraw((-5,-0.2)--(-5,0.2)--(0.2,0.2)--(0.2,-0.2)--cycle,
          white, invisible);
draw(O--(-5,0), 0.4mm+black);
filldraw(circle((0,-0.3), 0.2), white, invisible);
dot("\(0\)", O, S);
