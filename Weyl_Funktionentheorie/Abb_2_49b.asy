size(0,3cm);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real y=3.1; real x1=1; real x2=4.1;
//---------------------------------------------------------
draw((-y,0)--(y,0), 0.4mm+black);
draw((-y,1)--(y,1));
draw((-y,2)--(y,2), 0.4mm+black);
draw((-y,3)--(y,3));
draw((-y,4)--(y,4));
label("\(0\)", (0,0), S);
draw((-0.5,-0.5)--(-2,-0.5), EndArrow);

draw((-3,x1)--(-3,x2), dashed); label("\(b\)", (-3,x2), N);
draw((-1.5,x1)--(-1.5,x2), dashed); label("\(a\)", (-1.5,x2), N);
draw((0,-0.1)--(0,4-pi), 0.4mm+black);
draw((0,4-pi)--(0,4), 0.5mm+dotted); label("\(\pi\)", (0,x2), N);
draw((1.5,x1)--(1.5,x2), dashed); label("\(a_1\)", (1.5,x2), N);
draw((3,x1)--(3,x2), dashed); label("\(b_1\)", (3,x2), N);
draw((-0.5,1.8)--(-0.5,x2), EndArrow);
