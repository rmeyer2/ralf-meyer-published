size(8cm,0);
import three;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(2,-7,2);
face[] faces;

path3 ebene=shift((-2,-4,0))*plane((4,0,0),(0,8,0));
path3 achse=(0,0,-2)--(0,0,1.5);
path3 kreis1=circle(O,1,Z), kreis2=circle(O,1.8,Z);
//---------------------------------------------------------
draw(faces.push(ebene), achse);
filldraw(faces.push(ebene), ebene, white);

add(faces);

dot(O);
draw(O--(0,0,1.5));
draw(kreis1, dashed);
draw(kreis2, dashed);
