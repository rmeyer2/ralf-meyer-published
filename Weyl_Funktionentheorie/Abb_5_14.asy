usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path L=(-3,0)..(0,-2)..(3,0)..(0,2)..cycle;
//---------------------------------------------------------
draw(L);
draw(subpath(L, 1, 1.5), EndArrow);
label("\(\mathfrak{L}\)", point(L,2.5), NE);

dot((-2,0));
draw(circle((-2,0), 0.5));

dot((1,-1));
draw(circle((1,-1), 0.5));

dot((1,1));
draw(circle((1,1), 0.5));
