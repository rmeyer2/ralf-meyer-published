size(7cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair p=(0,-1);
pair a1=(1,1), a2=polar(abs(a1), -log(abs(a1))^2 / angle(a1));
pair f1(real t) {return exp(t)*log(abs(a1))*(cos(angle(a1)*t),sin(angle(a1)*t));}
pair f2(real t) {return exp(t)*log(abs(a2))*(cos(angle(a2)*t),sin(angle(a2)*t));}
guide g1,g2;
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(x\)", EndArrow);

for(real i=0; i<=5; i+=0.1)
{
  g1 = g1..f1(i);
  g2 = g2..f2(i);
}
draw(g1);
draw(g2);
dot(f1(0));
dot(f2(0));
//label(string( log(abs(a1)) /), (1,0));
