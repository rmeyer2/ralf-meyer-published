size(5cm,0);
import math;
import graph;
import patterns;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real n=8;
real r=3;
real phi=2*pi/n;
pair O=(0,0);
add("gestrichelt", hatch(2mm,E));
//---------------------------------------------------------
dot(O);
draw(arc(O,r, -20, 200));
draw(O--polar(1.4*r, 0));
draw(O--polar(1.4*r, phi));
draw("$r$", O--polar(r, 3*pi/4));
filldraw(O--polar(1.3*r, 0)--polar(1.3*r, phi)--cycle, pattern("gestrichelt"), invisible);

markangle(radius=40, polar(r,0), O, polar(r,phi));
filldraw(circle((1.3,0.6),0.4), white, invisible);
label("$\frac{2\pi}{n}$", O, (5,2));
label("$I$", polar(1.4*r,0), E);
label("$II$", polar(1.4*r,phi), NE);
