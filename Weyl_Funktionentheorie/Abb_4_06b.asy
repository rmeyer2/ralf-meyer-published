usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real r=1;
pair z0=polar(r*0.5,pi/4);
pair z=polar(r*2, 0);
path K2=circle((0,0),r);
path K1=scale(1.5)*K2;
path p=z0--z0+(2,0.5);
//---------------------------------------------------------
dot("\(0\)", (0,0), SE);

draw(K2);
//label("\(\mathfrak{K}''\)", (0,-r), N);

draw(K1);
//label("\(\mathfrak{K}'\)", polar(1.5*r,-pi/4), E);

dot("\(z_0\)", z0, N);
dot("\(z\)", z, SW);
dot("\(z'\)", intersectionpoints(K2,p)[0], N+(0.5,0.5));
dot("\(z''\)", intersectionpoints(K1,p)[0], N+(0.5,0.5));
draw(p);
dot("\(z_1\)", point(p,1), N);
