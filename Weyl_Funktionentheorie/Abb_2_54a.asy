unitsize(0.7cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
pair O=(0,0);
//---------------------------------------------------------
for(real i=-pi/2; i<=pi/2; i+=pi/8)
  draw((i,-2)--(i,2));
label("\(-\frac{\pi}{2}\)", (-pi/2,0), W);
label("\(+\frac{\pi}{2}\)", (pi/2,0), E);

draw((0,-2)--(0,2), 0.4mm+black);
label("\(0\)", O, SW+(0.2,0));

draw((-pi/2,1)--(pi/2,1), dashed);
draw((-pi/2,0)--(pi/2,0), dashed);
draw((-pi/2,-1)--(pi/2,-1), dashed);

filldraw((3*pi/8,-2)--(3*pi/8,2)--(pi/2,2)--(pi/2,-2)--cycle,
        pattern("strich"), invisible);
