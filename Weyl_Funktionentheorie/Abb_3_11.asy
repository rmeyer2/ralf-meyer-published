size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path g=(-5,1)..(0,-1)..(5,1)..(5,5)..(0,12)..(-5,5)..cycle;
path p=shift((-0.5,0))*((-3,-2)..(0,3)..(-1,7)..(0,9)..(2,11)..(4,13));
path p1, p2;
pair d;
real t;
//---------------------------------------------------------
draw(g, 0.3mm+black);
draw(p);
for(real i; i<=1; i+=0.02)
{
  t = reltime(p, i);
  d=point(p, t);
  draw(d -- d+rotate(-90)*dir(p,t) );
  p1=p1..d+rotate(-90)*dir(p,t);
  p2=p2..d+rotate(-90)*dir(p,t)/2;
}
draw(p1);
draw(p2);
label("G", (3,5));
clip(g);
