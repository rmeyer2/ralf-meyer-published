size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), u=(1.5,1);
//---------------------------------------------------------
draw("\(\vec{u}\)", O--u, EndArrow);
dot("\(x,y\)", O, SE);
