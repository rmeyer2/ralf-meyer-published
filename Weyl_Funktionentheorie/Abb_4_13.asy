usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair z0=(0,0), z=(-1,-3);
path K=circle(z0,5);
path K1=circle(z0,4.5);
path k=circle(z0,1);
path k1=circle(z0,1.5);
//---------------------------------------------------------
draw(K);
label("\(\mathfrak{K}\)", point(K, 1.5), NW);

filldraw(K1, pattern("strich"));
filldraw(circle(polar(3.7,3*pi/4), 0.5), white, invisible);
label("\(\mathfrak{K}_1\)", point(K1, 1.5), SE);

filldraw(k1, white);
filldraw(circle((0,2.2), 0.5), white, invisible);
label("\(\mathfrak{k}_1\)", point(k1, 1), N);

draw(k);
label("\(\mathfrak{k}\)", point(k, -0.5), (-0.2,0.2));

dot("\(z_0\)", z0, (-0.1,0.1));
filldraw(circle((-1.5,-3), 0.3), white, invisible);
dot("\(z\)", z, W);
