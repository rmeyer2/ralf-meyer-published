//--broken in new version of Asymptote
size(6cm,0);
import three;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,1);

triple O=(0,0,0), M=(0,0,1), No=(0,0,2), P=(2.5,0,0);
triple u=(1,2,0), v=(-1,1,0);
path3 g1=(P-2*u)--(P+0.5*u);
path3 g2=(P-0.7*v)--(P+3*v);
path3 plane1=shift(No-0.5*u)*scale3(1)*plane(0.5*u,P-No);
path3 plane2=shift(No+2*v)*scale3(1)*plane(-2*v,P-No);
add("gestrichelt1", hatch(3mm,project(u)));
add("gestrichelt2", hatch(3mm,project(v)));
path3 C=circle(M,1,Y);
triple P1[]=intersectionpoints(P--No,C);
//---------------------------------------------------------
draw(g2);
label("$g_2$", P+3*v, S);
draw(plane2);
filldraw(project(plane2),palegrey);
filldraw(project(plane2),pattern("gestrichelt2"));
label("$E_2$", (-1,0,0.5), W);
draw((-1,0,0.5)--(0.3,0,0.8), dotted);

draw(g1);
label("$g_1$", P-2*u, S);
draw(plane1);
filldraw(project(plane1),mediumgrey);
filldraw(project(plane1),pattern("gestrichelt1"));
label("$E_1$", (2.5,0,1), E);
draw((2.5,0,1)--(1.6,0,0.5), dotted);

draw(P--No);
dot("$P$", P, N);
dot("$N$", No, NE);
dot("$P'$", P1[1], NE);
