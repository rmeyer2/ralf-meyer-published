size(4cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair eins=polar(1,9*pi/8), zwei=polar(1,3*pi/4);
pair drei=polar(1,-pi/8), vier=polar(1,3*pi/16);
path c=circle((0,0),1);
//---------------------------------------------------------
draw(c, dashed);
draw(eins--zwei);
draw(eins--drei);
draw(zwei--drei);
draw(drei--vier);
draw(vier--eins);

label("$1$", eins, W);
label("$2$", zwei, NW);
label("$3$", drei, E);
label("$4$", vier, NE);

markangle(radius=20, eins, zwei, drei);
markangle(radius=20, eins, vier, drei);
