size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
//---------------------------------------------------------
draw(rotate(45)*((-0.5,0)..(-0.3,2)..(0,2.2)..(1.2,2.5)..(2.5,0)..
         (1.2,-2.5)..(0,-2.2)..(-0.3,-2)..cycle));
draw(shift((1,-0.5))*scale(0.2)*((-2,-2)..(-1.8,0)..(-1.8,2)..(-1.6,2)..(2,2.3)..(2.3,0)..
         (3,-2)..(0,-2.1)..cycle));
filldraw(circle((0,0),0.3),white, invisible);
label("\(\mbox{G}\)", (0,0));
