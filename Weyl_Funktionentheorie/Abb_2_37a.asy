size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
//---------------------------------------------------------
filldraw((-2,-2)..(-1.8,0)..(-2,1.9)..(-1.9,2)..(0,2.2)..
     (2,1.5)..(3,-1.5)..(2.9,-1.6)..(0,-2.5)..cycle, pattern("strich"));
filldraw((0.5,-1)..(1,-0.1)..(1.1,0)..(1.5,-0.5)..(1.3,-0.5)..(1.2,-0.8)..cycle, white);
filldraw(circle((0,0),0.3),white, invisible);
label("\(\mbox{G}\)", (0,0));
