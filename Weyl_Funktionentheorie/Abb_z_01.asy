size(7cm,0);
import math;
import graph;
import geometry;
import markers;
//---------------------------------------------------------
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
int n=5;
real phi=pi/3, rho = 6;
pair O=(0,0), A=polar(rho,phi);
path kreis=circle(O,4);
//---------------------------------------------------------
draw(kreis);
draw((-4,0)--(5,0));

draw("\(\rho\)", O--A, W);
dot("\(A\)", A, E);

for(int i=0; i<=n-1; ++i)
{
  draw(O--polar(4,phi/n + i*2*pi/n));
  label("\(z_"+string(i+1)+"\)", polar(4.5,phi/n + i*2*pi/n));
}

markangle(radius=40, A, O, (1,0));
label("\(\varphi\)", (1.2,0.8));

markangle(radius=70, polar(1, phi/n), O, (1,0));
label("\(\frac{\varphi}{n}\)", (3,0.3));
