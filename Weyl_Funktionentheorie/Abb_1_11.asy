size(7cm,0);
import solids;
defaultpen(font("OT1","cmr","m","n")+fontsize(10));
//---------------------------------------------------------
// O ist der Ursprung
currentprojection = orthographic(1,-7,2);
real rho=2, r=1, r1=rho*rho/r, phi=3*pi/8, theta=-pi/4;
triple P=polar(r,phi,theta), P1=polar(r1,phi,theta);
revolution direktrix = sphere(O,rho);
//---------------------------------------------------------
draw(O--(3,0,0), EndArrow); label("$x$",(3,0,0),X);
draw(O--(0,3,0), EndArrow); label("$y$",(0,3,0),Y);
draw(O--(0,0,2.5), EndArrow); label("$z$",(0,0,2.5),Z);
//---------------------------------------------------------
draw(O--P1);
dot("$P$",P, N);
dot("$P'$",P1);
direktrix.draw(3);
draw("$\rho$", O--polar(rho,pi/2, -5*pi/8));
draw(O--(P1.x,P1.y,0), dashed);
draw(P1--(P1.x,P1.y,0), dashed);
draw(P--(P.x,P.y,0), dashed);

dot(polar(rho,phi,theta));
draw(arc(O, (0,0,rho), polar(rho,pi/2,theta)));
