usepackage("amsfonts");
size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real eins=1.5;
//guide L=(1,3)..(2,0.5)..(3,0)..(6,1)..(8,3)..(9,5)
//      ..(8,6.5)..(6,6.5)..(4,6)..(3,5.5)..cycle;
guide L=(2,4)..(2,2)..(3,1.5)..(6,2)..(8,3)..(9,5)
      ..(8,6.5)..(6,6.5)..(4,5.7)..(3,5.2)..cycle;
pair p=point(L, 4);
pair t=dir(L, 4);
pair n=rotate(90)*t;
pair u=(0,3.2);
pair uv=n*abs(u)*cos(angle(n)-angle(u));
//---------------------------------------------------------
//Kurve mit Beschriftung
draw(L);
draw(shift((0,0.3))*subpath(L, 7.5, 9), EndArrow);
label("\(\mathfrak{L}\)", point(L, 2), N);

//Vektoren
draw("\(\vec{w}\)", p -- p+u, EndArrow);
draw(p -- p+2*t);
draw(p -- p+uv, SW);
draw("\(\vec{w}_\nu\)", shift(-0.3*t)*(p -- p+uv), SW, EndArrow);
draw(p+uv-0.3*t -- p+u);

//Beschriftung
label("\(0\)", p, SE);
draw(p -- p+t*eins, EndBar);
label("\(1\)", p+t*eins, SE);
draw(p -- p+n*eins, EndBar);
label("\(1\)", p+n*eins, NE);
label("\(l\)", p-0.3*t, S);
