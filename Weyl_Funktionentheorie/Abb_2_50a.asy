size(11cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair O=(0,0);
real a,b=0.89;
real f(real x) {return sqrt(b^2/a^2 * x^2 - b^2);}
real g(real x) {return -sqrt(b^2/a^2 * x^2 - b^2);}
guide ellipse(real a) {return (0,a)..(sqrt(1+a^2),0)..(0,-a)..(-sqrt(1+a^2),0)..cycle;}
//---------------------------------------------------------
draw((-2,0)--(-1,0));
draw((1,0)--(2,0));
draw((0,-1)--(0,1), 0.4mm+black);
dot("\(-1\)", (-1,0), S);
dot("\(1\)", (1,0), S);
draw((-1.9,0.05)--(-1.1,0.05), EndArrow);
draw((-1.9,-0.05)--(-1.1,-0.05), BeginArrow);
draw((1.9,0.05)--(1.1,0.05), EndArrow);
draw((1.9,-0.05)--(1.1,-0.05), BeginArrow);
label("\(4\)", (-1.1,0), 2*N);
label("\(0\)", (1.9,0), 2*N);
label("\(2\)", (0,1), W);
label("\(3\)", (-1,1.3), N);
label("\(1\)", (1,1.3), N);
draw((-1,0)--(1,0), dotted+0.5mm);

for(real i=0.7; i<=1.3; i+=0.6)
{
  draw(point(ellipse(i),0.5)..point(ellipse(i),0.2)..point(ellipse(i),0)..
       point(ellipse(i),3)..point(ellipse(i),2)..
       point(ellipse(i),1.8)..point(ellipse(i),1.5), dashed);
  draw(point(ellipse(i),1.5)..point(ellipse(i),1)..
       point(ellipse(i),0.5), dashed, Arrows);
}
label("\(a_1\)", (1,0.4), NE);
label("\(b_1\)", (1.3,0.8), NE);
label("\(a\)", (1,-0.4), SE);
label("\(b\)", (1.3,-0.8), SE);

a = sqrt(1-b^2);
draw(graph(f, a, 2, operator ..));
draw(graph(f, -2, -(a+0.000001), operator ..));
draw(graph(g, a, 2, operator ..));
draw(graph(g, -2, -(a+0.000001), operator ..));

clip((-2,-1.6)--(-2,1.6)--(2,1.6)--(2,-1.6)--cycle);
