size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), z=polar(3/2,pi/6);
pair z1=polar(2/3,-pi/6), z2=polar(2/3,pi/6);
path kreis=circle(O,1);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);
draw("\(1\)", O--polar(1,5*pi/6));
dot("\(0\)", O, SW);
label("", (-1.1,1.2));

draw(kreis);
draw(z1--z2);
draw(z2--z);

dot("\(\frac{1}{z}\)", z1, W);
dot(z2);
dot("\(z\)", z, N);
