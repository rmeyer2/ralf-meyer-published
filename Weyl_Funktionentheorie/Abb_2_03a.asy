size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("gestrichelt", hatch(4mm,NE));
//---------------------------------------------------------
filldraw(circle((0,0),1),pattern("gestrichelt"));
dot("$z$", (2,0));
