size(7cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z=(2,2), xi1=(2.5,1.25), xi2=(0.5,1);
pair x1=(z.x-z.y*(xi1-z).x/(xi1-z).y,0);
pair x2=(z.x-z.y*(xi2-z).x/(xi2-z).y,0);
//---------------------------------------------------------
xaxis();
yaxis();

draw(z--x1);
draw(z--x2);
draw(xi1--xi2);
dot("$z$", z, NE);
dot("$\xi_1$", xi1, E);
dot("$\xi_2$", xi2, S);
markangle(radius=30, z, x1,(x1.x+1,x1.y));
markangle(radius=30, z, x2,(x1.x+1,x1.y));
label("$\beta$", x1, (1.5,2));
label("$\alpha$", x2, (5,1.5));
