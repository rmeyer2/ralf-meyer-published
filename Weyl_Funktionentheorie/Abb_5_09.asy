size(0,1.2cm);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((0,1)--(5,1));
draw((0,0)--(5,0));
draw((0,-1)--(5,-1));
label("\(I\)", (0,1), W);
label("\(II\)", (0,0), W);
label("\(III\)", (0,-1), W);
