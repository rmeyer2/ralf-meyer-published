size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//------------------------------------------------------
pair x1=(-1,0), x2=(1,0);
pair O=(0,0);

path kreis1(real t, real phi1=0, real phi2=360)
{
  return arc((0,t),sqrt((t)^2 + x1.x^2), phi1, phi2);
}
path kreis2(real t, real phi1=0, real phi2=360)
{
  pair s = intersectionpoints(kreis1(0.5),O--polar(10,t))[0];
  pair x = intersectionpoints((10,0)--(-10,0),s-(-10,10*s.x/(s.y-0.5))--s+(-10,10*s.x/(s.y-0.5)))[0];
  return arc(x,abs(x-s),phi1,phi2);
}
//------------------------------------------------------
yaxis();
dot("\(\xi_1\)", x1, W); dot("\(\xi_2\)", x2, E);
draw(x1--x2);
draw(kreis2(5*pi/6));
draw(kreis2(4.5*pi/6));
draw(kreis2(1.5*pi/6));
draw(kreis2(1*pi/6));
draw(kreis2(4*pi/6, -90, 90));
draw(kreis2(2*pi/6, 90, 270));
draw(kreis2(3.5*pi/6, -45, 45));
draw(kreis2(2.5*pi/6, 135, 225));
