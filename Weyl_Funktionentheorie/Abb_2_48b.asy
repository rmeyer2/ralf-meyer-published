size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
path R=(-3,-2)--(-3,2)--(3,2)--(3,-2)--cycle;
//---------------------------------------------------------
filldraw(R, pattern("strich"), invisible);
xaxis("\(x\)",xmin=-3,xmax=3.5,EndArrow);
yaxis("\(y\)",ymin=-3.5,ymax=3.5,EndArrow);
draw((-3,-2)--(3,-2));
draw((-3,2)--(3,2));
//xaxis(xmin=-3, xmax=3, axis=YEquals(2));
//xaxis(xmin=-3, xmax=3, axis=YEquals(-2));
//xaxis(xmin=-3, xmax=3);
//yaxis(ymin=-2, ymax=2);
filldraw(circle((0.4,-0.4), 0.3), white, invisible);
dot("\(0\)", (0,0), SE);
label("\(\pi\)", (0,2),NW);
label("\(-\pi\)", (0,-2),SW);
//label("", (0,-2.7));
