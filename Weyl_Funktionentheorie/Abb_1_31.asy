size(9cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), z1=(1,2), z2=(2,1), z3=(2.5,2.5);
pair x12=(z1.x-(z2.x-z1.x)/(z2.y-z1.y)*z1.y,0);
pair x23=(z2.x-(z3.x-z2.x)/(z3.y-z2.y)*z2.y,0);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);

draw(O--z1);
draw(O--z2);
draw(O--z3);
draw("\(\rho\)", z1--z2, (-2,4), 0.3mm+black);
draw(z2--x12);
draw("\(r\)", z3--z2, E, 0.3mm+black);
draw(z2--x23);

dot("\(z_1\)", z1, W);
dot("\(z_2\)", z2);
dot("\(z_3\)", z3);

markangle(z1, x12, (x12.x+1,0));
label("\(\varphi\)", x12, (1,2));
markangle(z2, x23, (x23.x+1,0));
label("\(\vartheta\)", x23, (3,2));
markangle("\(\varphi-\vartheta\)", z1, z2, z3);
label("", (x12.x+0.5,0));
