size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real x1=0, x2=3, y1=0, y2=2;
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);
label("", (2,1.2));
label("", (-2,-1.2));

draw((0.7,0.5)--(0.7,1.5), EndArrow);
draw((0.7,-0.5)--(0.7,-1.5), EndArrow);
draw((-0.7,0.5)--(-0.7,1.5), EndArrow);
draw((-0.7,-0.5)--(-0.7,-1.5), EndArrow);;
