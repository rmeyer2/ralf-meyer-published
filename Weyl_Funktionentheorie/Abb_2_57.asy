size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
for(int i=-5; i<=3; i+=2)
{
  draw((-9,i)--(0,i));
  if(i != 1 && i != -1)
    label("\("+string(i)+"\pi\)", (2,i), W);
}
label("\(\pi\)", (2,1), W);
label("\(-\pi\)", (2,-1), W);
draw((-9,0)--(0,0), 0.4mm+black);
label("\(0\)", (2,0), W);
