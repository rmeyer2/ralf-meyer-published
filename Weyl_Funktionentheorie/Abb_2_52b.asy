size(0,3.5cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
//---------------------------------------------------------
filldraw((0,-pi)--(0,pi)--(pi,pi)--(pi,-pi)--cycle,
         pattern("strich"), invisible);
draw((0,-pi)--(0,pi));
draw((pi,-pi)--(pi,pi));
draw((0,0)--(pi,0));
label("\(0\)", (0,0), W);
label("\(\pi\)", (pi,0), E);
