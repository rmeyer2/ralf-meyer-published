size(8cm,0);
import solids;
defaultpen(font("OT1","cmr","m","n")+fontsize(10));
//---------------------------------------------------------
// O ist der Ursprung
currentprojection = orthographic(1,-7,2);
triple M=(0,0,2);
revolution direktrix = sphere((0,0,1),1);
revolution kugel = sphere(M,2);
//---------------------------------------------------------
draw(O--(2.5,0,0), EndArrow); label("$x$",(2.5,0,0),X);
draw(O--(0,3.5,0), EndArrow); label("$y$",(0,3.5,0),Y);
draw(O--(0,0,4.5), EndArrow); label("$z$",(0,0,4.5),Z);
dot("$0$", O, S);
//---------------------------------------------------------
kugel.draw(3);
direktrix.draw(3);
dot("\(N\)", M, NE);
dot((0,0,1));
draw("\(\frac{1}{2}\)", (1.5,0,1)--(1.5,0,2), E, Bars);
draw(M--(1.5,0,2), dashed);
draw((0,0,1)--(1.5,0,1), dashed);
