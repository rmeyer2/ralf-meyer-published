defaultpen(font("OT1","lmr","m","n")+fontsize(10));

size(3.5cm,0);
// v1
draw( "$v_1$", (0,0) -- (0.2,0.8), EndArrow);
// v2
draw( "$v_2$", (0.2,0.8) -- (1,1), EndArrow);
// v1+v2
draw( "$v_1 + v_2$", (0,0) -- (1,1), EndArrow);
