size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), xy=(2,3/2), u=(-1,4/3);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);
label("", (2,1.2));

draw(O--xy);
draw("\(\vec{u}\)", xy--u+xy, E, EndArrow);
dot("\(x,y\)", xy, E);
