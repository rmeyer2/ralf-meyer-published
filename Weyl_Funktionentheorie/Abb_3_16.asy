usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((0,0)--(4,0)--(4,2)--(0,2)--cycle);
draw((0,0)--(3.2,0)--(3.2,1.2), 0.4mm+black);
dot("\(z\)", (3.2,1.2), NE);
label("\(a,b\)", (0,0), S);
label("\(\mathfrak{L}_z\)", (2,0), N);
