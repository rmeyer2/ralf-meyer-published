size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real x1=1, x2=6, y0=2;
pair u=(1,2);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);
label("", (7,0));

filldraw((x1+1,y0)--u+(x1+1,y0)--u+(x1+2,y0)--(x1+2,y0)--cycle, lightgrey, invisible);

draw((x1,0)--(x1,y0));
draw((x1,y0)--(x2,y0));
draw((x2,y0)--(x2,0));
draw((x1,0)--(x2,0), Bars);
label("\(x_1\)", (x1,-0.1), S);
label("\(x_2\)", (x2,-0.1), S);

draw("\(\vec{u}\)", (x1+1,y0)--u+(x1+1,y0), W, EndArrow);
draw(u+(x1+1,y0)--u+(x1+2,y0));
draw(u+(x1+2,y0)--(x1+2,y0));
draw("\(\textup{d}x\)", (x1+1,y0)--(x1+2,y0), S, Bars);

draw((x1,y0)--(0,y0), dashed);
label("\(y_0\)", (0,y0), W);
