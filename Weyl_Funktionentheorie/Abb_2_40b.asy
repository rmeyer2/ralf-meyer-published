size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
real m=3;
//---------------------------------------------------------
for(real r=1; r<=m; ++r)
{
  draw(circle(O, exp(r)));
  //label(string(r^2), (r^2,0), E-(0.8,0));
  draw(arc(O, exp(r)+0.8, 35/r, 150/r), EndArrow);
}
for(real d=0; d<2*pi; d+=pi/20)
{
  draw(polar(exp(1),d)--polar(exp(m),d), 0.05mm+black);
}
