usepackage("amsfonts");
size(10cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path g=(0,0)..(2,2)..(3,2)..(5,3)..(7,0)..(5,-1)..(3,-1)..cycle;
path s=(-1,1)..(0,0)..(1,1)..(3,1)..(5,1)..(7,0)..(8,1);
pair P=(4,2), Q=(1,0), Q1=(5,0);
path L=P..(4,1)..(3,0)..(2,1.5)..Q;
path L1=P..(4.5,1.5)..(4.5,0.5)..(6,1)..(6,0)..Q1;
real eins=intersections(s,L)[2][0];
real zwei=intersections(s,L)[1][0];
real eins1=intersections(s,L1)[0][0];
real zwei1=intersections(s,L1)[2][0];
path u;
//---------------------------------------------------------
draw(g);
draw(s);
label("G", (6,2));
clip(g);

dot("\(P\)", P, NE);
dot("\(Q\)", Q, S);
dot("\(Q'\)", Q1, SW);

draw(L);
draw(L1);

draw(subpath(s,eins,zwei), Bars);
label("\(1\)", point(s,eins), NW+(-1,1));
label("\(2\)", point(s,zwei), NW-(1,0));

draw(subpath(s,eins1,zwei1), Bars);
label("\(1'\)", point(s,eins1), NE+(1,1));
label("\(2'\)", point(s,zwei1), NE+(1,0));

for(real i=zwei; i<=eins; i+=0.01)
{
  u=u--(point(s,i)+0.1*(rotate(90)*dir(s,i)));
}
draw(u, dashed);

u = nullpath;
for(real i=eins1; i<=zwei1; i+=0.01)
{
  u=u--(point(s,i)+0.1*(rotate(90)*dir(s,i)));
}
draw(u, dashed);

u = nullpath;
for(real i=zwei; i<=zwei1; i+=0.01)
{
  u=u--(point(s,i)-0.1*(rotate(90)*dir(s,i)));
}
draw(u, dashed);
