usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair z0=(0,0), z=(-1,-3);
path K=circle(z0,5);
path K1=circle(z0,4.5);
path k=circle(z0,0.5);
path k1=circle(z0,1);
path KK=circle(z0, 3.5);
//---------------------------------------------------------
draw(K);
filldraw(K1, pattern("strich"));
draw(KK, 0.4mm+black);
filldraw(circle((0,3), 0.4), white, invisible);
label("\(\mathfrak{K}\)", (0,3));
filldraw(k1, white);
draw(k);
dot(z0);
