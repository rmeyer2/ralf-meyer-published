size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0), z1=(11,5);
path c0=circle(z0,2);
path c1=circle(z1,2);
path p=z0..(3,2)..(6,3)..(9,4)..z1;
//---------------------------------------------------------
dot("\(z_0\)", z0, SW);
dot("\(z_1\)", z1, NE);
draw(c0);
draw(c1);
draw(p);
draw(circle(point(p, arctime(p,1)), 1.5));
draw(circle(point(p, arctime(p,3)), 1.2));

for(int i=1; i<=11; ++i)
{
  draw(subpath(p, arctime(p,i-0.5), arctime(p,i)), EndBar);
}
