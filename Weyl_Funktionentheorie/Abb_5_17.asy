usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path L=(0,0)..(1,1)..(2,1)..(3,1)..(4,2);
//---------------------------------------------------------
draw(circle((0,0), 1));
dot("\(0\)", (0,0), SW);
draw(L);
draw(subpath(L, 1, 2), EndBar);
label("\(t\)", point(L,2), SW);
label("\(\mathfrak{L}\)", point(L,4), SE);

