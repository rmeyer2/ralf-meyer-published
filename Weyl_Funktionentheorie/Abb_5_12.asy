size(7cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path g=(0,1)--(0,-1);
path e=(0,1.5)..(-0.7,0)..(0,-1.5)..(0.7,0)..cycle;
path k=(0.5,1)..(1.5,1.2)..(2.5,0)..(2,-1.2)..(1.5,-1.3)
     ..(0.2,-1.4)..(-0.2,-1.3)..(-0.4,-0.8)..(-0.3,0)
     ..(0,0.5)..(0.7,0.7);
path p=(2,0)..(1.5,0.7)..(0.8,0)..(0.8,-1)..(0,-3)
     ..(-1,-1)..(-0.8,1)..(0,2)..(0.6,1.4)..(1.5,1.4)
     ..(2,1.4)..(3,0)..(2.5,-2.5)..(0,-3.5)..(-1.5,-2)
     ..(-1.2,0)..(0,0.8)..(0.2,1)..(0,1.2)..(-0.3,0.5)
     ..(-0.3,0)..(0.8,-1)..cycle;
//---------------------------------------------------------
draw(g);
dot("\(+\textup{i}\)", (0,1), N);
dot("\(-\textup{i}\)", (0,-1), (0,-0.05));
draw((1,0)--(4,0), EndArrow);
dot("\(+1\)", (1,0), S);

draw(e);
draw(subpath(k, intersections(k,e)[0][0], 2));
draw(subpath(k, 2, intersections(k,g)[0][0]), dashed);
draw(subpath(k, intersections(k,g)[0][0], intersections(k,e)[2][0]));

draw(subpath(p, 11, intersections(p,g)[0][0]), gray+dashed);
draw(subpath(p, intersections(p,g)[0][0], intersections(p,g)[1][0]), gray);
draw(subpath(p, intersections(p,g)[1][0], 22), gray+dashed);
draw(subpath(p, 22, 33), gray);
