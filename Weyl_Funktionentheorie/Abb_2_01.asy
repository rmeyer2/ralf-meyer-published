size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real f(real t) {return -2*t^2;}
guide G=shift((0.1,0))*rotate(90)*graph(f, -0.05, 0.4, operator ..);
pair P=point(G,50), d=dir(G,50);
path T=shift(P)*(-0.1*d--0.3d);
//---------------------------------------------------------
xaxis("$$", EndArrow);
yaxis("$$", EndArrow);

draw(G);
draw(T);
dot("$P$", P, NW);
label("$x,y$", P, SE);
