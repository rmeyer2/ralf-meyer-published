size(4cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair eins=(-1,0), zwei=(0,1), drei=(2,0), vier=(0,-2);
real mq=(drei.y-zwei.y)/(drei.x-zwei.x);
real mr=(vier.y-drei.y)/(vier.x-drei.x);
real ms=(eins.y-vier.y)/(eins.x-vier.x);
//---------------------------------------------------------
draw("$p$", eins--zwei, NW);
draw("$q$", zwei--drei, NE);
draw("$r$", drei--vier, SE);
draw("$s$", vier--eins, SW);

label("$1$", eins, W);
label("$2$", zwei, N);
label("$3$", drei, E);
label("$4$", vier, S);

draw(arc(zwei, 0.5, 225,360*(1+atan(mq)/(2*pi))));
draw(arc(vier, 0.5, 360*atan(mr)/(2*pi), 360*atan(ms)/(2*pi)-180));
