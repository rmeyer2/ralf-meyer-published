size(10cm,0);
import contour;
//--------------------------
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
real f1(real x, real y) {return ((x-1)^2+y^2)*((x+1)^2+y^2);}
real f2(real x, real y) {return log(((x-1)^2+y^2)*((x+1)^2+y^2));}

guide[][][] c1, c2;
guide g1, g2;

real time;
pair p,d;
int n=20;
//--------------------------
for(int i=0; i<n; ++i)
{
  c1[i] = contour(f1,(-5,-5),(5,5),new real[] {4-4*i/n}, 200, 200, operator ..);
}

for(real t=-115; t<=145; t+=10)
{
  g1 = point(c1[0][0][0], t);
  time = t;
  for(int i=0; i<n-1; ++i)
  {
    d=rotate(-90)*dir(c1[i][0][0], time)/2;
    p=point(c1[i][0][0], time);
    time = intersections(c1[i+1][0][0], p--p+d)[0][0];
    g1 = g1..point(c1[i+1][0][0], time);
  }
  g1=g1..(-1,0);
  draw(g1);
  draw(reflect((0,0),(0,1))*g1);
}
