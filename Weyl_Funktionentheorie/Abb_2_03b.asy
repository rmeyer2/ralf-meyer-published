size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((-2,-1)..(-1.5,0)..(-1.5,1.5)..(1.5,2)..(2,1)..(3.5,-0.5)..(1,-1.5)..cycle);
dot((1.3,1.3));
draw(circle((1.3,1.3), 0.3));
