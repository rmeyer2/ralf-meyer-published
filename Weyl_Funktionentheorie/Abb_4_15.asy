usepackage("amsfonts");
size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path g=(0,1)..(1,0)..(5,-1)..(10,0)..(10,6)..(4,6)..cycle;
path L=(4,5)..(9,4)..(8,0)..(4,1)..cycle;
//---------------------------------------------------------
draw(g);
label("G", (10,3));
draw(L);
draw(subpath(L,0.5,1), BeginArrow);
label("\(\mathfrak{L}\)", point(L,1), NE);

dot((6,3));
draw(arc((6,3),0.5,90,450), ArcArrow);

dot((4.2,2));
draw(arc((4.2,2),0.5,90,450), ArcArrow);

dot((7,0.5));
draw(arc((7,0.5),0.5,90,450), ArcArrow);

dot((5,5));
draw(arc((5,5),0.5,90,450), ArcArrow);

dot((7,4));
draw(arc((7,4),0.5,90,450), ArcArrow);

dot((8,3));
draw(arc((8,3),0.5,90,450), ArcArrow);
