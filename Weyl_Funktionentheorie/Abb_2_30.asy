size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
//---------------------------------------------------------
draw(arc(O,1,10,80));
draw(O--polar(1,pi/6), EndArrow);
draw(O--polar(1,2*pi/6), EndArrow);
draw(arc(O,1,30,60), 0.4mm+black);
label("\(r\textup{d}\varphi\)", polar(1,3*pi/12), E);
