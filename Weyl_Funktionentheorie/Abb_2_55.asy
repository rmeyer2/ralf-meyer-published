size(0,3cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
pair O=(0,0);
//---------------------------------------------------------
filldraw((-1,-3)--(-1,3)--(1,3)--(1,-3)--cycle,
          pattern("strich"), invisible);
filldraw((-0.2,1)--(-0.2,3)--(0.2,3)--(0.2,1)--cycle,
          white, invisible);
filldraw((-0.2,-1)--(-0.2,-3)--(0.2,-3)--(0.2,-1)--cycle,
          white, invisible);
draw((0,1)--(0,3), 0.4mm+black);
draw((0,-1)--(0,-3), 0.4mm+black);
filldraw(circle((0.7,1), 0.5), white, invisible);
filldraw(circle((0.7,-1), 0.5), white, invisible);
label("\(+\textup{i}\)", (0,1), E);
label("\(-\textup{i}\)", (0,-1), E);
