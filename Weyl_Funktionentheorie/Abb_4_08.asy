usepackage("amsfonts");
size(7cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
guide g=rotate(90)*((0,-2)..(-1,-1)..(-1,1)..(0,2)
  ..(0.5,0.8)..(0,0.5)..(-0.5,0)..(0,-0.5)..(0.5,-0.8)..cycle);
guide K=arc((0.5,1.5), 1.8, 200, 340);
pair z0=(1.1,0.22);
//---------------------------------------------------------
filldraw(g, pattern("strich"), invisible);
clip(K--cycle);

draw(g);
draw(K);
filldraw(circle((1.21,0.31), 0.1), white, invisible);
dot("\(z_0\)", z0, (0.15,0.15));
draw(circle(z0, 0.35));
label("G", (-1,-0.5));
label("\(\mathfrak{K}\)", point(K,0), W);
