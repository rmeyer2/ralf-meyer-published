size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real a=1, b=-1;
pair u=(a,-b), O=(0,0);
//---------------------------------------------------------
for(int i=-2; i<=2; ++i)
{
  draw(shift((i,0))*(O--u), EndArrow);
  draw(shift((i,0))*(O--2*u), EndArrow);
  draw(shift((i,0))*(O--3*u), EndArrow);
}
label("", (0,-1));
