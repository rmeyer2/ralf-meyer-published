size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
guide g=shift((2,3))*rotate(-90)*
  ((-0.3,0)..(-0.3,2)..(0,2.2)..(1.2,2.5)..
  (2.5,0)..(1.2,-2.5)..(0,-2.2)..(-0.3,-2)..cycle);
//---------------------------------------------------------
xaxis("\(x\)", xmin=-1, xmax=5, EndArrow);
yaxis("\(y\)", ymin=-0.3, ymax=4, EndArrow);

draw(g);
dot("\(z_0\)", point(g,3), E);
