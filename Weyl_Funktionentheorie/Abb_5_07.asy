size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real f1(real x) {return sqrt(x^2-1);}
real f2(real x) {return -sqrt(x^2-1);}
//---------------------------------------------------------
draw((-3,0)--(3,0));
draw((-1,0)--(1,0), Bars);
label("\(-1\)", (-1,0), NE);
label("\(+1\)", (1,0), NW);

label("\(I^+\)", (0,-1));
label("\(I^-\)", (0,1));
label("\(II^-\)", (2,-1));
label("\(II^+\)", (2,1));
label("\(I^+\)", (0,-1));
label("\(III^-\)", (-2,-1));
label("\(III^+\)", (-2,1));

draw(graph(f1, 1.00001, 2, operator ..));
draw(graph(f2, 1.00001, 2, operator ..));
draw(graph(f1, -1.00001, -2, operator ..));
draw(graph(f2, -1.00001, -2, operator ..));
