size(0,5cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(1mm, NW));
pair O=(0,0);
//---------------------------------------------------------
draw(O--(3,0), 0.4mm+black);

draw(polar(3,pi/4)--polar(3,5*pi/4));
draw(polar(3,pi/2)--polar(3,3*pi/2));
draw(polar(3,3*pi/4)--polar(3,-pi/4));

draw(circle(O,1/2), dashed);
draw(circle(O,1), dashed);
draw(circle(O,2), dashed);

//filldraw((-3,0)--(-3,0.2)--(0,0.2)--(0,0)--cycle,
//          pattern("strich"), invisible);
filldraw((-3,-0.2)--(-3,0.2)--(0,0)--cycle,
          pattern("strich"), invisible);
