\select@language {ngerman}
\select@language {english}
\contentsline {chapter}{Preface}{v}
\select@language {ngerman}
\contentsline {chapter}{Zur Entstehung dieses Buches}{ix}
\contentsline {chapter}{Einleitung: Vom Begriff der Funktion}{1}
\contentsline {chapter}{\numberline {1}Stereographische Projektion und die linearen Substitutionen}{3}
\contentsline {section}{\numberline {1.1}Einf\IeC {\"u}hrung der komplexen Zahlen}{3}
\contentsline {subsection}{\numberline {1.1.1}Geometrische Interpretation der komplexen Zahlen und der Grundrechnungsarten nach Gau\IeC {\ss }}{4}
\contentsline {subsection}{\numberline {1.1.2}Die Methode der stereographischen Projektion}{7}
\contentsline {section}{\numberline {1.2}Die Transformation durch reziproke Radien}{11}
\contentsline {section}{\numberline {1.3}Gruppencharakter der linearen Substitution}{15}
\contentsline {section}{\numberline {1.4}Die linearen gebrochenen Substitutionen und die Kreisverwandtschaft}{18}
\contentsline {section}{\numberline {1.5}Winkeltreue der stereographischen Projektion}{26}
\contentsline {section}{\numberline {1.6}Kinematische Deutung der linearen ganzen Substitution}{30}
\contentsline {subsection}{\numberline {1.6.1}Die elliptische Substitution}{31}
\contentsline {subsection}{\numberline {1.6.2}Die hyperbolische Substitution}{32}
\contentsline {subsection}{\numberline {1.6.3}Die allgemeine ganze lineare Substitution}{33}
\contentsline {subsection}{\numberline {1.6.4}Der parabolische Fall}{35}
\contentsline {section}{\numberline {1.7}Die kinematische Deutung der linearen gebrochenen Substitution}{37}
\contentsline {section}{\numberline {1.8}Grundlagen der Lie-Theorie}{42}
\contentsline {section}{\numberline {1.9}Invarianz des Doppelverh\IeC {\"a}ltnisses}{45}
\contentsline {section}{\numberline {1.10}Ein \IeC {\"U}bungsblatt zur Vorlesung}{49}
\contentsline {chapter}{\numberline {2}Begriff der analytischen Funktion}{55}
\contentsline {section}{\numberline {2.1}Bedingungen der Konformit\IeC {\"a}t einer Abbildung}{55}
\contentsline {section}{\numberline {2.2}Begriff der analytischen Funktion}{61}
\contentsline {subsection}{\numberline {2.2.1}Einfache Beispiele: Polynome und rationale Funktionen}{62}
\contentsline {subsubsection}{Stetigkeit der Potenzfunktion}{62}
\contentsline {subsubsection}{Differenzierbarkeit der Potenzfunktion}{62}
\contentsline {subsection}{\numberline {2.2.2}Der allgemeine Begriff}{64}
\contentsline {subsubsection}{Beispiele}{70}
\contentsline {section}{\numberline {2.3}Rationale Funktionen als konforme Abbildungen}{71}
\contentsline {section}{\numberline {2.4}Cauchy-Riemannsche Differentialgleichungen und Str\IeC {\"o}mungstheorie}{86}
\contentsline {subsection}{\numberline {2.4.1}Inkompressibilit\IeC {\"a}t der Str\IeC {\"o}mung}{87}
\contentsline {subsection}{\numberline {2.4.2}Wirbelfreiheit der Str\IeC {\"o}mung}{91}
\contentsline {subsection}{\numberline {2.4.3}Str\IeC {\"o}mungskurven}{96}
\contentsline {paragraph}{Beispiel 5}{102}
\contentsline {section}{\numberline {2.5}Formale Erzeugungsprinzipien analytischer Funktionen}{108}
\contentsline {section}{\numberline {2.6}Exponentialfunktion und Logarithmus}{111}
\contentsline {section}{\numberline {2.7}Die trigonometrischen Funktionen und ihre Umkehrungen}{124}
\contentsline {section}{\numberline {2.8}Die allgemeine Potenz~$z^\alpha $}{131}
\contentsline {section}{\numberline {2.9}Historische Bemerkungen}{137}
\contentsline {chapter}{\numberline {3}Der Cauchysche Integralsatz}{141}
\contentsline {section}{\numberline {3.1}Vom Begriff der Kurve oder des Weges}{141}
\contentsline {section}{\numberline {3.2}Begriff des Kurvenintegrals}{146}
\contentsline {section}{\numberline {3.3}Erster Beweis des Cauchyschen Integralsatzes}{153}
\contentsline {section}{\numberline {3.4}Anwendung des Cauchyschen Integralsatzes}{170}
\contentsline {section}{\numberline {3.5}Zweiter Beweis des Cauchyschen Integralsatzes}{174}
\contentsline {chapter}{\numberline {4}Theorie der eindeutigen analytischen Funktionen}{181}
\contentsline {section}{\numberline {4.1}Die Cauchysche Integralformel}{181}
\contentsline {section}{\numberline {4.2}Die Potenzentwicklung einer regul\IeC {\"a}r analytischen Funktion}{182}
\contentsline {section}{\numberline {4.3}Die Potenzreihen im komplexen Gebiet}{189}
\contentsline {subsection}{\numberline {4.3.1}Formale Erzeugungsprinzipien analytischer Funktionen}{197}
\contentsline {section}{\numberline {4.4}Weitere unmittelbare Anwedungen\\der Cauchyschen Integrationsformel}{203}
\contentsline {section}{\numberline {4.5}Isolierte Singularit\IeC {\"a}ten analytischer Funktionen}{205}
\contentsline {subsection}{\numberline {4.5.1}Hebbare Singularit\IeC {\"a}ten}{206}
\contentsline {subsection}{\numberline {4.5.2}Polstellen}{207}
\contentsline {subsection}{\numberline {4.5.3}Wesentliche Singularit\IeC {\"a}ten}{209}
\contentsline {subsection}{\numberline {4.5.4}Laurentreihen}{210}
\contentsline {section}{\numberline {4.6}Die Funktionen, die die einfachsten Singularit\IeC {\"a}ten besitzen}{214}
\contentsline {section}{\numberline {4.7}Anwendungen des Cauchyschen Residuensatzes}{218}
\contentsline {chapter}{\numberline {5}Mehrdeutige analytische Funktionen}{233}
\contentsline {section}{\numberline {5.1}Die Riemannsche Fl\IeC {\"a}che}{233}
\contentsline {section}{\numberline {5.2}Funktionentheorie auf der Riemannschen Fl\IeC {\"a}che}{242}
\contentsline {section}{\numberline {5.3}Der Weierstra\IeC {\ss }sche Begriff der analytischen Fortsetzung}{252}
\contentsline {chapter}{Literatur}{265}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
\select@language {english}
\select@language {ngerman}
