size(10cm,0);
import three;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("gestrichelt", hatch(2.5mm, N));
transform3 t=shift((6,0,0.3));

path3 naht=t*((2*cos(4*pi/3),2*sin(4*pi/3),-3)--(0,0,3));
path3 k1=t*((2,0,-3)--(0,0,3));
path3 k2=t*((-2,0,-3)--(0,0,3));
triple p=point(naht, 0.4);
triple p1=point(k1, 0.5);
triple p2=point(k2, 0.3);
//---------------------------------------------------------
currentprojection = orthographic((0,0,1));

draw((-1,0,0)--(3,0,0));
draw((0,-3,0)--(0,3,0));
draw((0,1.5,0)--(1,2.5,0));
draw((0,-1.5,0)--(1,-0.5,0));
dot((0,1.5,0));
dot((0,-1.5,0));
filldraw((0,-3,0)--(0,3,0)--(3,3,0)--(3,-3,0)--cycle,
     pattern("gestrichelt"), invisible);
//---------------------------------------------------------
currentprojection = orthographic((0,-7,2));

draw(t*arc((0,0,-3), (2,0,-3), (-2,0,-3), Z, CW));
draw(t*arc((0,0,-3), (2,0,-3), (-2,0,-3), Z), dashed);
draw(k1);
draw(k2);
draw(naht);
dot(p);
draw(point(p1..p..p2, 0.5)..p..point(p1..p..p2, 1.5));
