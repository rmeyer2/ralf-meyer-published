size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real a;
real f(real x) {return a/x;}
//real g(real x) {return -a/x;}
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);

for(a=-4.5; a<=4.5; a+=1.5)
{
  if(a!=0)
  {
    draw(graph(f, 0.25, 4, operator ..));
    draw(graph(f, 1, 1.5, operator ..), EndArrow);
    draw(graph(f, -4, -0.25, operator ..));
    draw(graph(f, -1.5, -1, operator ..), BeginArrow);
    //draw(graph(g, 0.25, 4, operator ..));
    //draw(graph(g, -4, -0.25, operator ..));
  }
}

clip((-4,-4)--(-4,4)--(4,4)--(4,-4)--cycle);
