size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((0,-1)--(0,1));
dot("\(-\textup{i}\)", (0,-1), W);
dot("\(\textup{i}\)", (0,1), W);

dot("\(\frac{1}{2}\)", (0.5,0), S);
dot("\(1\)", (1,0), S);

draw((1,0)--(3,0));
draw((0.5,0)--(0.75,0));
draw(arc((1,0), 0.25, 0, 180));
