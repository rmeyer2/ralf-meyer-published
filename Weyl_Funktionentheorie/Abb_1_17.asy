size(6cm,0);
import graph;
import math;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), M=(0,1), No=(0,2), P=(2.5,0);
path C = circle(M,1);
pair P1=intersectionpoints(P--No,C)[0];
real m=-(P1.x)/(P1.y-1);
pair T=(P1.x-P1.y/m,0);
//---------------------------------------------------------
draw(O--(3,0), EndArrow); label("$x$", (3,0), S);
draw(O--(0,2.5), EndArrow); label("$y$", (0,2.5), W);
dot("$0$", O, SW);

draw(C);
draw(No--P);
draw(M--P1);
draw(P1+(1/m,1)--T);
label("$T$", T, S);

dot("$M$", M, W);
dot("$N$", No, SW);
dot("$P$", P, S);
dot("$P'$", P1, NE);

//\mathop{\measuredangle}
markangle(radius = 20, O,No,P1); //=MNP'
markangle(radius = 20, M,P1,No);
markangle(radius = 20, T,P1,P);
markangle(radius = 20, T,P,P1);
markangle(radius = 15, M,P1,T);
dot(P1+polar(0.2,(5*pi/4)+atan(-1/m)),linewidth(0.5mm));
