unitsize(0.7cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NW));
pair O=(0,0);
//---------------------------------------------------------
for(real i=-pi; i<=pi; i+=pi/4)
  draw((-4.2,i)--(4.2,i));
label("\(+\pi\)", (0,pi), N);
label("\(-\pi\)", (0,-pi), S);

draw((-4.2,0)--(4.2,0), 0.4mm+black);
label("\(0\)", O, SW);

draw((2,-pi)--(2,pi), dashed);
draw((0,-pi)--(0,pi), dashed);
draw((-2,pi)--(-2,-pi), dashed);

filldraw((-4.2,3*pi/4)--(-4.2,pi)--(4.2,pi)--(4.2,3*pi/4)--cycle,
          pattern("strich"), invisible);
