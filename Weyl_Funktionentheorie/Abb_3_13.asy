usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));

path gi=circle((1,0),0.5);
path go=yscale(0.8)*((-3,0)..(-2,4)..(0,5)..(2,4)..(4,0)..(3,-4)..(0,-4)..cycle);
path L=(-2,0)..(-0.5,1.2)..(1,2)..(2,0)..(1,-2)..(-0.5,-1.2)..cycle;
path q=(1,0)..(0,0)..(-1,0.5)..(-1.5,-1)..(-3,-3);
//---------------------------------------------------------
filldraw(go, pattern("strich"));
filldraw(gi, white);
draw(L, 0.4mm+black);
filldraw(circle(point(L,4)-(0,0.5), 0.4), white, invisible);
label("\(\mathfrak{L}\)", point(L,4), S);
draw(subpath(q, intersections(q,gi)[0][0], intersections(q,go)[0][0]), 0.4mm+black);
label("\(q\)", point(q,3.2), E);

filldraw(circle((-2,2), 0.4), white, invisible);
label("G", (-2,2));
