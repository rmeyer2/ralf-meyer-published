size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((-6,0)--(6,0));
draw((-2,0)--(2,0), Bars);
label("\(-2\)", (-2,0), S);
label("\(+2\)", (2,0), S);
label("\(+\)", (0,1));
label("\(-\)", (0,-1));
