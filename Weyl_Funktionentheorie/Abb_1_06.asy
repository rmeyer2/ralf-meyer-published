size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O = (0,0);
real r1 = 1.5, r2 = 2, phi1 = pi/12, phi2 = pi/8;
pair r_1 = polar(r1,phi1), r_2 = polar(r2,phi2), R = polar(r1*r2,phi1+phi2);

// Zeichne zuerst Schattierungen, um Linien nicht zu überzeichnen
fill( O -- r_1 -- (1,0) -- cycle, lightgray);
fill( O -- R -- r_2 -- cycle, lightgray);

//--Koordinatensystem--------------------------------------
xaxis("$x$",EndArrow);   //x-Achse
yaxis("$y$",EndArrow);   //y-Achse
dot("$0$", O, SW);           //"Ursprung"
tick((1,0),S);
label("$1$", (1,0), SW);  // Strich  bei 1
//---------------------------------------------------------

// r_1, r_2, R = r_1 * r_2
draw(O -- r_1,EndArrow);
label("$\xi$", r_1, E);
draw(O -- r_2, EndArrow);
label("$\beta$", r_2, E);
draw(O -- R, EndArrow);
label("$\alpha$", R,E);

// Hilfslinien ähnliche Dreiecke
draw((1,0) -- r_1, dashed);
draw(r_2 -- R, dashed);
