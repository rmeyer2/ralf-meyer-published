size(5cm,0);
import math;
import graph;
import geometry;
import markers;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
add("strich2", hatch(5mm, NW));
real a=3*pi/4, x=10;
pair O=(0,0);
path streifen=(-x,a)--(-x,a+2*pi)--(x,a+2*pi)--(x,a)--cycle;
//---------------------------------------------------------
xaxis("\(u\)", xmax=x+6, EndArrow);
xaxis(axis=YEquals(a+4*pi), xmin=-x, xmax=x);
xaxis(axis=YEquals(a+2*pi), xmin=-x, xmax=x);
xaxis(axis=YEquals(a), xmin=-x, xmax=x);
xaxis(axis=YEquals(a-2*pi), xmin=-x, xmax=x);
yaxis(axis=XEquals(x), ymin=a-2*pi, ymax=a+4*pi);

filldraw(streifen, pattern("strich"), invisible);
filldraw(shift((0,2*pi))*streifen, pattern("strich2"), invisible);
filldraw(shift((0,-2*pi))*streifen, pattern("strich2"), invisible);

draw("\(2\pi\)", (x+1,a+4*pi)--(x+1,a+2*pi), E, Bars);
draw("\(2\pi\)", (x+1,a+2*pi)--(x+1,a), E, Bars);
draw("\(2\pi\)", (x+1,a)--(x+1,a-2*pi), SE, Bars);
label("\(a\)", (-x,a), W);
