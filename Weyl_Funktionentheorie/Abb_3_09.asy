size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw(unitcircle);
dot("\(0\)", (0,0), SE);
draw((-2,0)--(0,0));
