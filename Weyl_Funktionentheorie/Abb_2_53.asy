size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair O=(0,0);
//---------------------------------------------------------
draw((-pi/2,-pi/2)--(-pi/2,pi/2));
draw((0,-pi/2)--(0,pi/2));
draw((pi/2,-pi/2)--(pi/2,pi/2));
draw((-pi/2,0)--(pi/2,0));
filldraw((-pi/2,-pi/2)--(-pi/2,pi/2)--(pi/2,pi/2)--(pi/2,-pi/2)--cycle,
          pattern("strich"), invisible);

label("\(+\frac{\pi}{2}\)", (-pi/2,0), W);
label("\(-\frac{\pi}{2}\)", (pi/2,0), E);
