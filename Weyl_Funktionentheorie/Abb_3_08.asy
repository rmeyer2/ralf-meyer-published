usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair O=(0,0);
path L=circle(O,1);
//---------------------------------------------------------
filldraw(circle(O, 2.5), pattern("strich"));
filldraw(circle((-0.3,-0.3), 0.25), white, invisible);
filldraw(circle((1.3,0), 0.25), white, invisible);
filldraw(circle(O, 0.2), white);

draw(L, 0.4mm+black);
label("\(\mathfrak{L}\)", point(L,0), E);
dot("\(0\)", O, SW-(0.5,0.5));
