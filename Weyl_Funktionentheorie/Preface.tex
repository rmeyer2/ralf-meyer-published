\selectlanguage{english}
\chapter*{Preface}
\addcontentsline{toc}{chapter}{Preface}
\markboth{Preface}{Preface}
\thispagestyle{empty}
\section*{Hermann Weyl's Lectures on Function Theory}

Hermann Weyl gave two courses on complex function theory at the
Georg-August-Universität Göttingen in the winter terms of
1910--11 and 1911--12, two years after he finished his thesis
on eigenfunctions of differential operators in 1908.  This book
is a transcription of the first of these two courses.  The
second one was the basis of Hermann Weyl's book \emph{Die Idee
  der Riemannschen Fläche}~\cite{weyl1}, which is often
regarded as one of the most important books on mathematics of
its era.  Nevertheless, it is not at all easy to read because
it is the second of two courses.  The first part already paves
the way for the integration of Riemann's geometric approach
with Weierstraß' analytic approach, which is the hallmark
of~\cite{weyl1}.  Several important concepts that are nowadays
treated differently, are explained carefully in the first part.
An example of this is Weierstraß' version of complex function
theory, which has been subsumed in the sheaf-theoretic language
of today.

Before going further a remark is called for here.  From the
period around 1825 onwards---roughly the time Crelle's Journal
was established---pure mathematics developed as a separate
discipline in Germany as it did nowhere else.  The use of the
words “reine” and “angewandte” on the title “Journal für die
reine und angewandte Mathematik” represents an early use of the
words “pure” and “applied” (in translation) in essentially the
senses we use today.  When Gauß was growing up he was more or
less entirely isolated as a mathematician.  The Prussian state,
which was recovering from the trauma of the Napoleonic period,
made mathematics, and for the first time pure mathematics, a
central academic concern.  Gauß became the model for the first
(Dirichlet, Jacobi) and second (Weierstraß, Kummer, Riemann,
Eisenstein) generations of German mathematicians and their
successors (Dedekind, Kronecker, Klein, Cantor, Hilbert).  The
development from 1825 on was very dynamic.  It exposed the
imprecision of much of the foundations of mathematics and
forced mathematicians to confront problems of a foundational
nature.  This process began in the 1870s with the development
of ideal theory in algebraic number theory (Dedekind,
Kronecker), in the theory of trigonometric series (Riemann,
Cantor), in analysis (Weierstraß), the theory of algebraic
functions (Riemann, Weierstraß, Klein).  In the end, it was
necessary to go back to the notion of a real number (Dedekind)
and of sets (Cantor) and to the foundations of mathematics
itself (Frege, Hilbert).  All of these developments took place
in a relatively short time; the beginnings were in the 1870s
and 1880s and there was great activity up to 1900--1910.  A
very large number of them were rooted in the German
mathematical community, although they widened to other
countries (especially Italy) in the following decades.

Very different points of view were expressed, and more by
chance than anything else there emerged two schools, the
“Berlin school” and the “Göttingen school”.  The first was
based on an arithmetical approach to mathematics---typical of
this were Weierstraß' \(\varepsilon\)--\(\delta\) approach to
analysis and Kronecker's constructive approach to algebraic
number theory.  The “Göttingen school” combined two sets of
ideas that were not immediately or obviously connected with one
another.  One was a geometric (“anschaulich”) approach to
mathematics propagated by Felix Klein.  The other was a
set-theoretic foundation, which was one of Dedekind's most
revolutionary innovations.  He was the first to define or
construct mathematical objects as sets.  The best known
examples of his work are the theory of ideals and his
construction of the real numbers using Dedekind cuts.  Dedekind
had been a student of Gauß in Göttingen and taught later in
Braunschweig.  Hilbert was profoundly influenced by Dedekind's
ideas and took them much further.  These ideas were very
suspect to some members of the Berlin mathematical community,
especially Kronecker, for they were not rigorous by his
standards.  See~\cite{rowe} for a discussion of the two
schools.

Several protagonists in these debates were very strong
personalities, and there were passionate arguments, which
collectively went under the name of “Grundlagenkrise.” Besides
the mathematical differences, there were more material grounds
for an antipathy between Berlin and Göttingen: Klein had the
ear of the Ministerialrat F.\ Althoff in Berlin, and in Berlin
this was taken badly, especially by G.\ Frobenius.  These
debates continued, in varying forms, for a long time.  One of
the most ferocious outbreaks was the locking of horns of
Hilbert and L.\,E.\,J.\ Brouwer over the editorship of
Mathematische Annalen (see~\cite{vdalen}).  Weyl himself was
very much involved in this debate, see \cites{weyl2,weyl3}; see
his remarks on the events of that time in the Nachtrag
to~\cite{weyl3}.  The heat went out of most of these arguments
after Gödel's work appeared in the early 1930s.

I.\ Lakatos (\cite{lakatos}) stressed the role of dialectic in
the development of mathematics, in bringing together differing
points of view.  This is often done by mathematicians of one
generation absorbing the achievements of their predecessors and
uniting them in their minds.  In this fashion, debates of the
past lose their force; we, for example, do not really decide
between Kronecker's and Dedekind's approach to algebraic number
theory; they are combined into one theory and we use what is
most appropriate to the problem in hand.  One can regard the
arguments as being essentially productive even if they were not
so experienced by the participants.

This is the case with Weyl's “Idee der Riemannschen
Fläche”~\cite{weyl1}.  At the time Weyl gave his course,
Göttingen was at its zenith as a centre of mathematics, not
only in Germany but in the world.  What Weyl united here were
Weierstraß' rigorous and ungeometrical approach to the theory
of “analytische Gebilde” (roughly the theory of the structure
sheaf) with Riemann's much more geometrical viewpoint,
especially as propagated by Klein.  In particular, as early as
1874, Klein had begun to think of Riemann surfaces as abstract
entities, see \cite{klein}*{page~x}.  (Klein ascribes his
epiphany to Prym but, as R.\ Remmert discovered, Prym claimed
that he had neither told Klein anything of this nature, nor had
he thought it himself, see~\cite{remmert}.)

We can see this idea already in Riemann's “Über die Hypothesen,
welche der Geometrie zugrunde liegen” (\cite{riemann}*{pages
  304--317}; Riemann speaks of “Massverhältnisse” for what we
would now call local coordinates); it was with Klein that
Riemann's germ took root and flourished.  Klein liked to stress
the visual (anschaulich) aspects of mathematics and had little
time for foundational questions.  His approach was an anathema
to Weierstraß and his followers, and this led to a Göttingen
school which in many respects was the obverse of the strict
Berlin school.  Klein's notion was not rigorous in a sense that
the Berlin school could recognise.  What was needed was a
formal definition of a Riemann surface, and this is the crucial
step taken by Weyl in~\cite{weyl1}.  The definition follows
Dedekind and Hilbert in defining a Riemann surface as a set
with further properties, namely an atlas and transition
functions.  In order to do this he used Weierstraß' ideas.  For
a very compact argument, in the case of algebraic functions,
see~\cite{lang}.

One problem before this was that one could not always talk
about a Riemann surface.  At the time of Weyl's lectures, the
general uniformisation theorem of P.~Koebe and H.~Poincaré was
still quite new (1909), and function theory was correspondingly
very topical.  See~\cite{abikoff} for a discussion of this.
Even the uniformisation theorem was not expressed as a theorem
about Riemann surfaces, but rather as a statement about (a
connected component of) solutions of systems of analytic
equations which were one-dimensional (an “analytisches
Gebilde”).  This makes both the formulation and the proof of
the uniformisation theorem considerably more difficult.  After
Weyl's formal definition, the whole theory became much more
transparent and flexible.  It is worth noting that Koebe ends
his paper~\cite{koebe} with the remark, “Damit ist ein Problem,
welches, wie man wohl sagen darf, im Sinne der Weierstraß'schen
Gedankenbildung liegt, durch die Anwendung von Prinzipien
gelöst, welche dem Riemannschen Ideenkreis eigentümlich sind.”
See here the discussion in~\cite{ullrich2}.

The present-day formulation of the uniformisation theorem
considers a Riemann surface~\(R\) and a signature~\(\sigma\) on
it.  The signature is a function on~\(R\) taking values in
\(\{1,2,\dotsc,\infty\}\) and such that \(R \setminus
\sigma^{-1}(1)\) is discrete.  One calls a point where the
weight is~\(\infty\) \emph{parabolic} and where it is \(\ge 1\)
but finite \emph{elliptic}.  Then one has to construct a
“covering” surface~\(\tilde{R}\) and a map \(p\colon \tilde{R}
\to R \setminus \sigma^{-1}(\{\infty\})\) which is a covering
map on \(\sigma^{-1}(1)\) and so that if~\(P\) is such that
\(\sigma(P)>1\) and~\(P'\) such that \(p(P')=P\) then for local
variables~\(z'\) at~\(P'\) and~\(z\) at~\(P\), one has that
\(z'^{\sigma(P)}/z \circ p\) is holomorphic and non-vanishing
in a neighbourhood of~\(P'\).  The general uniformisation
theorem then states that~\(\tilde{R}\) is holomorphically
equivalent to the unit disc, the plane or the Riemann sphere.
Moreover to a parabolic element~\(P\) of~\(\sigma\) there
exists a parabolic subgroup~\(H\) of the group~\(G\) of deck
transformations for which there exists a horocyclic
neighbourhood~\(U\) of the fixed point so that \(H \backslash
U\) can be identified biholomorphically with a punctured
neighbourhood of~\(P\).  (A horocyclic neighbourhood
of~\(\infty\) on the boundary of the upper
half-plane~\(\mathbb{H}\) is a set of the form \(\{z \mid
\Imag(z)>c\}\) for \(c>0\).)

The apparent core of the proof is the demonstration of
Riemann's mapping theorem, namely that a simply connected
Riemann surface is biholomorphically equivalent to the Riemann
sphere, the plane or the unit disc.  One fixes a point and
attempts to construct a harmonic function~\(u\) with a
logarithmic singularity at that point and which “tends to zero
at infinity.” This is generally done by an exhaustion argument
which is based on the Poincaré--Volterra “Lemma” (proved in
1888); this essentially shows that~\(\tilde{R}\) is paracompact
if~\(R\) is, as one always assumes, see~\cite{ullrich1}.  The
construction of~\(\tilde{R}\) is subtle and is rarely given in
full generality.  For an admirable exception, see
\cite{lamotke}[Kap.4].  If we assume that~\(R\) is compact, so
that~\(\sigma\) has a finite support, the whole argument is
much simpler.

If~\(R\) is compact, then a combinatorial argument allows to
determine the group of deck transformations.  This most
remarkable fact goes back to Möbius and Jordan.  It is one of
the reasons why the theory of Riemann surfaces and Fuchsian
groups (that is, the groups of deck transformations) is much
more complete than in any other situation.

These results are the goals of~\cite{weyl1}.  In this first
lecture course, we see that he had not yet completed his major
step but is thinking towards it.  He gave a course with its own
distinctive flavour, as Weyl always did, but not revolutionary.
Most results covered in this course are contained in the
introductory text by Burckhardt~\cite{Burckhardt} and the
treatises by Osgood~\cite{Osgood} and Hensel and
Landsberg~\cite{HL}.  But the tasteful selection of topics
already hints at Weyl's later achievements.

As befits a student of the Göttingen school, he emphasised
visual aspects and connections to mathematical physics wherever
possible.  He discussed kinematical aspects of fractional
linear transformations in remarkable detail---following Klein's
ideas on mathematics education (see~\cite{Krueger}).  This led
him to an excursion on one-parameter subgroups of Lie groups.
The care taken in discussing topology was very much
cutting-edge at the time.  In the final chapter, he introduced
Riemann surfaces---still without the rigour
of~\cite{weyl1}---and Weierstraß' theory of analytic
continuation, and argued that both approaches are equivalent.

\smallskip

The interest of this lecture course is, first of all, that it
clarifies some of the concepts that for a modern reader are
difficult in “Die Idee der Riemannschen Fläche” and, secondly,
that it shows the path taken by Weyl to his conception.  For
these reasons, this course is of considerable interest both to
the historian of mathematics and to the historically interested
mathematician.

\bigskip
\begin{flushright}
  Samuel J.\ Patterson
\end{flushright}


\selectlanguage{ngerman}
\chapter*{Zur Entstehung dieses Buches}
\label{cha:Entstehung}
\addcontentsline{toc}{chapter}{Zur Entstehung dieses Buches}
\markboth{Zur Entstehung dieses Buches}{Zur Entstehung dieses
  Buches}

Dieses Buch beruht auf der Vorlesung „\emph{Einleitung in die
  Funktionentheorie}”, die Hermann Weyl im Wintersemester
1910--11 an der Georg-August-Universität Göttingen gehalten
hat.  Wie damals üblich, wurde einer der Hörer -- hier Fritz
Frankfurther -- damit beauftragt, eine autorisierte
Vorlesungsmitschrift zu erstellen; diese wurde vom Dozenten
gegebenenfalls kommentiert und korrigiert und verblieb dann in
der Bibliothek des Mathematischen Instituts, die entsprechend
heute über eine große Zahl von Vorlesungsmitschriften aus
dieser Zeit verfügt.

Dass diese Handschrift schließlich als Buch herausgebracht
werden kann, ist eine Nebenwirkung der Studienreform.  Bei
dieser Gelegenheit wurde in Göttingen nämlich ein \LaTeX-Kurs
im Bereich Schlüsselqualifikationen eingeführt, in dem die
Studierenden als Projektarbeit auch einen etwa zehnseitigen
mathematischen Text in {\LaTeX} erstellen.  Als ich diesen Kurs
im Oktober 2007 erstmals hielt, habe ich auf Anregung von Yuri
Tschinkel und Samuel Patterson den Studierenden angeboten,
jeweils zehn Seiten der Vorlesungsmitschrift von Weyls
Vorlesung zu transkribieren.  Mehrere Studierende griffen
diesen Vorschlag bereitwillig auf.  Insofern beruht dieses Buch
hauptsächlich auf der Arbeit von Kathrin Becker, Malte Böhle,
Sandra Bösiger, Steven Gassel, Jan-Niklas Grieb, Alexander
Hartmann, Robert Hesse, Carolin Homann, Torsten Klampfl, Simon
Naarmann, Patrick Neumann, Christian Otto, Mila Runnwerth,
Sophia Scholtka und Hannes Vennekate, denen ich hier nochmals
meinen besonderen Dank ausspreche.  Dieser gilt auch Prof.\
Dr.\ Samuel Patterson, der das Vorwort beisteuerte, und Robert
Schieweck, der die knapp 200 Skizzen und Illustrationen im
Original mit dem Programm Asymptote nachzeichnete.

Der Inhalt dieses Buches ist im Wesentlichen identisch mit dem
Original.  Ich hielt es aber für angemessen, einige
Kleinigkeiten zu verbessern, ohne dies jeweils im Detail
anzugeben:
\begin{itemize}
\item Das Original kennt keine nummerierten Sätze und kaum
  nummerierte Gleichungen, und Textverweise beziehen sich immer
  auf die Seitennummer.  Ich habe mir erlaubt, einige wichtige
  Sätze, entsprechend den heutigen Gepflogenheiten, als
  nummerierte Sätze aus dem Text herauszuheben, und Verweise
  darauf entsprechend angepasst.

\item Die Abbildungen wurden ebenfalls teilweise nummeriert und
  mit erklärenden Bildunterschriften versehen, die von Robert
  Schieweck oder von mir stammen.

\item Alle Fußnoten mit Ausnahmen der Fußnoten in
  Abschnitt~\ref{sec:Uebung} stammen von mir.  Im Original gibt
  es keine Fußnoten, sondern nur spätere Ergänzungen, die sich
  fast immer direkt in den Text einfügen.

\item Ich habe eine Reihe von Tippfehlern im Original
  korrigiert und in einigen Punkten die Notation dem modernem
  Gebrauch angepasst.  Letzteres betrifft unter anderem die
  Schreibung von Grenzwerten, die Bezeichnung von Gebieten, und
  die Verwendung von Klammern.  (Im Original wird ein Gebiet
  meist mit~\(g\) bezeichnet, daraus wurde~\(G\); geschweifte
  oder eckige Klammern im Original wurden durch größere runde
  Klammern ersetzt, weil diese Klammern heute spezielle
  Bedeutungen haben.)

\item Die Rechtschreibung wurde an die heutige Norm angepasst,
  an manchen Stellen habe ich die Satzstellung verändert.

\end{itemize}

Außerdem wurden zusammen mit der Vorlesung noch ein
korrigiertes Übungsblatt und zwei aus Papier ausgeschnittene
Versuche eines Modells der aufgeschnittenen Riemannschen Fläche
aus Abbildung~\ref{abb:5-12} überliefert.  Es ist unklar,
inwieweit es noch mehr Übungen gab außer dieser.  Das
Übungsblatt wurde als Abschnitt~\ref{sec:Uebung} in das Buch
aufgenommen, aber mit den Papiermodellen ließ sich wenig
anfangen.

Mein eigener Beitrag zu diesem Buch ist vor allem die
Endredaktion und die Zusammenführung der verschiedenen
Beiträge.

\bigskip
\begin{flushright}
  Prof.~Dr.~Ralf Meyer, Mai~2008
\end{flushright}


\mainmatter
\addcontentsline{toc}{chapter}{Einleitung: Vom Begriff der Funktion}
\chapter*{Einleitung: Vom Begriff der Funktion}
\label{cha:Einleitung}%
\label{Orig:005}%

Es handelt sich bei dem elementaren \emph{Funktionsbegriff} um
die Bestimmung einer \emph{Abhängigkeit von Größen in einem
  Intervall}, sagen wir von~\(0\) bis~\(1\).  Die Funktionen
\(f(x)=x\) oder noch einfacher \(f(x)=1\) werden bekanntlich
dabei durch gerade Linien dargestellt.  Ein Beispiel einer
komplizierteren Funktion ist~\(\e^x\).  Auch kann in
verschiedenen Intervallen die Funktion durch verschiedene
analytische Ausdrücke gegeben sein.

\emph{Allgemein} ist der Funktionsbegriff dadurch
charakterisiert, dass durch eine Funktion \emph{Größen einander
  zugeordnet} werden.

Durch Analyse des Funktionsbegriffs gelangt man dann zu dem
Begriff der Stetigkeit, Differenzierbarkeit, mehrmaligen
Differenzierbarkeit, und so weiter.  Doch eine Unannehmlichkeit
haftet dem Funktionsbegriff, wie wir ihn hier dargestellt haben
noch an: Ist etwa die Funktion im Intervall von~\(0\) bis~\(1\)
wohl definiert, so herrscht in Bezug auf die Bestimmung der
Funktion über~\(1\) hinaus noch eine große Willkür, und doch
können wir dabei die Voraussetzungen der Differenzierbarkeit,
und so weiter, festhalten.

Das \emph{Wesen nun der Funktionentheorie}, oder, wie man auch
sagt, der \emph{Theorie der analytischen Funktionen}, besteht
darin, dass sie in den Funktionen das \emph{Ideal des
  Funktionsbegriffs} verwirklicht, indem sie durch Definition
in einem beliebigen Bereich den weiteren Verlauf einer Funktion
festsetzt.  Es ist dies das so genannte \emph{Prinzip der
  analytischen Fortsetzung}.  Die \emph{Funktionen komplexen
  Arguments}, die dieses anzustrebende Ideal verwirklichen,
heißen \emph{analytische Funktionen}.

Nachdem dieses Prinzip gefunden war, war es klar, dass die
Funktionentheorie eine große Rolle zu spielen begann.  Dies
beruht eben gerade darauf, dass durch Definition einer Funktion
in einem noch so kleinen Intervall in einem ganzen gewünschten
Intervall der Verlauf der Funktion bestimmt wird.

Die komplexen Variablen haben eine \emph{durchaus reale
  Bedeutung}.  Ihre Funktionen lassen eine geometrische Deutung
durch Abbildung, außerdem eine physikalische als Bild der
elektrischen Strömung zu.  Auf Grund beider Bedeutungen hat
sich ihre Entwicklung vollzogen.  Man kann die
Funktionentheorie geradezu auf"|fassen als einen Teil der
mathematischen Physik.  Der Aufbau des Gebietes knüpft sich an
die Namen \emph{Cauchy}, \emph{Riemann}, \emph{Weierstraß}.

\emph{Riemann} ging von der \emph{mathematischen Physik} aus;
er entnahm aus ihr die Fragestellung, die für seine
funktionentheoretischen Entwicklungen charakteristisch ist.
\emph{Weierstraß} nahm, von \emph{Potenzreihen} ausgehend, eine
ganz andere Richtung.  Im Wesen der Sache kommen sie auf
dasselbe hinaus, und man muss die Cauchy-Riemannsche und die
Weierstraßsche Methode berücksichtigen.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Weyl-Funktionentheorie"
%%% End:
