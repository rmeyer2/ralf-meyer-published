size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
add("strich2", hatch(3mm, NW));
path dreieck=(-3,0.8)--(-0.6,0)--(-3,-0.8);
//---------------------------------------------------------
filldraw((-3,-1.5)--(-3,1.5)--(2,1.5)--(2,-1.5)--cycle,
         pattern("strich"), invisible);
draw((-3,-1)--(0,0)--(-3,1), 0.4mm+black);
draw(dreieck);
filldraw(dreieck--cycle, pattern("strich2"), invisible);
