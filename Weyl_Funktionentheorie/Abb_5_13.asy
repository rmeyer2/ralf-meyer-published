size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path p=(2.5,0)..(1,1)..(-0.5,0)..(1,-1)..cycle;
//---------------------------------------------------------
dot((1,0));
dot((0,1));
dot((0,-1));

draw((0,1.5)..(-0.8,0)..(0,-1.5)..(0.8,0)..cycle);
draw(subpath(p, 0, intersections(p,(0,1)--(0,-1))[0][0]));
draw(subpath(p, intersections(p,(0,1)--(0,-1))[0][0], 4), dashed);
