size(3cm,0);
import math;
import graph;
import patterns;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
real phi=pi/3;
//---------------------------------------------------------
draw((0,0)--polar(3,0));
draw((0,0)--polar(3,phi));
filldraw((0,0)--polar(3.5,0)--polar(3.5,phi)--cycle,
          pattern("strich"), invisible);
filldraw(circle((1,0.5), 0.3), white, invisible);
label("\(\alpha_1\varphi\)", (1,0.5));
markangle(radius=40, polar(1,phi), (0,0), polar(1,0));
