size(10cm,0);
import graph;
import math;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path x=(-1,0)--(4,0), y=(0,-1)--(0,4), s=(-0.2,0)--(0.2,0);
path quadrat=unitsquare;
transform t=scale(1.5)*shift((4,2))*rotate(-45);
add("gestrichelt1", hatch(2mm,NE));
add("gestrichelt2", hatch(2mm,t*NE));
//---------------------------------------------------------
dot("$0$", (0,0), SW);
draw(x, EndArrow); label("$x$", (4,0), E);
draw(shift((1,0))*rotate(90)*s);
draw(shift((2,0))*rotate(90)*s);
label("$1$", (1,-0.5));
label("$2$", (2,-0.5));
draw(y, EndArrow); label("$y$", (0,4), N);
draw(shift((0,1))*s);
draw(shift((0,2))*s);
label("$1$", (-0.4,1));
label("$2$", (-0.4,2));
draw(quadrat); filldraw(quadrat,pattern("gestrichelt1"));

dot("$0$", t*(0,0), 2*W);
draw(t*x, EndArrow); label("$x''$", t*(4,0), (1,0));
draw(t*shift((1,0))*rotate(90)*scale(2/3)*s);
draw(t*shift((2,0))*rotate(90)*scale(2/3)*s);
label("$1$", t*(1,-0.5), NE);
label("$2$", t*(2,-0.5), NE);
draw(t*y, EndArrow); label("$y''$", t*(0,4), (0,1));
draw(t*shift((0,1))*scale(2/3)*s);
draw(t*shift((0,2))*scale(2/3)*s);
label("$1$", t*(-0.5,1), SE/2);
label("$2$", t*(-0.4,2));
draw(t*quadrat); filldraw(t*quadrat,pattern("gestrichelt2"));

draw((1.5,1.5)..(2.5,4)..(5,6)..(7,6), dotted, EndArrow);
