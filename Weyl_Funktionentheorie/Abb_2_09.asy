size(10cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform t=shift((4,0));
pair O=(0,0), M=(0,1), No=(0,2);
path C=circle(M,1);
path x=(-2,0)--(1.5,0);
path v=(-1.5,0)--No;
//---------------------------------------------------------
draw(x);
draw(C);
draw(v);
dot("$0$", O, S);
dot("$N$", No, N);

draw(t*x);
draw(t*C);
draw(t*v);
dot("$0_1$", t*O, S);
dot("$N_1$", t*No, N);
