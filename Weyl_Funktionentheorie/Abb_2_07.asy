size(10cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform t=shift((12,0));
real x=0, y=0;
real vx1(real u) {return sqrt(-4*(x^2)*(u-x^2));}
real vx2(real u) {return -sqrt(-4*(x^2)*(u-x^2));}
real vy1(real u) {return sqrt(4*(y^2)*(u+y^2));}
real vy2(real u) {return -sqrt(4*(y^2)*(u+y^2));}
//---------------------------------------------------------
for(int i=-3; i<=3; ++i)
{
  draw(shift((i,0))*((0,-3.5)--(0,3.5)), dashed);
  draw(shift((0,i))*((-3.5,0)--(3.5,0)));
}

draw("\(w=z^2\)", (4.5,1)..(5.5,1.2)..(6.7,1), N, dotted, EndArrow);

dot(t*(0,0));
draw(t*((-5,0)--(5,0)));
for(x=0.5; x<=2; x+=0.5)
{
  draw(t*graph(vx1, -5, x^2, operator ..), dashed);
  draw(t*graph(vx2, -5, x^2, operator ..), dashed);
}
for(y=0.5; y<=2; y+=0.5)
{
  draw(t*graph(vy1, -y^2, 5, operator ..));
  draw(t*graph(vy2, -y^2, 5, operator ..));
}
clip((-5,-5)--(-5,5)--t*(5,5)--t*(5,-5)--cycle);
