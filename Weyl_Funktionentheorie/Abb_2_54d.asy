size(58mm,0); //urspr. size(0,7cm);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(1mm, NW));
transform r=rotate(90);
pair O=(0,0);

path kreis1(real t, real phi1=0, real phi2=360)
{
  return arc((0,t),sqrt(1+t^2), phi1, phi2);
}
path kreis2(real t, real phi1=0, real phi2=360)
{
  pair s = intersectionpoints(kreis1(0.5),O--polar(10,t))[0];
  pair x = intersectionpoints((10,0)--(-10,0),s-(-10,10*s.x/(s.y-0.5))--s+(-10,10*s.x/(s.y-0.5)))[0];
  return arc(x,abs(x-s),phi1,phi2);
}
//---------------------------------------------------------
draw((0,-1)--(0,1), 0.4mm+black);
label("\(-\textup{i}\)", (0,-1), W-(1,0));
label("\(\textup{i}\)", (0,1), W-(1,0));

draw(r*kreis1(-1));
draw(r*kreis1(1));
draw(r*kreis1(-1.5));
draw(r*kreis1(1.5));
draw((-3,0)--(3,0));

draw(r*kreis2(pi/4), dashed);
draw(r*kreis2(pi/3, 270, 90), dashed);
draw(r*kreis2(5*pi/4), dashed);
draw(r*kreis2(4*pi/3, -90, 90), dashed);

//filldraw((-0.1,-3)--(-0.1,-1)--(0.1,-1)--(0.1,-3)--cycle,
//          pattern("strich"), invisible);
//filldraw((-0.1,3)--(-0.1,1)--(0.1,1)--(0.1,3)--cycle,
//          pattern("strich"), invisible);

filldraw((-0.1,-3)--(0.1,-3)--(0,-1)--cycle,
          pattern("strich"), invisible);
filldraw((-0.1,3)--(0.1,3)--(0,1)--cycle,
          pattern("strich"), invisible);
