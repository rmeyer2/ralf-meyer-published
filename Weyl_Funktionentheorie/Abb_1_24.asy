size(7cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform r=rotate(-150);
pair a1=polar(exp(1),1);
pair a=polar(exp(1),1);
pair f(real t) {return exp(t)*log(abs(a))*(cos(angle(a)*t),sin(angle(a)*t));}
guide g1,g2;
real x1, x2;
path g=(0,22)--(50,5);
//---------------------------------------------------------
for(real s=1; s<=3; s+=0.5)
{
  a = s*a1;
  g1 = nullpath;
  g2 = nullpath;
  for(real t=0; t<=4; t+=0.1)
  {
    g1 = g1..f(t);
    g2 = g2..(f(t).x,-f(t).y);
  }
  draw(r*g1);
  draw(r*g2);
  x1=intersections(r*g1,g)[0][0];
  x2=intersections(r*g1,shift((0,-5))*g)[0][0];
  draw(r*subpath(g1, x1, x2), BeginArrow);
}
dot("\(0\)", (0,0), NE);
label("\(II\)", (45,28));
label("\(I\)", (27,-5));
clip((-10,-10)--(50,-10)--(50,30)--(-10,30)--cycle);
