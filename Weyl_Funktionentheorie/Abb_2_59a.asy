size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
//---------------------------------------------------------
filldraw((-3,-1)--(-3,1)--(2,1)--(2,-1)--cycle,
         pattern("strich"), invisible);
filldraw((-3,0.3)--(0,0.3)..(0.3,0)..(0,-0.3)--(-3,-0.3)--cycle,
         white, invisible);
draw((-3,0.3)--(0,0.3)..(0.3,0)..(0,-0.3)--(-3,-0.3), 0.05mm+black);
draw((-3,0)--(0,0), 0.4mm+black);
dot((0,0));
