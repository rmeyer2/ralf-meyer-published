size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real y=3.1; real x1=1; real x2=4.1;
//---------------------------------------------------------
draw((0,-y)--(0,y), 0.4mm+black);
draw((1,-y)--(1,y));
draw((2,-y)--(2,y), 0.4mm+black);
draw((3,-y)--(3,y));
draw((4,-y)--(4,y));
label("\(0\)", (0,0), W);
draw((-0.5,0.5)--(-0.5,2), EndArrow);

draw((x1,-3)--(x2,-3), dashed); label("\(b_1\)", (x2,-3), E);
draw((x1,-1.5)--(x2,-1.5), dashed); label("\(a_1\)", (x2,-1.5), E);
draw((-0.1,0)--(4-pi,0), 0.4mm+black);
draw((4-pi,0)--(4,0), 0.5mm+dotted); label("\(\pi\)", (x2,0), E);
draw((x1,1.5)--(x2,1.5), dashed); label("\(a\)", (x2,1.5), E);
draw((x1,3)--(x2,3), dashed); label("\(b\)", (x2,3), E);
draw((1.8,0.5)--(x2,0.5), EndArrow);
