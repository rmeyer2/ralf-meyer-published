size(11cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
//---------------------------------------------------------
dot((-1,-2.2)); dot((1,-2.2));
draw((-2,-2.2)--(-1,-2.2)); draw((1,-2.2)--(2,-2.2));
filldraw((-2,-1.7)--(2,-1.7)--(2,-2.7)--(-2,-2.7)--cycle, pattern("strich"), invisible);

//dot((-1,-2.5)); dot((1,-2.5));
//draw((-2,-2.5)--(-1,-2.5)); draw((1,-2.5)--(2,-2.5));
//filldraw((-2,-2)--(2,-2)--(2,-3)--(-2,-3)--cycle, pattern("strich"), invisible);
