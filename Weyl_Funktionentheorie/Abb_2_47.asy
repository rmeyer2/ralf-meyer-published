size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
dot("\(0\)", (0,0), E);
draw((0,0)..(-2,-1)..(-4,0)..(-6,-2)..(-7,-2)..(-8,-3), ArcArrow);
