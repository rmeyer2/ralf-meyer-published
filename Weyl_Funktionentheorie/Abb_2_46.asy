size(10cm,0);
import graph3;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(2,-6,3);

real x(real t) {return cos(t);}
real y(real t) {return sin(t);}
real z(real t) {return t/4;}
//---------------------------------------------------------
xaxis(Label("\(x\)", 1), (-2,0,0), (2,0,0), Arrow);
yaxis(Label("\(y\)", 1), (0,-2,0), (0,2,0), Arrow);
zaxis(Label("\(z\)", 1), (0,0,-1), (0,0,1.5), Arrow);
dot("\(0\)", O, SE);
draw(shift((-1.5,-1.5,0))*plane((3,0,0),(0,3,0)));

draw(graph(x,y,z,0,3*pi+1,Spline));
draw("\(2\pi\)", (x(0),y(0),z(0))--(x(2*pi),y(2*pi),z(2*pi)), NE, Arrows);
