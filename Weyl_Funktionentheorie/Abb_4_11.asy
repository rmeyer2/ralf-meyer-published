size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0);
//---------------------------------------------------------
dot("\(z_0\)", z0, SW);
draw(circle(z0, 1));
draw(circle(z0, 4));
