size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real f(real x) {return asin(exp(-x));}
real g(real x) {return -asin(exp(-x));}
guide G1=graph(f, 0, 8, operator ..);
guide G2=shift((0,pi))*graph(g, 0, 8, operator ..);
pair L=W-(1,0);
transform t=shift((0,-pi));
transform tm=shift((0,pi));
transform cm=shift((-2,0));
transform cp=shift((2,0));
transform s=shift((0,-pi/2));
//---------------------------------------------------------
xaxis("\(x\)", axis=YEquals(0), EndArrow);
xaxis(axis=YEquals(pi));
//xaxis(xmin=0, axis=YEquals(pi/2), dashed);
//xaxis(xmin=0, axis=YEquals(-pi/2), dashed);
yaxis("\(y\)", ymin=-pi, ymax=3.8, LeftTicks("%", new real[]{-pi,-pi/2,0,pi/2,pi}), EndArrow);

label("\(\frac{\pi}{2}\)", (0,pi/2), L);
label("\(-\frac{\pi}{2}\)", (0,-pi/2), L);

draw(G1); draw(G2);
draw(cm*G1); draw(cm*G2);
draw(cp*G1); draw(cp*G2);

draw(t*G1); draw(t*G2);
draw(t*cm*G1); draw(t*cm*G2);
draw(t*cp*G1); draw(t*cp*G2);

draw(s*G1, dashed); draw(s*G2, dashed);
draw(s*cp*G1, dashed); draw(s*cp*G2, dashed);

draw(s*t*G2, dashed);
draw(s*t*cp*G2, dashed);

draw(tm*s*G1, dashed);
draw(tm*s*cp*G1, dashed);
