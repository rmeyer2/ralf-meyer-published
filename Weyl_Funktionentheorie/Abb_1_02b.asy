size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//-------------------------------------------------------
pair o = (0,0), a = (0.5,2), b = (4,1);

// Zeichne zuerst Schattierungen, um Linien nicht zu überzeichnen
fill( o -- a -- (a.x,0) -- cycle, lightgray);
fill( b -- (a+b) -- ((a+b).x,b.y) -- cycle, lightgray);
//--Koordinatensystem--------------------------------------
xaxis("$x$",EndArrow);
yaxis("$y$",EndArrow);
dot(o);           //"Ursprung"
label("$0$",o,SW);  //"Ursprung" beschriften
//---------------------------------------------------------
// alpha, beta, alpha+beta
draw( o -- a, EndArrow);  //alpha
label( "$\alpha$", a, N);
draw( o -- b, EndArrow);  //beta
label( "$\beta$", b, S);
draw( o -- (a+b), EndArrow); //alpha+beta
label( "$\alpha + \beta$", (a+b),NE);

// gestrichelte Dreiecke
draw( a -- (a.x,0), dashed);
draw( b -- (a+b), dashed);
draw( (a+b) -- ((a+b).x,b.y), dashed);
draw( b --  ((a+b).x,b.y), dashed);
draw( a -- (a+b), dashed);
