usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0), z=(-1,-2);
path K=circle(z0,3);
path Ke=circle(z0,1);
//---------------------------------------------------------
dot("\(z_0\)", z0, NW);
dot("\(z\)", z, W);

draw(K);
draw(shift((-0.2,0.2))*subpath(K,1.2,1.8), EndArrow);
label("\(\mathfrak{K}\)", (3,0), W);

draw(Ke);
draw(shift((0.2,0.2))*subpath(Ke,0.2,0.8), BeginArrow);
draw("\(\varepsilon\)", z0--polar(1,pi/4));
label("\(\mathfrak{K}_\varepsilon\)", (1,0), E);

draw(z0--(-3,0), dotted);
draw((-1.5,0.2)--(-2.5,0.2), BeginArrow);
draw((-1.5,-0.2)--(-2.5,-0.2), EndArrow);
