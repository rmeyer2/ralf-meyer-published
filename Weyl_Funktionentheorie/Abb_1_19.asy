size(6cm,0);
import three;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,1);
face[] faces;

triple s=(0.25,0,0.5);

triple u1=(1,0,0), v1=(0,1,0);
path3 plane1=plane(u1,v1), plane2=shift(s)*plane1;

triple u3=(0.2,1,0), v3=s;
path3 plane3=shift(u1/4)*plane(u3,v3);

triple u4=(-0.4,1,0), v4=s;
path3 plane4=shift(u1/4+(u3.x-u4.x,0,0))*plane(u4,v4);
//---------------------------------------------------------
filldraw(faces.push(plane1),plane1,palegrey);
filldraw(faces.push(plane2),plane2,palegrey);
draw(faces.push(plane2),(u1/4 + u3) -- (u1/4 + u3 + v3), dashed);
filldraw(faces.push(plane3),plane3,mediumgrey);
filldraw(faces.push(plane4),plane4,mediumgrey);

add(faces);

draw(plane1, dashed);
