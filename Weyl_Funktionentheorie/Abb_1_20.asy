size(6cm,0);
import three;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-5,1);

triple P=(2.5,0,0), P1=intersectionpoints(P--(0,0,2),circle((0,0,1),1,Y))[1];
real m=-(P1.x)/(P1.z-1);
triple uz=(0,4,0), vz=(1.5,0,0);
triple ut=(0,4,0), vt=(P1.z*1.2/m,0,P1.z*1.2);
path3 planeZ=shift((P1.x-P1.z/m,-2,0))*plane(uz,vz);
path3 planeT=shift((P1.x-P1.z/m,-2,0))*plane(ut,vt);

//Test
triple Q = P1 - ut/6 - vt/3; triple R = P1 + ut/8 - vt/2.5;
triple Q1= Q + (P.z-Q.z)/(P.z-P1.z) * (P-P1);
triple R1= R + (P.z-R.z)/(P.z-P1.z) *(P-P1);
path3 box=P1--Q--Q1--P--cycle;
//---------------------------------------------------------
draw(planeZ);
draw(planeT);
filldraw(box,white);
draw(planeT, dashed);
draw(subpath(planeZ,1,2), dashed);

draw(R--R1, dashed);
draw(R--P1, dashed);
draw(R1--P, dashed);

dot("$P$", P);
dot("$P'$", P1, N);
label("$T$", (1,0,0.15));
markangle(radius = 30, Q, P1, R);
markangle(radius = 30, Q1, P, R1);
