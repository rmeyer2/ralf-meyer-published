usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path K=arc((0,0), 10, 45, 405);
path L=(-2,-5)..(-5,0)..(-2,5)..(0,2)..(5,-1)..(3,-4)..cycle;
//---------------------------------------------------------
draw(K, BeginArrow);
label("\(\mathfrak{K}\)", point(K,4), W);

draw(L);
draw(subpath(L, 3, 3.5), EndArrow);
label("\(\mathfrak{L}\)", point(L,-0.5), N);

dot((-6,6));
draw(arc((-6,6),1,90,450), ArcArrow);

dot((4,-6));
draw(arc((4,-6),1,90,450), ArcArrow);

dot((7,0));
draw(arc((7,0),1,90,450), ArcArrow);

dot((3,5));
draw(arc((3,5),1,90,450), ArcArrow);
