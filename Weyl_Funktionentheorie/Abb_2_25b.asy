size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real a=1, b=-1, x=1, y=1;
pair u=(a,-b), O=(0,0);
//---------------------------------------------------------
draw(O--(x+a,0)--(x+a,y-b)--(0,y-b)--cycle);
draw((x,0)--(x,y)--(0,y));
draw("\(\vec{u}\)", (x,y)--u+(x,y), N, EndArrow);
draw("\(\vec{u}_x = a\)", (x,0)--(x+a,0), S, Bars);
draw("\(\vec{u}_y = -b\)", (0,y)--(0,y-b), W, Bars);
dot("\(x,y\)", (x,y), SE);
