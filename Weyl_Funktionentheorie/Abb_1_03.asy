defaultpen(font("OT1","lmr","m","n")+fontsize(10));
size(5cm,0);
//v1
draw( "$v_1$", (0,0) -- (1,-1), EndArrow);
//v2
draw( "$v_2$", (1,-1) -- (3,-0.5), EndArrow);
//v3
draw( "$v_3$", (3,-0.5) -- (2.5,0.5), EndArrow);
//v1+v2+v3
draw( (0,0) -- (2.5,0.5), EndArrow);
label("$v_1+v_2+v_3$", (1,0.5));
