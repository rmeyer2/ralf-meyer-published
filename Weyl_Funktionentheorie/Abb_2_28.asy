size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform r=rotate(5);
//---------------------------------------------------------
draw(r*((-3.5,0)--(3.5,0)));
label("\(g\)", r*(3.5,0), N);

dot("\(0\)", r*(0,0), SW+(-0.8,-0.4));
for(real i=-1.5; i<=1.5; i+=0.5)
{
  if(i!=0)
    draw(r*circle((i,0), abs(i)));
}
