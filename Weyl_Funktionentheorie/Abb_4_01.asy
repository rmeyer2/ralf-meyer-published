usepackage("amsfonts");
size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real r=1;
pair z=(-3,0);
path g=(-7,0)..(-7,-3)..(0,-3)..(5,0)..(0,2)..(-3,2.5)..cycle;
path L=(0,0)..(-3,2)..(-6,0)..(-3,-2.5)..(-2,-1.3)..cycle;
path K=circle(z,r);
//---------------------------------------------------------
dot("\(z\)", z, E);

draw(K);
label("\(\mathfrak{K}_r\)", z-(r,0), W);
draw(subpath(K,0,1), ArcArrow);

draw(L);
label("\(\mathfrak{L}\)", point(L,4.5), SE);
draw(subpath(L,0,1.5), EndArrow);

draw(g);
label("G", (4,0));
