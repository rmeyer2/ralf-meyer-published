usepackage("amsfonts");
size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
path L=(-7,-10)..(9,0)..(7,10)..(-9,0)..cycle;
path L1=(-3,-8)..(-1,-6)..(-3,-4)..(-5,-6)..cycle;
path L2=(1,2)..(5,5)..(2,8)..(-1,5)..cycle;
//---------------------------------------------------------
filldraw(L, pattern("strich"));
draw(subpath(L,1,1.5), EndArrow);
label("\(\mathfrak{L}\)", point(L,1), E);

filldraw(L1, white);
draw(subpath(L1,1,1.5), EndArrow);
label("\(\mathfrak{L}_1\)", point(L1,1), W);

filldraw(L2, white);
draw(subpath(L2,1,1.5), EndArrow);
label("\(\mathfrak{L}_2\)", point(L2,1), W);

filldraw(circle((-3,2), 1), white, invisible);
label("G", (-3,2));
