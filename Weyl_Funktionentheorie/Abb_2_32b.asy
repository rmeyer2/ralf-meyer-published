size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
xaxis();
yaxis();

for(real i=-2; i<=2; i+=0.5)
{
  draw(circle((0,i), abs(i)));
}

clip((-3,-2.1)--(-3,2.1)--(3,2.1)--(3,-2.1)--cycle);
