size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path kreis=(-1,1)--(1,1)..(0,0)..cycle;
pair P=(2,0), No=(0,1), P1=intersectionpoints(kreis,No--P)[1];
//---------------------------------------------------------
xaxis(xmax=2.3, EndArrow);
yaxis("\(z\)", ymax=1.3, EndArrow);
dot("\(0\)", (0,0), SW);

draw(kreis);
dot("\(N\)", No, NW);
draw(No--P);
dot("\(P\)", P, S);
dot("\(P'\)", P1, NE+(0.2,0));
draw(P1--(P1.x,0));
dot("\(Q\)", (P1.x,0), S);
draw(P1--(0,P1.y));
dot("\(S'\)", (0,P1.y), SE);
draw("\(1\)", (-0.1,0)--(-0.1,1), W, Arrows);
