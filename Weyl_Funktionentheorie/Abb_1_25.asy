size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair b=(2,1), o=(-1,2)/2;
real m=b.y/b.x;
//---------------------------------------------------------
xaxis("$x$",EndArrow);
yaxis("$y$",EndArrow);
dot((0,0));

for(int i=-2; i<=2; ++i)
{
    draw(shift(i/2*o)*((-3,-3*m)--(3,3*m)), EndArrow);
}

draw(shift(-b/2)*((1,-1/m)--(-1,1/m)));
draw(shift(b/2)*((1,-1/m)--(-1,1/m)));
draw("$b$", shift((-1,1/m)-(o+b)/2)*((0,0)--b), (0,0.5),EndArrow);
draw("$b$", shift((1,-1/m)-(-o+b)/2)*((0,0)--b), EndArrow);
