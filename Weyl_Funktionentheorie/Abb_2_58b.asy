size(3cm,0);
import math;
import graph;
import patterns;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
real phi=pi/8;
//---------------------------------------------------------
filldraw((0,0)--polar(3.5,0)--polar(3.5,phi)--cycle,
          pattern("strich"), invisible);
filldraw((0.92,0.05)--(0.92,0.38)--(1.5,0.38)--(1.5,0.05)--cycle,
         white, invisible);
draw((0,0)--polar(3,0));
draw((0,0)--polar(3,phi));
label("\(\alpha_1\varphi\)", (1.2,0.2));
markangle(radius=40, polar(1,phi), (0,0), polar(1,0));
