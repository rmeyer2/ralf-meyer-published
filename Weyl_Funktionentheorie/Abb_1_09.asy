size(7cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real X=2, x=3, Y=1, y=1.5;
pair O = (0,0), Q1=(X,Y), P=(x,y);
//--Koordinatensystem--------------------------------------
draw((-0.1,0)--(x+0.2,0), EndArrow);
label("$x$", (x+0.2,0), E);
draw((0,-0.1)--(0,y+0.2), EndArrow);
label("$y$", (0,y+0.2), N);
dot("$0$", O, SW);           //"Ursprung"
//---------------------------------------------------------
draw((0,Y)--Q1);
draw((X,0)--Q1);
label("$Q'$", Q1, SE);
draw((0,y)--P);
draw((x,0)--P);
label("$P$", P, E);

draw(O--P);
draw("$X$", (0,-0.35)--(X,-0.35), N, Bars);
draw("$x$", (0,-0.6)--(x,-0.6), N, Bars);
draw("$Y$", (-0.35,0)--(-0.35,Y), E, Bars);
draw("$y$", (-0.6,0)--(-0.6,y), E, Bars);
