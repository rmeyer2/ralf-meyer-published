size(12cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((-6,0)--(6,0));
draw((-2,0)--(2,0), Bars);
label("\(-2\)", (-2,0), S);
label("\(+2\)", (2,0), S);

label("\(I^+\)", (-5,0), N);
label("\(II^+\)", (-4,0), N);
label("\(III^+\)", (-3,0), N);
label("\(II^-\)", (-5,0), S);
label("\(I^-\)", (-4,0), S);
label("\(III^-\)", (-3,0), S);

label("\(I^+\)", (-1,0), N);
label("\(II^+\)", (0,0), N);
label("\(III^+\)", (1,0), N);
label("\(I^-\)", (-1,0), S);
label("\(II^-\)", (0,0), S);
label("\(III^-\)", (1,0), S);

label("\(I^+\)", (3,0), N);
label("\(II^+\)", (4,0), N);
label("\(III^+\)", (5,0), N);
label("\(III^-\)", (3,0), S);
label("\(II^-\)", (4,0), S);
label("\(I^-\)", (5,0), S);
