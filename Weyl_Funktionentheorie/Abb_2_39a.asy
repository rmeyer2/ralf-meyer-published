size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
for(real y=-1; y<=1; y+=1/2)
{
  if(y!=0)
    draw((-4,pi*y)--(3,pi*y));
  draw((-3,pi*y+0.5)--(1,pi*y+0.5), EndArrow);
}
draw((-4,0)--(3,0), 0.4mm+black);
draw((0,0)--(1,0), BeginBar);
label("\(0\)", (0,-0.15), S);
label("\(y=-\pi\)", (3,-pi), E);
label("\(y=-\frac{\pi}{2}\)", (3,-pi/2), E);
label("\(y=0\)", (3,0), E);
label("\(y=\frac{\pi}{2}\)", (3,pi/2), E);
label("\(y=\pi\)", (3,pi), E);
