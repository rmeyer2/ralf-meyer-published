size(0,4cm);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real u(real x) {return cos(log(x)*10);}
//---------------------------------------------------------
xaxis("\(x\)", axis=YEquals(0), xmin=-0.5, EndArrow);
xaxis(xmin=0, axis=YEquals(1));
xaxis(xmin=0, axis=YEquals(-1));
yaxis("\(u\)", ymin=-1, ymax=1.4, LeftTicks("%", new real[]{-1,1}), EndArrow);
label("\(-1\)", (0,-1), W-(1,0));
label("\(+1\)", (0,1), W-(1,0));

draw(graph(u, 1.5, 5, operator ..));
draw(graph(u, 0.8, 1.5, operator ..), dashed);
