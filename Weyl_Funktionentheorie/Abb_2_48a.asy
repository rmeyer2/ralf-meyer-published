size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
path R=(-4,-2)--(-4,2)--(2,2)--(2,-2)--cycle;
path r=(-4,-0.5)--(-4,0.5)--(0.5,0.5)--(0.5,-0.5)--cycle;
//---------------------------------------------------------
filldraw(R, pattern("strich"), invisible);
filldraw(r, white, invisible);
xaxis(xmin=-4, xmax=0);
filldraw(circle((0,-0.4), 0.3), white, invisible);
dot("\(0\)", (0,0), S);
filldraw(circle((1,-0.4), 0.3), white, invisible);
dot("\(1\)", (1,0), S);
