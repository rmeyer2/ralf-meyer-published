size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real y1=0.3, y2=1.5, x0=1;
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);
label("", (2,0));

draw("\(x_0\)", (0,0.7)--(x0,0.7), N);
draw((x0,y1)--(x0,y2));
dot("\(y_1\)", (x0,y1), E);
dot("\(y_2\)", (x0,y2), E);
