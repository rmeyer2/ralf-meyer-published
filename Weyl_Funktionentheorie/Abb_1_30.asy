size(6cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
xaxis();
yaxis();
dot("$\xi$", (0,0), (-2,-1.5));

for(int i=-2; i<=2; ++i)
{
    if(i != 0)
        draw(circle((i,0),i));
}

for(int i=-2; i<=2; ++i)
{
    if(i != 0)
        draw(circle((0,i),i), dashed);
}

draw(arc((0,-3),3,-10,190), dashed);
draw(arc((0,-4),4,0,180), dashed);

label("\(I\)", (3,1));
label("\(II\)", (2.5,-3.5));
