usepackage("amsfonts");
size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0);
path g=(0,2)..(1,2)..(2,1)..(2,-2)..cycle;
path K=circle(z0,1);
//---------------------------------------------------------
dot("\(z_0\)", z0, E);
draw(K);
label("\(\mathfrak{K}\)", z0-(0,1), S);
draw(g);
label("G", (1.5,-1.5));
