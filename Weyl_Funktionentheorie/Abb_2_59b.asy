size(4cm,0);
import math;
import graph;
import patterns;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(2mm, NE));
real phi=4*pi/6;
//---------------------------------------------------------
draw(polar(1,phi)--(0,0)--polar(1,-phi), 0.4mm+black);
draw((0,0)--(2,0));
filldraw((polar(1,phi)+(0.2,0))--(2,sin(phi))--(2,-sin(phi))
       --(polar(1,-phi)+(0.2,0))--(0.2,0)--cycle,
          pattern("strich"), invisible);
draw((polar(1,phi)+(0.2,0))--(0.2,0)--(polar(1,-phi)+(0.2,0)),
      0.05mm+black);
draw(polar(0.5,phi)..(1,0)..polar(0.5,-phi));
filldraw((0.23,0.15)--(0.23,0.45)--(0.8,0.45)--(0.8,0.15)--cycle,
        white, invisible);
label("\(2\pi\alpha_1\)", (0.5,0.3));
