size(7cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O = (0,0);
real rho=2, r=1.6, r1=2.5, phi = pi/4;
pair P = polar(r,phi), P1 = polar(r1,phi);
//--Koordinatensystem--------------------------------------
xaxis("$x$",EndArrow);
yaxis("$y$",EndArrow);
dot("$0$", O, SW);           //"Ursprung"
tick((1,0),S); label("$1$", (1,-0.05), S);
label("$$",(rho+0.5,rho+0.5));
//---------------------------------------------------------
dot("$P$", P, N);
dot("$P'$", P1, NE);
draw(circle(O,rho));
draw("$\rho$", O--polar(2,+3.7*phi));
draw(O--P1);
draw("$r$", (-0.15,0.15)--(P.x-0.15,P.y+0.15), NW, Bars);
draw("$r'$", (0.15,-0.15)--(P1.x+0.15,P1.y-0.15), SE, Bars);
