size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
guide g1=(-1,-2)..(-2,-1)..(-2,1)..(-1,2)..(0.5,0.8)..(0,1)..
         (-1,0)..(0,-1)..(0.5,-0.8)..cycle;
guide g2=(-0.5,0)..(-0.3,2)..(0,2.2)..(1.2,2.5)..(2.5,0)..
         (1.2,-2.5)..(0,-2.2)..(-0.3,-2)..cycle;
//---------------------------------------------------------
draw(g1);
draw(g2);

picture schnitt=new picture;
fill(schnitt, g1, pattern("strich"));
clip(schnitt, g2);
add(schnitt);
filldraw(circle((0,1.5),0.3),white, invisible);
label("\(\mbox{G}_1\)", (0,1.5));
filldraw(circle((0,-1.5),0.3),white, invisible);
label("\(\mbox{G}_2\)", (0,-1.5));
