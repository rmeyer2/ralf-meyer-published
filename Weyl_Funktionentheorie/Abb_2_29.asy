size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
for(real phi=0; phi<(2*pi); phi+=(pi/8))
{
  draw((0,0)--polar(1,phi), EndArrow);
} 
