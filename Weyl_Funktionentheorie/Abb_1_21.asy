size(5cm,0);
import three;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
triple P1=(sin(pi/4)*cos(-pi/4),sin(pi/4)*sin(-pi/4),cos(pi/4));
triple P2=(sin(pi/4)*cos(pi/4),sin(pi/4)*sin(pi/4),cos(pi/4));
triple P3=(sin(pi/10)*cos(pi/2),sin(pi/10)*sin(pi/2),cos(pi/10));

triple D=cross(P2-P1, P3-P1);
currentprojection = orthographic(D.x,D.y,D.z);
//---------------------------------------------------------
draw(P1--P2--P3--cycle);
draw(arc(O,P1,P2));
draw(arc(O,P2,P3));
draw(arc(O,P3,P1));
