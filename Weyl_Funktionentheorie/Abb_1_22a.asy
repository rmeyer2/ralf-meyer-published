size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path kugelflaeche=circle((0,1),1);
path zebene = (-2,0)--(1.5,0);
//---------------------------------------------------------
draw(kugelflaeche);
draw(zebene, black+0.3mm);
tick((0,0),S); tick((0,0),N); label("$0$", (0,-0.5), N);
tick((0,2),S); tick((0,2),N); label("$N$", (0,2-0.5), N);
label("$I$", (-1.5,-0.5), N); label("$z$", (-1,-0.5), N);
