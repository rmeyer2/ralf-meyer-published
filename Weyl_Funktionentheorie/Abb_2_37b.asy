size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
//---------------------------------------------------------
filldraw((-2,-2)..(-1.8,0)..(-1.8,2)..(-1.6,2)..(2,2.3)..(2.3,0)..
         (3,-2)..(0,-2.1)..cycle, pattern("strich"));
filldraw((0.5,-0.5)..(0.5,0)..(0.7,0.5)..(1,0.6)..(0.8,-0.4)..cycle, white);
filldraw(rotate(135)*((0.5,-1)..(1,-0.1)..(1.1,0)..(1.5,-0.5)..(1.3,-0.5)..(1.2,-0.8)..cycle), white);
filldraw(circle((0,0),0.3),white, invisible);
label("\(\mbox{H}\)", (0,0));
