size(5cm,0);
import graph;
import math;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), M=(0,1), No=(0,2), P=(2.5,0);
path C = circle(M,1);
pair P1=intersectionpoints(P--No,C)[0];
real m=-(P1.x)/(P1.y-1);
//---------------------------------------------------------
draw(O--(3,0), EndArrow); label("$x$", (3,0), S);
draw(O--(0,2.5), EndArrow); label("$y$", (0,2.5), W);
dot("$0$", O, SW);

draw(C);
draw(No--P);
draw(P1+(1/m,1)--P1-(1/m,1));
label("$T$", P1-(1/m,1)+(0.1,0.2));
dot("$M$", M, W);
dot("$N$", No, SW);
dot("$P$", P, S);
dot("$P'$", P1, NE);
