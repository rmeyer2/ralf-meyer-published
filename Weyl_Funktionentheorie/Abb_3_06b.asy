usepackage("amsfonts");
size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0), z1=(10,2);
guide L1=z0..(3,1.5)..z1;
guide L2=z0..(3,1)..(2,3)..(-1,0)..(2,-2)..(5,-1)..(7,3)..(9,4)..z1;
//---------------------------------------------------------
draw(L1, 0.4mm+black);
draw(L2);
dot("\(z_0\)", z0, N);
dot("\(z_1\)", z1, E);

label("\(\mathfrak{L}_1\)", point(L1,1.3), N);
label("\(\mathfrak{L}_2\)", point(L2,5.3), E);
