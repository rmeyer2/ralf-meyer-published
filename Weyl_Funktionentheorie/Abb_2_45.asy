size(10cm,0);
import solids;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,2);

real f(real x) {return log(x);}
real x(real t) {return t;}
real y(real t) {return 0;}
revolution trichter=revolution(c=O, f, a=0.2, b=2, n=32, axis=Z);
//---------------------------------------------------------
xaxis(Label("\(x\)", 1), (-2,0,0), (2,0,0), Arrow);
yaxis(Label("\(y\)", 1), (0,-2,0), (0,2,0), Arrow);
zaxis(Label("\(z\)", 1), (0,0,-2), (0,0,1.5), Arrow);
dot("\(0\)", O, SE);

trichter.draw(2);
draw(rotate(45,Z)*graph(x,y,f,0.2,2,Spline));
draw(rotate(135,Z)*graph(x,y,f,0.2,2,Spline));

