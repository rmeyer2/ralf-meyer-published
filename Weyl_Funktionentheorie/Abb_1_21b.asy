size(5cm,0);
import math;
import graph;
//---------------------------------------------------------
path ellipse=(-4,0)..(0,3)..(4,0)..(0,-3)..cycle;
pair P1 = intersectionpoint(ellipse, (-4,-3)--(0,0));
pair P2 = intersectionpoint(ellipse, (-3,4)--(0,0));
pair P3 = intersectionpoint(ellipse, (0,0)--(5,-2));
path dreieck = P1--P2--P3--P1--cycle;
//---------------------------------------------------------
draw(dreieck);
