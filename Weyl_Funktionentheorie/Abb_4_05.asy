usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real r=1;
pair z0=polar(r,pi/6);
path K=circle((0,0),r);
//---------------------------------------------------------
dot("\(0\)", (0,0), SE);

draw(K);
label("\(\mathfrak{K}\)", (0,-r), N);

draw(scale(1.5)*K);
label("\(\mathfrak{K}'\)", polar(1.5*r,-pi/4), E);

dot("\(z_0\)", z0, SW);
dot("\(z'\)", 1.5*z0, N+(0.5,0));
dot("\(z_1\)", 3*z0, E);
draw(z0--3*z0);
