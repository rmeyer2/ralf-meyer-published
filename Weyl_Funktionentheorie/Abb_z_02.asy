size(5cm,0);
import math;
import graph;
import geometry;
import markers;
//---------------------------------------------------------
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
real phi=pi/4;
pair O=(0,0);
path p=(1,2)..(2,2)..(3,1.2);
path t=point(p,1)-1.5*dir(p,1)--point(p,1)+1.5*dir(p,1);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);

draw(O--polar(4,phi));
markangle(radius=40, (1,0), O, polar(1,phi));
label("\(\varphi\)", (3,1)/4);

draw(p, BeginArrow);
label("\(y=f(x)\)", point(p,2), S);
draw(t);
markangle(radius=40, polar(4,phi), point(p,1), point(p,1)+dir(p,1));
label("\(\xi\)", point(p,1)+(3,1)/4);
