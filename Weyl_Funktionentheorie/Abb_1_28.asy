size(10cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair x1=(-1,0), x2=(1,0);
pair O=(0,0);
path kreis1(real t, real phi1=0, real phi2=360)
{
  return arc((0,t),sqrt((t)^2 + x1.x^2), phi1, phi2);
}
path kreis2(real t, real phi1=0, real phi2=360)
{
  pair s = intersectionpoints(kreis1(0.5),O--polar(10,t))[0];
  pair x = intersectionpoints((10,0)--(-10,0),s-(-10,10*s.x/(s.y-0.5))--s+(-10,10*s.x/(s.y-0.5)))[0];
  return arc(x,abs(x-s),phi1,phi2);
}
//---------------------------------------------------------
xaxis();
yaxis();

for(int i=-2; i<=2; ++i)
{
    if(i != 0)
        draw(kreis1(i/4));
}
draw(kreis1(-1,-20,200));

draw(kreis2(5*pi/6), dashed);
draw(kreis2(4.5*pi/6), dashed);
draw(kreis2(1.5*pi/6), dashed);
draw(kreis2(1*pi/6), dashed);
draw(kreis2(4*pi/6, -90, 90), dashed);

dot(x1); dot(x2);
label("$I$", (0.5,1), W);
label("$I$", (0.5,-1), W);
label("$I$", point(kreis1(-1,-20,200),0), S);
label("$II$", (-2,0.5));
label("$II$", (2,0.5));
label("$II$", point(kreis2(4*pi/6,-90,90),0), W);
