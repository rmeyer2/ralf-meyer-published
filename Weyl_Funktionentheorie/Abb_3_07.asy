size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0), z=(5,2), del=(0.7,0.2);
//---------------------------------------------------------
dot("\(z_0\)", z0, S);
dot("\(z\)", z, N);
dot("\(z+\Delta z\)", z+del, E);

draw(z0..(1,1)..(2,1)..z);
draw(z -- z+del);
