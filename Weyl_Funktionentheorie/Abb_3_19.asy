size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
draw((0,0)--(3.5,0)--(3.5,-1)--(5,-1)--(5,0)--(6,0)
   --(6,3)--(4.5,3)--(4.5,4)--(1,4)--(1,3)--(0,3)--cycle);

draw((3.5,0)--(3.5,4));

draw((3,0)--(4,0)--(4,4)--(3,4)--cycle, 0.4mm+black);
label("\(Q_1^{(3)}\)", (3,2), W);
