size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
xaxis();
yaxis();
dot("$N$", (0,0), (-2,-2));

for(int i=-2; i<=2; ++i)
{
    if(i != 0)
        draw(circle((i,0),i));
}

for(int i=-2; i<=2; ++i)
{
    if(i != 0)
        draw(circle((0,i),i), dashed);
}
