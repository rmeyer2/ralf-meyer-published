size(12cm,0);
import math;
import graph;
import geometry;
import markers;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
add("strich2", hatch(1mm, NW));

real n=15, phi=pi/n;
pair O=(0,0), Nm=(-n,0), Np=(n,0);
pair P1=polar(abs(Nm-Np)/cos(phi),phi)-(n,0);
pair P2=polar(abs(Nm-Np)/cos(phi),-phi)-(n,0);
transform t=scale(1/n)*shift(Np);
//---------------------------------------------------------
dot("\(-n\)", Nm, W);
dot("\(+n\)", Np, E);
dot("\(0\)", O, SW);
draw(Nm--P1);
draw(Nm--P2);
draw(Nm--O);
filldraw(Nm--P1--P2--cycle, pattern("strich"), invisible);
markangle(radius=70, P1, Nm, P2);
filldraw(circle(Nm+(5,0), 0.3mm), white, invisible);
label("\(\frac{2\pi}{n}\)", Nm+(5,0));
filldraw(circle(Nm/4, 0.3mm), white, invisible);
label("\(I\)", Nm/4);

draw(t*(Nm--P1));
draw(t*(Nm--P2));
filldraw(t*(Nm--P1--P2--cycle), pattern("strich2"));
draw((2,-2)--(1,0));
filldraw(circle((2,-2), 0.3mm), white, invisible);
label("\(II\)", (2,-2));
