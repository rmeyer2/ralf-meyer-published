size(10cm,0);
import three;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,1);
face[] faces;

triple s=(3,0,0), u1=(1,1,0), v1=(-1,0,1);
triple P1=0.5*u1+0.5v1, P2=shift(s)*P1, c=shift(s/2)*P1;
path3 plane1=plane(u1,v1), plane2=shift(s)*plane1;
path3 planeR = rotate(180,P1,P2)*plane1;
path3 rotation=arc(c,c+(0,-1,0.5)/3,c+(0,-1,0.5)/4,X);
//---------------------------------------------------------
filldraw(faces.push(plane1),plane1,palegrey);
draw(faces.push(plane1),planeR,dashed);
filldraw(faces.push(plane2),plane2,palegrey);
draw(faces.push(plane1),P1--P2);
dot(faces.push(plane2),"$P$", P1, W);
draw(faces.push(plane2),P1--P2, dashed);
dot(faces.push(plane2),"$P'$", P2);
draw(faces.push(plane2),"$\pi$",rotation,E,dashed,ArcArrow);
filldraw(faces.push(planeR),planeR,mediumgrey);
draw(faces.push(planeR),plane1,dashed);

add(faces);
