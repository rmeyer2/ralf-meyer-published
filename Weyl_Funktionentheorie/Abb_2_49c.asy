size(4.5cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
//---------------------------------------------------------
draw(O--polar(1,0), 0.4mm+black);
draw(O--polar(1,pi/4));
draw(O--polar(1,pi/2), 0.4mm+black);
draw(O--polar(1,3*pi/4));
draw(O--polar(1,pi));

draw(shift((0,-0.05))*(polar(0.75,0)--polar(0.25,0)), EndArrow);
draw(shift((-0.05,0.05))*(polar(0.75,pi/4)--polar(0.25,pi/4)), EndArrow);
draw(shift((-0.05,0))*(polar(0.75,pi/2)--polar(0.25,pi/2)), EndArrow);

label("\(0\)", polar(1,0), S);
label("\(1\)", polar(1,pi/4), NE);
label("\(2\)", polar(1,pi/2), N);
label("\(3\)", polar(1,3*pi/4), NW);
label("\(4\)", polar(1,pi), S);
draw("\(b_1\)", arc(O, 1, 20, 35), E, dashed, EndArrow);
draw("\(a_1\)", arc(O, 0.7, 20, 40), E, EndArrow);
draw(arc(O, 1, 0, 180), dashed);
draw(arc(O, 0.7, 0, 180), dashed);
draw(arc(O, 0.4, 0, 180), dotted+0.5mm);
