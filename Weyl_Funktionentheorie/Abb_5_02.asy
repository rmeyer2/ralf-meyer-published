size(12cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform s=shift((10,0));
//---------------------------------------------------------
draw((-0.5,2)--(6,2));
label("\(I\). Blatt", (-0.5,2), W);
dot((0,2));

draw((-0.5,0)--(6,0));
label("\(II\). Blatt", (-0.5,0), W);
dot((0,0));

draw((-0.5,-2)--(6,-2));
label("\(III\). Blatt", (-0.5,-2), W);
dot((0,-2));

draw(s*((0,0)--polar(3,0)));
draw(s*((0,0)--polar(3,2*pi/3)));
draw(s*((0,0)--polar(3,4*pi/3)));

label("\(I\)", s*polar(1.5, pi/3));
label("\(II\)", s*polar(1.5, 3*pi/3));
label("\(III\)", s*polar(1.5, 5*pi/3));
