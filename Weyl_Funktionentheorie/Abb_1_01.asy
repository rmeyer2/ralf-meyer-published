size(5cm,0);
import math;
import graph;

defaultpen(font("OT1","lmr","m","n")+fontsize(10));

path a = (0,1) -- (2,1);
path b = (2,0) -- (2,1);

xaxis("$x$",EndArrow);
yaxis("$y$",EndArrow);

draw(a);  //a
label("$a$", (2,0), S); 
draw(b);  //b
label("$b$", (0,1), W); 

label("$a+\textup{i}b$", (2,1), NE); //a+ib
label("", (2.5,1.5));
