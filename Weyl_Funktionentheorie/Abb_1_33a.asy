size(4cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair eins=polar(1,5*pi/4), zwei=polar(1,7*pi/8);
pair drei=polar(1,pi/2), vier=polar(1,0-pi/8);
path c=circle((0,0),1);
//---------------------------------------------------------
draw(c, dashed);
draw(eins--zwei);
draw(zwei--drei);
draw(drei--vier);
draw(vier--eins);

label("$1$", eins, S);
label("$2$", zwei, W);
label("$3$", drei, N);
label("$4$", vier, E);

markangle(radius=20, eins, zwei, drei);
markangle(radius=20, eins, vier, drei);
