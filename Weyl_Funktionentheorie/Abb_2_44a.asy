size(5cm,0);
import math;
import graph;
import geometry;
import markers;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
real a=3*pi/4;
//---------------------------------------------------------
filldraw((4,2)--(4,-1)--(-2,-1)--(-2,2)--cycle,
         pattern("strich"), invisible);
filldraw(circle((0.3,0.5), 0.3), white, invisible);
filldraw(circle((3.7,-0.2), 0.3), white, invisible);
xaxis("\(x\)", xmin=0, xmax=4, EndArrow);
draw((0,0)--polar(sqrt(8),a), 0.4mm+black);
markangle(radius=30, polar(1,a), (0,0), (1,0));
label("\(a\)", (0.3,0.5));
