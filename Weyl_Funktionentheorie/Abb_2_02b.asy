size(3cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real f(real t) {return 2*log(t);}
guide G=graph(f, 0.44, 2.33, operator ..);
pair P=point(G,10), d=dir(G,10);
path T=shift(P)*((0,0)--3*d);
//---------------------------------------------------------
draw(G);
draw(T);
draw(P--(P.x+3,P.y));
dot("$P'$", P, W);
markangle(shift(P)*(3*d), P, (P.x+3,P.y));
label("$\Theta$", P, (3,2));
label("", (0,-1.7));
label("", (3.5,2.5));
