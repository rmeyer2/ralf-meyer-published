usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real e=0.25, r=1.5, r1=r-2*e*r, r2=r-e*r;
pair O=(0,0);
//---------------------------------------------------------
draw(circle(O, r1));
label("\(\mathfrak{K}_1\)", (0,-r1), N);
draw(circle(O, r2));
label("\(\mathfrak{K}_2\)", (0,-r2), SE+(0,0.15));
draw(circle(O, r));
label("\(\mathfrak{K}\)", (r,0), E);

dot(O);
draw(O--polar(r, pi/6));
label("\(r\)", polar(r,pi/6), E);
draw("\(\varepsilon r\)", polar(r1,-pi/6)--polar(r2,-pi/6));
draw("\(\varepsilon r\)", (r2,0)--(r,0));
