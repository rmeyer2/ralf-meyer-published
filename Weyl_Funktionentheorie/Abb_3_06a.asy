usepackage("amsfonts");
size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair z0=(0,0), z1=(10,2);
guide L1=z0..(3,1.5)..(7,2)..z1;
guide L2=z0..(4,0)..(6,2)..(5,2.5)..(4,1)..(2,1)..(4,3)..(7,3)..z1;
pair[] x=intersectionpoints(L1,L2);
//---------------------------------------------------------
draw(L1, 0.4mm+black);
draw(L2);
dot("\(z_0\)", z0, W);
dot("\(z_1\)", z1, E);

label("\(\mathfrak{L}_1\)", point(L1,2.5), S);
label("\(\mathfrak{L}_2\)", point(L2,1), S);

label("\(1\)", x[1], NW);
label("\(3\)", x[2], NE);
label("\(2\)", x[3], NW);
