size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
real y=3;
//---------------------------------------------------------
for(real x=-3; x<=3; x+=1)
{
  if(x!=0)
    draw((x,-y)--(x,y));
  if(x>=0)
    draw((x-0.3,-1)--(x-0.3,1), EndArrow);
}
draw((0,-y)--(0,y), 0.4mm+black);
for(real d=-y+0.2; d<=y-0.2; d+=0.4)
{
  draw((0,d)--(3,d), 0.05mm+black);
}
dot(O);
