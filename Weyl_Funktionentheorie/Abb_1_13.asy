size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real r=2, phi=pi/6;
pair O=(0,0), Z=polar(r,phi), Z_1=polar(1/r,phi), Z1=polar(1/r,-phi);
//---------------------------------------------------------
xaxis("$x$",EndArrow);
dot("$0$", O, S);
draw(circle(O,1));
draw("$1$",O--polar(1,3*pi/4));
//---------------------------------------------------------
dot("$Z$", Z);
dot("$Z'$", Z1, S);
dot("$\overline{Z'}$", Z_1, N);
draw(O--Z);
draw(Z_1--Z1);
draw(O--(Z.x,-Z.y), dashed);
draw(Z--(Z.x,-Z.y), dashed);
