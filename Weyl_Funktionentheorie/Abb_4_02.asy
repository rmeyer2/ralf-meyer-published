size(4cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
pair O=(0,0);
//---------------------------------------------------------
filldraw(circle(O,0.9), pattern("strich"));
draw(circle(O,1));
dot(O);
draw(O--polar(1,-pi/4));
filldraw(circle((0.2,-0.4), 0.1), white, invisible);
label("\(1\)", (0.2,-0.4));
