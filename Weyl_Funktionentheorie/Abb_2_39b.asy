size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
//---------------------------------------------------------
for(real phi=0; phi<pi; phi+=pi/4)
{
  draw(polar(1,phi+pi)--polar(1,phi), Arrows);
}
draw(polar(0.9,pi)--polar(0.9,0), 0.4mm+black);
dot("\(0\)", O, SW-(2.1,0.5));
