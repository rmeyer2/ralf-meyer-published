size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O = (0,0), a = (2,1), b = (1.5,0), x = a-b;
//---------------------------------------------------------
dot("$0$", O, SW);

draw(O -- a, EndArrow);
label("$\alpha$", a, E);

draw(O -- b, EndArrow);
label("$\beta$", b, E);

draw("$\overline{0\xi}$", O -- x, NW, EndArrow);
label("$\xi$", x, N);

draw( x -- a, dashed);
draw("$\overline{\beta\alpha}$", b -- a, dashed);
