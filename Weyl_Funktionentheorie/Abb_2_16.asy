size(11cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform t=shift((5,0));
pair O=(0,0);
real a,b;
real f(real x) {return sqrt(b^2/a^2 * x^2 - b^2);}
real g(real x) {return -sqrt(b^2/a^2 * x^2 - b^2);}
//---------------------------------------------------------
draw((-1.6,0)--(1.6,0), dashed);
draw((0,-1.6)--(0,1.6), dashed);
draw(polar(1.6,5*pi/4)--polar(1.6,pi/4), dashed);
draw(polar(1.6,3*pi/4)--polar(1.6,-pi/4), dashed);

for(real i=0.5; i<=1.5; i+=0.5)
{
  draw(circle(O,i));
}

label("\(-1\)", (-1,0), NW);
label("\(+1\)", (1,0), NE);
label("\(1\)", (0,1), NE);
//---------------------------------------------------------
draw("\(\frac{1}{2}(z+\frac{1}{z})\)", (1.8,0.5)..(2.4,0.7)..(3,0.5), N, dotted, EndArrow);

draw(t*((-1,0)--(1,0)));
draw(t*((0,-1.6)--(0,1.6)));

for(real i=0.5; i<=1.5; i+=0.5)
{
  draw(t*((0,i)..(sqrt(1+i^2),0)..(0,-i)..(-sqrt(1+i^2),0)..cycle));
}

for(b=0.69; b<1.0; b+=0.10)
{
  a = sqrt(1-b^2);
  draw(t*graph(f, a, 2, operator ..), dashed);
  draw(t*graph(f, -2, -(a+0.00001), operator ..), dashed);
  draw(t*graph(g, a, 2, operator ..), dashed);
  draw(t*graph(g, -2, -(a+0.00001), operator ..), dashed);
}

dot("\(-1\)", t*(-1,0), NW-(1,0));
dot("\(1\)", t*(1,0), NE+(1,0));

clip((-1.6,-1.6)--(-1.6,1.6)--t*(2,1.6)--t*(2,-1.6)--cycle);
