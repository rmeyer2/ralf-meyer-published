size(8cm,0);
import solids;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,1);
// O ist automatisch der Ursprung: O == (0,0,0) durch import three

triple M = (0,0,1), No = (0,0,2), P = (-1.5,-3,0);
real t = -(2*P.z-4)/((P.x)^2+(P.y)^2+(P.z-2)^2);
triple P1 = (t*P.x,t*P.y,t*(P.z-2)+2), Q1 = (P1.x,P1.y,0), V1 = (0,0,P1.z);
revolution kugel = sphere(M,1);
guide3 halbkreis = No..P1..O;
//---------------------------------------------------------
//kugel.filldraw(palegray+opacity(0.1));
kugel.draw(3);

draw(O--(1.5,0,0), EndArrow); label("$x$",(1.5,0,0),X);
draw(O--(0,3,0), EndArrow); label("$y$",(0,3,0),Y);
draw(O--(0,0,2.5), EndArrow); label("$z$",(0,0,2.5),Z);
dot("$0$", O, S);
dot("$P$", P,NW);
dot("$N$", No, NW);
dot("$P'$", P1, SE);
dot("$V'$", V1, E);
dot("$Q'$", Q1, S);

draw(halbkreis);
draw(No--P);
draw(O--P);
draw(P1--V1, dashed);
draw(P1--Q1, dashed);
