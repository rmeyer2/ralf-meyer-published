usepackage("amsfonts");
size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real r=1;
pair z0=polar(r,3*pi/4);
path K=circle((0,0),r);
//---------------------------------------------------------
dot("\(0\)", (0,0), SE);
draw(K);
label("\(\mathfrak{K}\)", (r,0), W);
draw(arc((0,0), r, 135, 90), BeginBar);
label("\(z_0\)", z0, NW);
