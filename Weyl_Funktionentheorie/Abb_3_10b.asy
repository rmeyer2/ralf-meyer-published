usepackage("amsfonts");
size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("strich", hatch(3mm, NE));
path L=(-7,-10)..(9,0)..(7,10)..(-9,0)..cycle;
path L1=(-3,-8)..(-1,-6)..(-3,-4)..(-5,-6)..cycle;
path L2=(1,2)..(5,5)..(2,8)..(-1,5)..cycle;
//---------------------------------------------------------
path c=(-7,-15)--(7,15);
transform s1=shift(rotate(-90)*dir(c,0.5));
transform s2=shift(rotate(90)*dir(c,0.5));

path Lc1=s1*subpath(L, intersections(L,c)[0][0], intersections(L,c)[1][0]);
path Lc2=s2*subpath(L, intersections(L,c)[1][0], intersections(L,c)[0][0]+4);

path L1c1=s1*subpath(L1, intersections(L1,c)[1][0], intersections(L1,c)[0][0]+4);
path L1c2=s2*subpath(L1, intersections(L1,c)[0][0], intersections(L1,c)[1][0]);

path L2c1=s1*subpath(L2, intersections(L2,c)[1][0], intersections(L2,c)[0][0]+4);
path L2c2=s2*subpath(L2, intersections(L2,c)[0][0], intersections(L2,c)[1][0]);

path c11=s1*(intersectionpoints(c,L)[1]--intersectionpoints(c,L2)[0]);
path c12=s2*(intersectionpoints(c,L)[1]--intersectionpoints(c,L2)[0]);
path c21=s1*(intersectionpoints(c,L2)[1]--intersectionpoints(c,L1)[0]);
path c22=s2*(intersectionpoints(c,L2)[1]--intersectionpoints(c,L1)[0]);
path c31=s1*(intersectionpoints(c,L)[0]--intersectionpoints(c,L1)[1]);
path c32=s2*(intersectionpoints(c,L)[0]--intersectionpoints(c,L1)[1]);
//---------------------------------------------------------
draw(Lc1); draw(Lc2);
label("\(\mathfrak{L}'\)", point(Lc1,1), SE);
label("\(\mathfrak{L}''\)", point(Lc2,1), NW);
draw(s1*subpath(Lc1, 1.2, 1.8), EndArrow);
draw(s2*subpath(Lc2, 0.5, 0.8), EndArrow);

draw(L1c1); draw(L1c2);
label("\(\mathfrak{L}_1'\)", point(L1c1,2), SE);
label("\(\mathfrak{L}_1''\)", point(L1c2,2), W);
draw(s2*subpath(L1c1, reltime(L1c1,0.2), reltime(L1c1,0.8)), ArcArrow);
draw(s1*subpath(L1c2, reltime(L1c2,0.2), reltime(L1c2,0.8)), ArcArrow);

draw(L2c1); draw(L2c2);
label("\(\mathfrak{L}_2'\)", point(L2c1,2), SE);
label("\(\mathfrak{L}_2''\)", point(L2c2,2), NW);
draw(s2*subpath(L2c1, reltime(L2c1,0.2), reltime(L2c1,0.8)), ArcArrow);
draw(s1*subpath(L2c2, reltime(L2c2,0.2), reltime(L2c2,0.8)), ArcArrow);

draw(c11); draw(c12);
draw("\(c_3\)", s1*subpath(c11, reltime(c11,0.2), reltime(c11,0.8)), SE, BeginArrow);
draw(s2*subpath(c12, reltime(c12,0.2), reltime(c12,0.8)), EndArrow);

draw(c21); draw(c22);
draw("\(c_2\)", s1*subpath(c21, reltime(c21,0.2), reltime(c21,0.8)), SE, BeginArrow);
draw(s2*subpath(c22, reltime(c22,0.2), reltime(c22,0.8)), EndArrow);

draw(c31); draw(c32);
draw("\(c_1\)", s1*subpath(c31, reltime(c31,0.2), reltime(c31,0.8)), E, EndArrow);
draw(s2*subpath(c32, reltime(c32,0.2), reltime(c32,0.8)), BeginArrow);

label("\(\mbox{G}'\)", (6,-3));
label("\(\mbox{G}''\)", (-6,2));
