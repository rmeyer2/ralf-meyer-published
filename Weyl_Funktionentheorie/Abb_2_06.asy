size(10cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform t=shift((6.4,0));
add("gestrichelt", hatch(4mm,NE));
//---------------------------------------------------------
draw((-2,0)--(2.2,0));
draw((0,-2.2)--(0,2.2), black+0.4mm);
filldraw((0,-2)--(0,2)--(2,2)--(2,-2)--cycle, pattern("gestrichelt"), invisible);

draw("\(w=z^2\)", (2.2,1)..(3.2,1.2)..(4.2,1), N, dotted, EndArrow);

draw(t*((-2,0)--(2.2,0)));
draw(t*((0,-2.2)--(0,2.2)));
draw(t*((0,0)--(-2,0)), black+0.4mm);
filldraw(t*((-2,-2)--(-2,2)--(2,2)--(2,-2)--cycle), pattern("gestrichelt"), invisible);
