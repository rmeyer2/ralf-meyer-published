size(6.11cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path kugelflaeche=circle((0,1),1);
path zebene = (-3,0)--(1.5,0);
pair U1=(-2.5,0);
pair U2=intersectionpoints(kugelflaeche,U1--(0,2))[1];
pair gegen=intersectionpoints(kugelflaeche,U2--(-U2.x,-U2.y+1)*2+(0,1))[1];
//---------------------------------------------------------
draw(kugelflaeche);
draw(zebene, black+0.3mm);
draw(U1--(0,2));
draw("$III$", gegen-0.3*(1,-gegen.x/(gegen.y-1))--gegen+0.3(1,-gegen.x/(gegen.y-1)), (3,6), black+0.3mm);

dot("$U'$", U1, N);
dot("$U''$", U2, NW);
dot(gegen);

tick((0,0),S); tick((0,0),N); label("$0$", (0,-0.5), N);
tick((0,2),S); tick((0,2),N); label("$N$", (0,2-0.5), N);
label("$II$", (-1.5,-0.5), N); label("$z'$", (-1,-0.5), N);
