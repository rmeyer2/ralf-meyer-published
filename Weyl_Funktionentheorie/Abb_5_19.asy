size(6cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
add("s", hatch(2mm, NE));
path p=(3,0)--(15,0)--(15,12)--(12,12)--(12,15)--(6,15)
     --(6,14)--(0,14)--(0,12)--(6,12)--(6,9)--(3,9)--cycle;
path g1=(9,15)--(9,10);
path g2=(9,0)--(9,4);
path c1=circle((9,10), 2);
path c2=circle((9,7), 3);
//---------------------------------------------------------
filldraw(c1, pattern("s"), invisible);
clip(c2);

draw(p);
draw(g1, 0.4mm+black);
draw(g2, 0.4mm+black);
draw(c1);
draw(c2);
dot((9,7));

for(real i=0.5; i<5; i+=0.5)
{
  draw(subpath(g1, arctime(g1,i-1), arctime(g1,i)), EndBar);
}

for(real i=0.5; i<4; i+=0.5)
{
  draw(subpath(g2, arctime(g2,i-1), arctime(g2,i)), EndBar);
}
