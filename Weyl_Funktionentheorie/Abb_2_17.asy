size(3cm,0);
import math;
import graph;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), P1=polar(2,pi/6), P2=polar(2,-pi/6);
//---------------------------------------------------------
draw(O--(2,0));
draw(O--P1);
draw(O--P2);
dot("\(0\)", O, W);
//markangle(radius=80, P2, O, P1);
draw(arc(O, 1.5, -30, 30));
label("\(\frac{\pi}{n}\)", (1,0.3));
label("\(\frac{\pi}{n}\)", (1,-0.3));
