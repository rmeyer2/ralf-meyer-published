size(8cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real f(real x) {return asin(exp(-x));}
real g(real x) {return -asin(exp(-x));}
guide G1=graph(f, 0, 8, operator ..);
guide G2=shift((0,pi))*graph(g, 0, 8, operator ..);
pair L=W-(1,0);
transform t=shift((0,-pi));
//---------------------------------------------------------
xaxis("\(x\)", axis=YEquals(0), xmin=0, EndArrow);
xaxis(xmin=0, axis=YEquals(pi));
yaxis("\(y\)", ymin=-pi, ymax=4, LeftTicks("%", new real[]{-pi,-pi/2,0,pi/2,pi}), EndArrow);

label("\(\pi\)", (0,pi), L);
label("\(\frac{\pi}{2}\)", (0,pi/2), L);
label("\(0\)", (0,0), L);
label("\(-\frac{\pi}{2}\)", (0,-pi/2), L);
label("\(-\pi\)", (0,-pi), L);

draw(G1); draw(G2);
draw(t*G1); draw(t*G2);
