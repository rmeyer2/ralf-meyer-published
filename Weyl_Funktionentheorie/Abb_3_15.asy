size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path p=(0,0)..(10,10)..(20,0)--(12,0)..(10,2)..(8,0)--(7,0)
     ..(6,1)..(5,0)--cycle;
path q=shift((0,-1))*reflect((0,0),(1,0))*p;
//---------------------------------------------------------
draw(p);
draw(q);

for(real i=0.02; i<=0.58; i+=0.02)
{
  label("{\tiny\(+\)}", point(p,reltime(p,i)), rotate(90)*dir(p,reltime(p,i)));
  label("{\tiny\(+\)}", point(q,reltime(q,i)), rotate(-90)*dir(q,reltime(q,i)));
}

for(real i=0.6; i<=1; i+=0.02)
{
  if(i>0.92 || i<0.72 || (i<=0.86 && i>=0.84) || (i<=0.9 && i>=0.88) || (i<=0.82 && i>=0.74))
    label("{\tiny\(-\)}", point(q,reltime(q,i)), rotate(90)*dir(q,reltime(q,i))/8);
  
  if(i>0.92 || i<0.72 || (i<=0.86 && i>=0.84))
    label("{\tiny\(+\)}", point(p,reltime(p,i)), rotate(-90)*dir(p,reltime(p,i))/8);

  if((i<=0.9 && i>=0.88) || (i<=0.82 && i>=0.74))
    label("{\tiny\(-\)}", point(p,reltime(p,i)), rotate(-90)*dir(p,reltime(p,i))/8);
}
