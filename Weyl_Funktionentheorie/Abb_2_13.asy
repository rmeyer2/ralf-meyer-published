size(10cm,0);
import three;
import geometry;
import markers;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform3 t=shift((4,0,0));
int n=3;
real s=3, r=s/n;
real dz_r=sqrt(s^2 - r^2);
triple M=(0,0,0), T=(M.x,M.y,M.z+dz_r);
triple X1=(M.x+r,M.y,M.z), X2=(M.x-r,M.y,M.z);
triple P1=s*(cos(pi+pi/(2*n)),0,sin(pi+pi/(2*n)));
triple P2=s*(cos(-pi/(2*n)),0,sin(-pi/(2*n)));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,1);

draw(arc(M, X1, X2, Z, CW));
draw(arc(M, X1, X2, Z, CCW), dashed);
draw(M--T, dashed);
draw(X1--T);
draw("\(s\)", X2--T);
draw("\(r\)", M--(M.x+r*cos(5*pi/4),M.y+r*sin(5*pi/4),0), 3*N, dashed);
markangle(radius=40, X2, T, X1);
label("\(\alpha\)", T, (1.5,0,-8));
//---------------------------------------------------------
currentprojection = orthographic(0,-1,0);

draw("\(s\)", t*(T--T+P1));
draw(t*(T--T+P2));
draw("\(s\frac{2\pi}{n}\)", t*arc(T, T+P1, T+P2, Y, CW));
markangle(radius=25, t*(T+P1), t*T, t*(T+P2));
label("\(\frac{2\pi}{n}\)", t*T, S);
