size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0);
path p=O..(1,1)..(2,1.3)..(2.2,1.8)..(1,2.3)
     ..(2.2,2.7)..(2.5,2.8)..(3.3,3.3);
//---------------------------------------------------------
dot("\(z_0\)", O, SE);
draw(circle(O, 1.3));
draw("\(r_0\)", O--polar(1.3,3*pi/4));
draw(p);
for(int i=1; i<=7; ++i)
{
  if(i!=3)
    draw(subpath(p, arctime(p,i-0.5), arctime(p,i)), EndBar);
  if(i<3)
    label("\(z_"+string(i)+"\)", point(p, arctime(p,i)), 1.5*S);
  if(i>3)
    label("\(z_"+string(i-1)+"\)", point(p, arctime(p,i)), 1.5*S);
}
