size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair xi1=(-1,0), xi2=(1,0);

path kreis(real t, real phi1=0, real phi2=360)
{
  return arc((0,t),sqrt((t)^2 + xi1.x^2), phi1, phi2);
}
//---------------------------------------------------------
for(real i=-2; i<=2; i+=0.5)
{
  draw(kreis(i));
}

dot("\(\xi_1\)", xi1, W-(2,0));
dot("\(\xi_2\)", xi2, E+(2,0));

clip((-3,-2)--(-3,2)--(3,2)--(3,-2)--cycle);

label("", (0,-2.5));
