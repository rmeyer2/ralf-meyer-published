size(5cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real x1=0, x2=3, y1=0, y2=2;
//---------------------------------------------------------
draw((x1,y1)--(x2,y1)--(x2,y2)--(x1,y2)--cycle);
label("\(x_1,y_1\)", (x1,y1), S);
label("\(x_2,y_1\)", (x2,y1), S);
label("\(x_2,y_2\)", (x2,y2), N);
label("\(x_1,y_2\)", (x1,y2), N);
