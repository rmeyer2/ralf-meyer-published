usepackage("nicefrac");
size(10cm,0);
import solids;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
currentprojection = orthographic(1,-7,3);

triple M=(0,0,0.5), P=(3/2,0,0), Q=(2/3,0,0), A=(1,0,0), No=(0,0,1);
triple P1 = (6/13,0,9/13);
triple Q1 = (6/13,0,8/26);
triple B=polar(1,pi/2,-asin(1/abs(P)));
revolution sph=sphere(M,0.5);
//---------------------------------------------------------
draw((-0.6,0,0)--(1.7,0,0), EndArrow); label("$x$", (1.7,0,0), X);
draw((0,-1,0)--(0,1,0), EndArrow); label("$y$", (0,1,0), Y);
draw(O--(0,0,1.3), EndArrow); label("$z$", (0,0,1.3), Z);

sph.draw(3);
draw(arc(O, (sqrt(2)/2,sqrt(2)/2,0), (-sqrt(2)/2,sqrt(2)/2,0), Z, CW));
draw(No--P);
draw(No--Q);
draw(No--A);
draw(O--B);
draw(P1--Q1, dashed);
draw(Q--B);
draw(P--B);

dot("$N$", No, NW);
dot("$P$", P, S);
dot("$P_1$", P1, NE);
dot("$Q$", Q, SW);
dot("$Q_1$", Q1, E);
dot("$A$", A, S);
dot("$B$", B, S);

draw("{\footnotesize\(1\)}", O--(0,-1,0), W, Arrows);
draw("{\footnotesize\(\nicefrac{1}{2}\)}", (-0.5,0,0.5)--(0,0,0.5), Arrows);
//draw("{\footnotesize\(1\)}", (0,0,0)--(1,0,0), N+(1,0,-0.3), Arrows);
