size(5cm,0);
import math;
import graph;
import patterns;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
path G=(-3,0)..(-1.5,2)..(0,1)..(2.5,2.5)..(4,0)..
       (2.5,-1.5)..(0,-1)..(-1.5,-1.5)..cycle;
path F=shift(0.5,0.2)*scale(0.3)*reflect((-1,0),(1,0))*G;
add("gestrichelt", hatch(4mm, NE));
//---------------------------------------------------------
filldraw(G,pattern("gestrichelt"));
filldraw(F, white);
dot((-2,0));
dot((2,1));
draw((-2,0)..(0,0.8)..(2,1));
