size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair B=(2,1), R=(-1,2);
//---------------------------------------------------------
for(int i=-2; i<=2; ++i)
{
    draw(shift(i*R/4)*(-B--2*B), EndArrow);
    draw(shift(-B/3+i*B/4)*(-R--0.75*R), dashed);
}

draw(shift(R*0.625+B/6)*(-B-- -0.125*B), EndArrow);
draw(shift(-R*0.625+B/6)*(-B-- -0.125*B), EndArrow);
label("$II$", -R, S);
label("$I$", 2.3*B, W);
