size(3cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair f(real t) {return (sin(t)*cos(2*t), sin(t)*sin(2*t));}
path g;
//---------------------------------------------------------
for(real s=pi/2; s<=3*pi/2; s+=pi/100)
{
  g=g..f(s);
}
draw(arc((0,0), 1, 90, 270));
draw(rotate(90)*g);
