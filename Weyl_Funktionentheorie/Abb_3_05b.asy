usepackage("amsfonts");
size(0,2.5cm);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real a=2, b=2.8, A=11.2, B=3.5;
guide L=(a,b)..(3,1.5)..(5,1.6)..(6,1.5)..(7.5,1.2)..(10,2)..(A,B)
      ..(10,5)..(8,5)..(6,4.2)..(5.6,4.2)..(5,4.4)..(3.3,4.3)..cycle;
L=shift((-1.5,0))*L;
pair p1=point(L, 7.5);
pair p2=point(L, 4);
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);

draw(L);
draw(p1-(2,0) -- p1+(2,0));
draw(p2-(2,0) -- p2+(2,0));
label("\(\mathfrak{F}\)", (7,3));
