size(4cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
pair O=(0,0), x=(2,1), u=(1,1);
//---------------------------------------------------------
draw("\(x\)", O--x, S, EndArrow);
draw("\(\vec{u}\)", O--u, NW, EndArrow);
label("\(P_1\)", x, E);
