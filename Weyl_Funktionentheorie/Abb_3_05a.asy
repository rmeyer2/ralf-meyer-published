usepackage("amsfonts");
size(0,2.5cm);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
real a=2, b=2.8, A=11.2, B=3.5;
guide L=(a,b)..(3,1.5)..(5,1.6)..(6,1.5)..(7.5,1.2)..(10,2)..(A,B)
      ..(10,5)..(8,5)..(6,4.2)..(5.6,4.2)..(5,4.4)..(3.3,4.3)..cycle;
//---------------------------------------------------------
xaxis("\(x\)", EndArrow);
yaxis("\(y\)", EndArrow);

draw((a,0)--(a,b+1));
draw((A,0)--(A,B+1));
draw((a,b)--(a,0), BeginBar);
draw((A,B)--(A,0), BeginBar);
label("{\small\(a,b\)}", (a,b), W-(0.5,0));
label("\(A,B\)", (A,B), E);

draw(L);
label("\(\mathfrak{L}_2\)", point(L,12), S);
label("\(\mathfrak{L}_1\)", point(L,1), S);
draw(subpath(L,4,5), EndArrow);
draw(subpath(L,7,8), EndArrow);
label("\(\mathfrak{F}\)", (7,3));
