size(12cm,0);
import math;
import graph;
defaultpen(font("OT1","lmr","m","n")+fontsize(10));
//---------------------------------------------------------
transform s=shift((10,0));
pair P=(-2,-2), Q=(1,1), R=(2,2), d=(1,1);
pair P1=s*(-3,-2), Q1=s*(4,8/3), R1=s*(3/2,1), e=(1,2/3);
pair M=(P+R)/2, M1=(P1+R1)/2;
real r=abs((P-R)/2), r1=abs((P1-R1)/2);
path c=circle(M,r);
path c1=circle(M1,r1);
path g=(Q+(-4,8/3)--Q-(-3,2));
path g1=(Q1+(2,-2)--Q1-(2,-2));
pair[] x=intersectionpoints(c,g);
//---------------------------------------------------------
dot("$P$", P, W);
dot("$Q$", Q, S);
dot("$R$", R, (0,2));
draw((P-d)--(R+d));
draw(c);
draw(g);
draw("$\overline{PR}$", M--polar(r,3*pi/4), SW, dashed);

dot("$P''$", P1, W);
dot("$Q''$", Q1, (0.5,1.5));
dot("$R''$", R1, (2,0));
draw((P1-e)--(Q1+e));
draw(c1);
draw(g1);
draw("$\overline{P''R''}$", M1--shift(M1)*polar(r1,3*pi/4), NE, dashed);

draw(Q+(-2.9,2.5) .. (5,5)  .. Q1+(-1.7,1.5), dotted, EndArrow);
dot("$X$", x[1], (2,0));
label("?", (9,3), E);
draw(x[1]..(4,1.5)..(9,3), dotted, EndArrow);
label("$E$", (0,6));
label("$E''$", (10,6));
draw((5,7)--(5,-4), dashed);
