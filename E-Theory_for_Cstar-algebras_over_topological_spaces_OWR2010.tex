\NeedsTeXFormat{LaTeX2e}
\documentclass{owrart}
\pdfoutput=1 % forces pdf output, especially for arxiv submission
\usepackage{amssymb}
\usepackage{enumitem}

\usepackage{microtype}

\newcommand*{\Z}{\mathbb Z}
\newcommand*{\N}{\mathbb N}

\begin{document}
\begin{talk}[Marius Dadarlat]{Ralf Meyer}{E-theory for C*-algebras over topological spaces}{Meyer, Ralf}

This talk surveyed the main ideas and results of~\cite{Dadarlat-Meyer:E_over_space}.  This article defines an \(\textup E\)\nobreakdash-theory for separable \(\textup C^*\)\nobreakdash-algebras over second countable topological spaces and establishes its basic properties.  The most interesting of these is an approximation theorem, which relates the \(\textup E\)\nobreakdash-theory over a general space to the \(\textup E\)\nobreakdash-theories over finite approximations to this space.  This provides effective criteria for invertibility of  \(\textup E\)\nobreakdash-theory elements, even over infinite-dimensional topological spaces.   Furthermore, we prove a Universal Multicoefficient Theorem for \(\textup C^*\)\nobreakdash-algebras over totally disconnected metrisable compact spaces.

\smallskip

Why do we need such a new \(\textup E\)\nobreakdash-theory?

Eberhard Kirchberg~\cite{Kirchberg:Michael} proved a far-reaching classification theorem for non-simple, strongly purely infinite, stable, nuclear, separable \(\textup C^*\)\nobreakdash-algebras.  Roughly speaking, two such \(\textup C^*\)\nobreakdash-algebras are isomorphic once they have homeomorphic primitive ideal spaces --~call this space~\(X\)~-- and are \(\textup{KK}(X)\)-equivalent in a suitable bivariant \(\textup K\)\nobreakdash-theory for \(\textup C^*\)\nobreakdash-algebras over~\(X\).  To apply this classification theorem, we need tools to compute this bivariant \(\textup K\)\nobreakdash-theory.

Recall that Kasparov theory only satisfies excision for \(\textup C^*\)\nobreakdash-algebra extensions with a completely positive section.  Similar technical restrictions appear for all variants of Kasparov theory, including Kirchberg's.  This is a severe limitation.  For instance, excision does not hold in general for extensions of the form \(A(U) \rightarrowtail A \twoheadrightarrow A/A(U)\) for an open subset~\(U\), where \(A(U)\) denotes the restriction of~\(A\) to~\(U\), extended by~\(0\) to a \(\textup C^*\)\nobreakdash-algebra over the original space, even if~\(A\) is nuclear.

In the non-equivariant case, such technical problems are resolved by passing to \(\textup E\)\nobreakdash-theory, which satisfies excision for all \(\textup C^*\)\nobreakdash-algebra extensions (see~\cite{Connes-Higson:Deformations}).  The equivariant \(\textup E\)\nobreakdash-theory defined in~\cite{Dadarlat-Meyer:E_over_space} has the same advantages over its corresponding \(\textup{KK}\)-theory.  At the same time, the two theories are equal in sufficiently many cases to get information about \(\textup{KK}\) from \(\textup E\)\nobreakdash-theory arguments.  For instance, the natural map \(\textup E_*(X;A,B)\to \textup{KK}_*(X;A,B)\) is invertible if~\(X\) is locally compact and Hausdorff and~\(A\) is a continuous field of nuclear \(\textup C^*\)\nobreakdash-algebras over~\(X\).  Hence we may use the good formal properties of \(\textup E\)\nobreakdash-theory to study \(\textup{KK}\).

\smallskip

Our definition of \(\textup E_*(X;A,B)\) is based on asymptotic homomorphisms satisfying an approximate equivariance condition.  An asymptotic homomorphism \(\varphi_t\colon A\to B\), \(t\in [0,\infty)\), is called \emph{approximately \(X\)\nobreakdash-equivariant} if for each open subset \(U\subseteq X\), we have
\[
\lim_{t\to \infty} {}\lVert\varphi_t(a)\rVert_{X\setminus U} =0
\qquad\text{for all \(a\in A(U)\),}
\]
where \(\lVert\varphi_t(a) \rVert_{X\setminus U}\) denotes the norm of~\(\varphi_t(a)\) in the quotient \(B(X\setminus U) = B/B(U)\) of~\(B\).

Let \(\mathcal U=(U_n)_{n\in\N}\) be a countable basis for the topology of~\(X\).  For each \(n\in\N\), the open subsets \(U_1,\dotsc,U_n\) generate a finite topology~\(\tau_n\) on~\(X\).  Let~\(X_n\) be the \(\textup T_0\)\nobreakdash-quotient of \((X,\tau_n)\), this is a finite \(\textup T_0\)\nobreakdash-space.  The quotient map \(X\twoheadrightarrow X_n\) allows us to view \(\textup C^*\)\nobreakdash-algebras over~\(X\) as \(\textup C^*\)\nobreakdash-algebras over~\(X_n\) for all \(n\in\N\).  Our first main result is a short exact sequence
\begin{equation}
  \label{eq:intro_approximation_sequence}
  \mathop{\varprojlim_{n\in\N}}\nolimits^1 \textup E_{*+1}(X_n;A,B) \rightarrowtail \textup E_*(X;A,B) \twoheadrightarrow \varprojlim_{n\in\N} \textup E_*(X_n;A,B)
\end{equation}
for all separable \(\textup C^*\)\nobreakdash-algebras \(A\) and~\(B\) over~\(X\).  This is made plausible by the observation that an asymptotic homomorphism \(A\to B\) is approximately \(X\)\nobreakdash-equivariant if and only if it is approximately \(X_n\)\nobreakdash-equivariant for all \(n\in\N\).  Hence the space of approximately \(X\)\nobreakdash-equivariant asymptotic homomorphisms is the intersection of the spaces of approximately \(X_n\)\nobreakdash-equivariant asymptotic homomorphisms for \(n\in\N\).

As an important application of~\eqref{eq:intro_approximation_sequence}, we give an effective criterion for invertibility of \(\textup E\)\nobreakdash-theory elements: an element in \(\textup E_*(X;A,B)\) is invertible if and only if its image in \(\textup E_*\bigl(A(U),B(U)\bigr)\) is invertible for all \(U\in\mathbb O(X)\).  As a consequence, if all two-sided closed ideals of a separable nuclear \(\textup C^*\)\nobreakdash-algebra~\(A\) with Hausdorff primitive spectrum~$X$ are \(\textup{KK}\)-contractible, then
\[
A\otimes \mathcal O_\infty \otimes \mathbb K\cong \textup C_0(X)\otimes \mathcal O_2 \otimes \mathbb K.
\]
This result solves the problem of characterising the trivial continuous fields with fibre $\mathcal O_2 \otimes \mathbb K$ within the class of strongly purely infinite, stable, continuous fields of \(\textup C^*\)\nobreakdash-algebras.  It is worth noting that in general the \(\textup{KK}\)-contractibility of ideals does not follow from the \(\textup{KK}\)-contractibility of the fibres.  Indeed, there are examples of separable nuclear continuous fields~$A$ over the Hilbert cube with all fibres isomorphic to~$\mathcal O_2$ and yet such that $\textup K_0(A)\neq 0$, see~\cite{Dadarlat:Fiberwise_KK}.

While~\eqref{eq:intro_approximation_sequence}, in principle, reduces the computation of \(\textup E_*(X;A,B)\) for infinite spaces~\(X\) to the corresponding problem for the finite approximations~\(X_n\), this does not yet lead to a Universal Coefficient Theorem.  If \(\textup E_*(X_n;A,B)\) is computable by Universal Coefficient Theorems for all \(n\in\N\), the latter will usually involve short exact sequences.  Thus we have to combine two short exact sequences, as in the computation of the \(\textup K\)\nobreakdash-theory for crossed products by~\(\Z^2\) using the Pimsner--Voiculescu exact sequence twice.  This can only be carried through if we have some extra information.  In terms of the general homological machinery  developed in~\cite{Meyer-Nest:Homology_in_KK}, we find that the homological dimension of \(\textup E\)\nobreakdash-theory over an infinite space~\(X\) may be one larger than the homological dimensions of the finite approximations~\(X_n\).  Thus it is usually~\(2\), which does not suffice for classification theorems.

In fact, it is well-known that filtrated \(\textup K\)\nobreakdash-theory cannot be a complete invariant for \(\textup C^*\)\nobreakdash-algebras over the one-point compactification of~\(\N\).  The counterexample in~\cite{Dadarlat-Eilers:Bockstein} may be transported easily to any compact Hausdorff space.

If~\(X\) is the Cantor space or, more generally, a totally disconnected metrisable compact space, then we may resolve the counterexamples mentioned above by taking into account coefficients.  Our second main result is a Universal \emph{Multi}coefficient Theorem for \(\textup E_*(X;A,B)\) for  two \(\textup C^*\)\nobreakdash-algebras \(A\) and~\(B\) over the Cantor space.  It assumes that \(A(U)\) belongs to the \(\textup E\)\nobreakdash-theoretic bootstrap class for all open subsets \(U\subseteq X\) and yields a natural exact sequence
\[
\operatorname{Ext}_{\textup C(X,\Lambda)}\bigl(\underline{\textup K}(A)[1],\underline{\textup K}(B)\bigr)
\rightarrowtail \textup E(X;A,B) \twoheadrightarrow
\operatorname{Hom}_{\textup C(X,\Lambda)}\bigl(\underline{\textup K}(A),\underline{\textup K}(B)\bigr),
\]
where~\(\underline{\textup K}\) denotes the \(\textup K\)\nobreakdash-theory of~\(A\) with coefficients, viewed as a countable module over the \(\Z/2\)\nobreakdash-graded ring \(\textup C(X,\Lambda)\) of locally constant functions from~\(X\) to the \(\Z/2\)\nobreakdash-graded ring~\(\Lambda\) of B\"ockstein operations (see~\cite{Dadarlat-Loring:Multicoefficient}).  As a consequence, two \(\textup C^*\)\nobreakdash-algebras \(A\) and~\(B\) in the \(\textup E\)\nobreakdash-theoretic bootstrap class over~\(X\) are \(\textup E(X)\)-equivalent if and only if \(\underline{\textup K}(A)\) and \(\underline{\textup K}(B)\) are isomorphic as \(\textup C(X,\Lambda)\)-modules.


\begin{thebibliography}{9}
\bibitem{Connes-Higson:Deformations}
  {Connes, Alain},
  {Higson, Nigel},
  \textit{D\'eformations, morphismes asymptotiques et $K$\nobreakdash -th\'eorie bivariante},
  {C. R. Acad. Sci. Paris S\'er. I Math.},
  \textbf{311} (1990),
  {101--106}.

\bibitem{Dadarlat:Fiberwise_KK}
  {D\u {a}d\u {a}rlat, Marius},
  \textit{Fiberwise $KK$-equivalence of continuous fields of $C^*$\nobreakdash -algebras},
  {J. K-Theory},
  \textbf{3} (2009),
  {205--219}.

\bibitem{Dadarlat-Eilers:Bockstein}
  {D\u {a}d\u {a}rlat, Marius},
  {Eilers, S{\o }ren},
  \textit{The B\"ockstein map is necessary},
  {Canad. Math. Bull.},
  \textbf{42} (1999),
  {274--284}.

\bibitem{Dadarlat-Loring:Multicoefficient}
  {D\u {a}d\u {a}rlat, Marius},
  {Loring, Terry A.},
  \textit{A universal multicoefficient theorem for the Kasparov groups},
  {Duke Math. J.},
  \textbf{84},
  (1996),
  {355--377}.

\bibitem{Dadarlat-Meyer:E_over_space}
  {D\u {a}d\u {a}rlat, Marius},
  {Meyer, Ralf},
  \textit{E\nobreakdash -Theory for $\textup C^*$\nobreakdash -algebras over topological spaces},
  eprint (2009),
  arXiv: 0912.0283.

\bibitem{Kirchberg:Michael}
  {Kirchberg, Eberhard},
  {Das nicht-kommutative Michael-Auswahlprinzip und die Klassifikation nicht-einfacher Algebren},
  in \textit{\(C^*\)-Algebras (M\"unster, 1999)}, Springer, 2000, p.~{92--141}.

\bibitem{Meyer-Nest:Homology_in_KK}
  {Meyer, Ralf},
  {Nest, Ryszard},
  {Homological algebra in bivariant \(\textup {K}\)\nobreakdash -theory and other triangulated categories. I},
  in \textit{Triangulated categories}, {London Math. Soc. Lecture Notes} \textbf{375}, {Cambridge University Press}, 2010, {to appear},
  arXiv: {math.KT/0702146}.
\end{thebibliography}
\end{talk}
\end{document}